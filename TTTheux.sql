-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 04 oct. 2018 à 10:20
-- Version du serveur :  10.1.34-MariaDB
-- Version de PHP :  7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `TTTheux`
--

-- --------------------------------------------------------

--
-- Structure de la table `club`
--

CREATE TABLE `club` (
  `id` varchar(8) NOT NULL,
  `club` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `club`
--

INSERT INTO `club` (`id`, `club`) VALUES
('L184', 'TT Theux');

-- --------------------------------------------------------

--
-- Structure de la table `player`
--

CREATE TABLE `player` (
  `id` int(11) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `clubId` varchar(8) NOT NULL,
  `rankingId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `player`
--

INSERT INTO `player` (`id`, `lastName`, `firstName`, `clubId`, `rankingId`) VALUES
(128231, 'Boudron', 'Boris', 'L184', 7);

-- --------------------------------------------------------

--
-- Structure de la table `playerseries`
--

CREATE TABLE `playerseries` (
  `playerId` int(11) NOT NULL,
  `serieId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ranking`
--

CREATE TABLE `ranking` (
  `id` int(11) NOT NULL,
  `ranking` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ranking`
--

INSERT INTO `ranking` (`id`, `ranking`) VALUES
(17, 'B0'),
(16, 'B2'),
(15, 'B4'),
(14, 'B6'),
(13, 'C0'),
(12, 'C2'),
(11, 'C4'),
(10, 'C6'),
(9, 'D0'),
(8, 'D2'),
(7, 'D4'),
(6, 'D6'),
(5, 'E0'),
(4, 'E2'),
(3, 'E4'),
(2, 'E6'),
(1, 'NC');

-- --------------------------------------------------------

--
-- Structure de la table `serie`
--

CREATE TABLE `serie` (
  `id` int(11) NOT NULL,
  `serie` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `serie`
--

INSERT INTO `serie` (`id`, `serie`) VALUES
(10, 'Critérium Jeunes Minimes'),
(9, '﻿Critérium Jeunes Minimes');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `un_club` (`club`) USING BTREE;

--
-- Index pour la table `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ranking` (`rankingId`),
  ADD KEY `fk_club` (`clubId`);

--
-- Index pour la table `playerseries`
--
ALTER TABLE `playerseries`
  ADD PRIMARY KEY (`playerId`,`serieId`),
  ADD KEY `fk_serie` (`serieId`);

--
-- Index pour la table `ranking`
--
ALTER TABLE `ranking`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `un_ranking` (`ranking`) USING BTREE;

--
-- Index pour la table `serie`
--
ALTER TABLE `serie`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `un_serie` (`serie`) USING BTREE;

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `ranking`
--
ALTER TABLE `ranking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `serie`
--
ALTER TABLE `serie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `player`
--
ALTER TABLE `player`
  ADD CONSTRAINT `fk_club` FOREIGN KEY (`clubId`) REFERENCES `club` (`id`),
  ADD CONSTRAINT `fk_ranking` FOREIGN KEY (`rankingId`) REFERENCES `ranking` (`id`);

--
-- Contraintes pour la table `playerseries`
--
ALTER TABLE `playerseries`
  ADD CONSTRAINT `fk_player` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`),
  ADD CONSTRAINT `fk_serie` FOREIGN KEY (`serieId`) REFERENCES `player` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
