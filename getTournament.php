<?php

use TTTheux\Core\DB\TabTConnector;

require_once "vendor/autoload.php";
$loader = new Twig_Loader_Filesystem('src/Views');
$twig = new Twig_Environment($loader);
$twig->addExtension(new Twig_Extension_Debug());
$twig->addFilter(new Twig_SimpleFilter('var_dump', 'var_dump'));
//---------------------------------
// GetMatches-Frenoy.php
//---------------------------------
// Variables globales
$Season = 19;

//$Levels = array(1,16,12);   // National, IWB, Namur
//$Levels = array(1,16,12,11,4,13,14);  // AFTT
//$Levels = array(1,16,12,11,4,13,14,2,3,5,6,7,8,9,10,15); // FRBTT : National, IWB, Namur, BBW, Hainaut, Liège, Lux, ???, ???, VlBBr, Super, Oost-Vl., Antw., West-Vl., Limburg, RegionVTTL
$Levels = array(13); // Liège

# Chemin vers fichier texte
$file ="D:\\Test.txt";
//$file ="C:\\TT\\saison ".(2000+$Season-1)."-".(2000+$Season)."\DB AFTT\Frenoy\Matchs.txt";

$Credentials = TabTConnector::getCredentials();

// Variables pour appeler les fonctions de TabT
$TestRequest = array('Credentials' => $Credentials
);

$GetTournamentsRequest = array('Credentials' => $Credentials
                                ,'Season' => $Season
                                ,'TournamentUniqueIndex' => null
                                ,'WithResults' => false
                                );
$searchedTournamentName = 'CP Tour Minerois';
$searchedTournamentIndex = null;

$GetMatchesRequest = array('Credentials' => $Credentials
,'DivisionId'  => -1
,'Club'  => ''
,'Team'  => ''
,'DivisionCategory'  => -1
,'Season'  => $Season
,'WeekName'  => ''
,'Level'  => ''
,'ShowDivisionName' => 'no'
,'YearDateFrom'  => '2000-01-01'
,'YearDateTo'  => '9999-12-31'
,'WithDetails' => true
,'MatchId'  => ''
,'MatchUniqueId'  => ''
);

// Variable d'output
$DivisionCategory = '';
$DivisionId = '';
$MatchId = '';
$Journee = '';
$Date = '';
$Heure = '';
$IndexVt = '';
$TeamVt = '';
$PlayerVt = array('','','','');
$IsPlayerVtWo = array('','','','');
$PlayerVr = array('','','','');
$IsPlayerVrWo = array('','','','');
$ScoreVt = -1;
$ScoreVr = -1;
$IsFfg = false;
$RencSetsVt = array('','','','','','','','','','','','','','','','');
$RencSetsVr = array('','','','','','','','','','','','','','','','');
$RencDetPts = array('','','','','','','','','','','','','','','','');

// Ouverture de la connection Soap
$tabt = TabTConnector::getInstance();


// en output, on aura un tableau qui donnera le statut pour chaque appel
echo '<table>';

// Test
$ResponseTest = $tabt->Test($TestRequest);
echo '<tr>';
echo '<td></td>';
echo '<td>TimeStamp</td>';
echo '<td>CurrentQuota</td>';
echo '<td>AllowedQuota</td>';
echo '</tr>';
echo '<tr>';
echo '<td>Début de test</td>';
echo '<td>', $ResponseTest->Timestamp, '</td>';
echo '<td>', $ResponseTest->CurrentQuota, '</td>';
echo '<td>', $ResponseTest->AllowedQuota, '</td>';
echo '</tr>';


// Recherche de l'id du bon tournoi
$responseTournaments = $tabt->GetTournaments($GetTournamentsRequest);
$c = 0;
$tournamentFound = false;
while (!$tournamentFound && $c < $responseTournaments->TournamentCount)
{
    $t = $responseTournaments->TournamentEntries[$c];
    if($t->Name == $searchedTournamentName)
    {
        $tournamentFound = true;
        $searchedTournamentIndex = $t->UniqueIndex;
    }
    else
        $c++;
}

$GetTournamentsRequest['TournamentUniqueIndex'] = $searchedTournamentIndex;

$responseTournaments = $tabt->GetTournaments($GetTournamentsRequest);
var_dump($responseTournaments->TournamentEntries->SerieEntries[0]);



// Test
$ResponseTest = $tabt->Test($TestRequest);
echo '<tr>';
echo '<td>Fin de test</td>';
echo '<td>', $ResponseTest->Timestamp, '</td>';
echo '<td>', $ResponseTest->CurrentQuota, '</td>';
echo '<td>', $ResponseTest->AllowedQuota, '</td>';
echo '</tr>';

// en output, fin du tableau
echo '</table>';
//echo $twig->render("feuilleMatch.html.twig", [
//    'match' => $match,
//]);
?>