<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 11:13
 */

namespace Toolbox;


use JsonSerializable;
use TTTheux\Core\UTF8Management\UTF8Encoder;

abstract class BaseEntity implements JsonSerializable
{
    private $utf8Encoder;

    /**
     * BaseEntity constructor.
     */
    public function __construct()
    {
        $this->utf8Encoder = new UTF8Encoder();
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        foreach ($this->getJSONEncode() as $JSONEncode)
            $this->{'get' . ucwords($JSONEncode)}();
        $properties = $this->getChildVars();
        foreach ($this->getJSONIgnore() as $JSONIgnore)
            unset($properties[$JSONIgnore]);
        foreach ($this->getJSONTransfer() as $from => $to) {
            $properties[$to] = $this->{'get' . ucwords($from)}();
            unset($properties[$from]);
        }
        return $this->utf8Encoder->encode($properties);
    }

    /**
     * @return mixed[], an array of the child properties.
     */
    public abstract function getChildVars();

    /**
     * Specify properties which won't be serialized to JSON,
     * @return array of property names.
     */
    public abstract function getJSONIgnore();

    /**
     * Specify properties which will be serialized to JSON
     * that don't exist yet in the current context, typically
     * properties linked to a foreign key,
     * @return array of property names.
     */
    public abstract function getJSONEncode();

    /**
     * Specify properties which were retrieved from PDO as strings, and
     * need treatment before JSON serialization, typically DateTime object.
     * @return array of property names.
     */
    public abstract function getJSONTransfer();

    /**
     * Take the @param $tabTObject , and map it to the current object
     * following the TabTBindings implemented.
     * @return mixed
     */
    public function getTabTMapping($tabTObject)
    {
        if (is_object($tabTObject)) {
            foreach ($this->getTabTBindings() as $phpProperty => $tabtProperty)
                if (!is_array($tabtProperty)) {
                    if (array_key_exists($tabtProperty, $tabTObject))
                        $this->{'set' . ucwords($phpProperty)}($tabTObject->{$tabtProperty});
                } else {
                    $tabtProperties = [];
                    foreach ($tabtProperty as $property)
                        if (array_key_exists($property, $tabTObject))
                            $tabtProperties[$property] = $tabTObject->{$property};
                    $this->{'set' . ucwords($phpProperty)}($tabtProperties);
                }
        }
        return $this;
    }

    /**
     * Bindings between each property and the TabTObject members to hydrate it.
     * @return mixed[]
     */
    public abstract function getTabTBindings();

    public function getMapping($params)
    {
        foreach ($params as $key => $param)
            $this->{'set'.$key}($param);
        return $this;
    }
}