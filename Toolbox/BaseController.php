<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 11:21
 */

namespace Toolbox;


use ReflectionClass;

abstract class BaseController
{
    protected $repo;

    public function __construct()
    {
        try {
            $reflector = new ReflectionClass($this->getRepositoryName());
            $this->repo = $reflector->newInstance();
        } catch (\ReflectionException $e) {
        }
    }

    abstract public function getRepositoryName();

    public function getAll($params = null)
    {
        return $this->repo->getAll($params);
    }

    public function get($params = null)
    {
        if (array_key_exists('id', $params))
            return $this->repo->getByID($params['id']);
        else
            return $this->repo->getByID(0);
    }

    public function add()
    {
        $entity = null;
        try {
            $reflector = new ReflectionClass($this->repo->getEntityName());
            $entity = $reflector->newInstance();
            $entity->getMapping($_POST);
        } catch (\ReflectionException $e) {
        }
        if ($entity != null)
            return $this->repo->insert($entity);
        else
            return -1;
    }
}