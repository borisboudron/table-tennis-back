<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 19-09-18
 * Time: 14:42
 */

namespace Toolbox;

use Exception;
use TTTheux\Controllers\PlayerController;

class Router
{
    public function handleRoute($twig, $get, $post)
    {
//        $url = $_SERVER['REQUEST_URI'];
//        var_dump($url);
        try
        {
            if (isset($get["controller"]))
            {
                $controllerName = $get["controller"];
                $className = "\\TTTheux\\Controllers\\" . ucWords($controllerName) . "Controller";
                $controller = new $className($twig);
                if(isset($get["method"]))
                    if(isset($get["id"]))
                        if(empty($post))
                        {
                            if(isset($get["season"]))
                                return $controller->{$get["method"]}($get["id"], $get["season"]);
                            elseif ($get["controller"] === "interclubs")
                                return $controller->{$get["method"]}($get["id"], 19);
                            else
                                return $controller->{$get["method"]}($get["id"]);
                        }
                        else
                            return $controller->{$get["method"]}($get["id"], $post);
                    else
                        if(empty($post))
                            return $controller->{$get["method"]}();
                        else
                            return $controller->{$get["method"]}($post);
            }
            else
            {
                return $twig->render("homePage.html.twig", []);
            }
        }
        catch (Exception $e)
        {
            return $e->getMessage();
        }
    }
}