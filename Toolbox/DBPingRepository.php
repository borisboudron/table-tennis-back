<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 21-11-18
 * Time: 14:07
 */

namespace Toolbox;


use SoapClient;
use TTTheux\Core\DB\TabTConnector;

class DBPingRepository
{
    private $Credentials;
    private $tabt;

    public function __construct()
    {
        // Ouverture de la connection Soap
        $this->tabt = TabTConnector::getInstance();
        $this->Credentials = TabTConnector::getCredentials();
    }

    /**
     * @return mixed
     */
    public function getCredentials()
    {
        return $this->Credentials;
    }

    /**
     * @param mixed $Credentials
     * @return DBPingRepository
     */
    public function setCredentials($Credentials)
    {
        $this->Credentials = $Credentials;
        return $this;
    }

    /**
     * @return SoapClient
     */
    public function getTabt()
    {
        return $this->tabt;
    }

    /**
     * @param SoapClient $tabt
     * @return DBPingRepository
     */
    public function setTabt($tabt)
    {
        $this->tabt = $tabt;
        return $this;
    }

    // Retrieve all divisions of a given level
    public function getDivisions($level)
    {
        $GetDivisionsRequest = array('Credentials' => $this->Credentials
        ,'Level' => $level
        ,'ShowDivisionName' => 'yes'
        );
        return $this->tabt->GetDivisions($GetDivisionsRequest);
    }

    // Retrieve a division of a given level with a given name
    public function getDivision($level, $name)
    {
        $GetDivisionsRequest = array('Credentials' => $this->Credentials
        ,'Level' => $level
        ,'ShowDivisionName' => 'yes'
        );
        $divisions = $this->tabt->GetDivisions($GetDivisionsRequest);
        $division = null;
        foreach ($divisions->DivisionEntries as $d)
            if ($d->DivisionName === $name)
                $division = $d;
        return $division;
    }

    //Retrieve the division level of a given division
    public function getDivisionLevel($division)
    {
        return substr($division->DivisionName, 9, 1);
    }

    //Retrieve the division serie of a given division
    public function getDivisionSerie($division)
    {
        return substr($division->DivisionName, 10, 1);
    }

    // Retrieve a given match by its clubId (home or away) and its uniqueId
    public function getMatches($clubId)
    {
        $GetMatchesRequest = array('Credentials' => $this->Credentials
        ,'Club'  => $clubId
        ,'ShowDivisionName' => 'yes'
        ,'WithDetails' => true
        );
        return $this->tabt->GetMatches($GetMatchesRequest)->TeamMatchesEntries;
    }

    // Retrieve a given match by its uniqueId
    public function getMatch($id, $season = null)
    {
        $GetMatchesRequest = array('Credentials' => $this->Credentials
        ,'Club' => 'L184'
        ,'ShowDivisionName' => 'yes'
        ,'WithDetails' => true
        );
        if (!is_null($season))
        {
            $GetMatchesRequest['Season'] = $season;
            $GetMatchesRequest['MatchId'] = $id;
        }
        else
            $GetMatchesRequest['MatchUniqueId'] = $id;
        return $this->tabt->GetMatches($GetMatchesRequest)->TeamMatchesEntries;
    }

    // Retrieve club infos ; for venue
    public function getClubByUniqueID($uniqueID)
    {
        $GetClubRequest = array('Credentials' => $this->Credentials
        , 'Club' => $uniqueID
        );
        return $this->tabt->GetClubs($GetClubRequest)->ClubEntries;
    }

    // Retrieve venue for a given interclubs
    public function getVenue($interclubs)
    {
        $homeClub = $this->getClubByUniqueID($interclubs->HomeClub);
        $venueId = $interclubs->Venue;

        $venueEntries = $homeClub->VenueEntries;
        $venue = null;
        if (is_object($venueEntries))
            $venue = $venueEntries;
        else {
            $found = false;
            $c = 0;
            while ((!$found) && ($c < count($venueEntries))) {
                if ($venueId == $venueEntries[$c]->ClubVenue)
                    $found = true;
                else
                    $c++;
            }
            if ($found)
                $venue = $venueEntries[$c];
        }
        return $venue;
    }

    // Retrieve the list of players of the interclubs, home players go from 1 to 4, away players from 5 to 8
    public function getPlayersFromInterclubs($interclubs)
    {
        $players = [];
        for ($i = 0; $i < 4; $i++)
            $players[$i + 1] = $interclubs->MatchDetails->HomePlayers->Players[ceil(count($interclubs->MatchDetails->HomePlayers->Players)/4) * $i];
        for ($i = 0; $i < 4; $i++)
            $players[$i + 5] = $interclubs->MatchDetails->AwayPlayers->Players[ceil(count($interclubs->MatchDetails->AwayPlayers->Players)/4) * $i];
        return $players;
    }

    // Retrive active players from a given club, filters inactive and comitee members
    public function getPlayersFromClub($club)
    {
        $GetMembersRequest = array('Credentials' => $this->Credentials
                                    ,'Club'  => $club
                                    ,'ExtendedInformation'  => true
                                    );
        $ResponseMembers = $this->tabt->GetMembers($GetMembersRequest);
        $members = [];
        $nonPlayers = 0;
        if (is_object($ResponseMembers->MemberEntries))
        {
            $member = $ResponseMembers->MemberEntries;
            if ($member->Status === 'A')
                $members[] = $member;
        }
        else
            foreach ($ResponseMembers->MemberEntries as $member)
            {
                if ($member->Status === 'A') {
                    $member->Position = $member->Position - $nonPlayers;
                    $members[] = $member;
                } else
                    $nonPlayers++;
            }
        return $members;
    }

    // Retrieve player indexes
    public function getPlayerIndexes($player, $members)
    {
        $response = [
            'ClubPosition' => '',
            'ClubIndex' => '',
        ];
        $found = false;
        $c = 0;
        while ((!$found) && ($c < count($members))) {
            if ($members[$c]->UniqueIndex == $player->UniqueIndex)
                $found = true;
            else
                $c++;
        }
        if ($found)
        {
            $response['ClubPosition'] = $members[$c]->Position;
            $response['ClubIndex'] = $members[$c]->RankingIndex;
        }
        return $response;
    }

    public function getPlayer($id)
    {
        $GetMembersRequest = array('Credentials' => $this->Credentials
        ,'UniqueIndex'  => $id
        ,'ExtendedInformation'  => true
        );
        $ResponseMembers = $this->tabt->GetMembers($GetMembersRequest);
        return $ResponseMembers;
    }
}