<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 12-09-18
 * Time: 10:38
 */

namespace Toolbox;

use PDO;
use TTTheux\core\DB\TabTConnector;

abstract class BaseRepository implements IRepository
{
    protected $pdo;
    protected $tabt;
    protected $Credentials;

    public function __construct()
    {
        $this->tabt = TabTConnector::getInstance();
        $this->Credentials = TabTConnector::getCredentials();
        $this->pdo = new PDO('mysql:host=localhost;dbname=tttheux;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    }

    protected abstract function getTableName();

    protected abstract function getPKBinding();

    protected abstract function getUKBindings();

    protected abstract function getEntityName();

    protected abstract function getBindings();

    public function getAll()
    {
        $bindings = $this->getBindings();
        $items = [];
        $query = 'SELECT ';
        foreach ($this->getPKBinding() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($this->getUKBindings() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($bindings as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM ';
        $query .= $this->getTableName();
        $response = $this->pdo->query($query);
        while ($item = $response->fetchObject($this->getEntityName()))
            $items[] = $item;
        return $items;
    }

    public function getByID($id)
    {
        $query = 'SELECT ';
        foreach ($this->getPKBinding() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($this->getUKBindings() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($this->getBindings() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM ';
        $query .= $this->getTableName();
        $query .= ' WHERE ';
        $query .= key($this->getPKBinding());
        $query .= ' = :id';
        $response = $this->pdo->prepare($query);
        $response->execute(array(
            ':id' => $id,
        ));
        $item = $response->fetchObject($this->getEntityName());
        return $item;
    }

    public function getByUKs($item)
    {
        $ukBindings = $this->getUKBindings();
        $query = 'SELECT ';
        foreach ($this->getPKBinding() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($ukBindings as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($this->getBindings() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM ';
        $query .= $this->getTableName();
        $query .= ' WHERE ';
        foreach ($ukBindings as $key => $value) {
            $query .= $key;
            $query .= ' = :';
            $query .= $key;
            $query .= ' AND ';
        }
        $query = substr($query, 0, -5);

        $tab = [];
        foreach ($ukBindings as $key => $value)
            $tab[":" . $key] = $item->{"get" . ucwords($value)}();

        $response = $this->pdo->prepare($query);
        $response->execute($tab);
        $item = $response->fetchObject($this->getEntityName());
        return $item;
    }

    public function insert($item, $isPKAutoIncremented = true)
    {
        $pkBinding = $this->getPKBinding();
        $ukBindings = $this->getUKBindings();
        $bindings = $this->getBindings();

        $query = 'INSERT INTO ';
        $query .= $this->getTableName();
        $query .= ' (';
        if (!$isPKAutoIncremented && count($pkBinding) > 0) {
            $query .= implode(', ', array_keys($pkBinding));
        }
        if (count($ukBindings) > 0) {
            if (!$isPKAutoIncremented && count($pkBinding) > 0)
                $query .= ', ';
            $query .= implode(', ', array_keys($ukBindings));
        }
        if (count($bindings) > 0) {
            if ((!$isPKAutoIncremented && count($pkBinding) > 0) || count($ukBindings) > 0)
                $query .= ', ';
            $query .= implode(', ', array_keys($bindings));
        }
        $query .= ') VALUES (';
        if (!$isPKAutoIncremented && count($pkBinding) > 0) {
            $query .= ':';
            $query .= implode(', :', array_keys($pkBinding));
        }
        if (count($ukBindings) > 0) {
            if (!$isPKAutoIncremented && count($pkBinding) > 0)
                $query .= ', ';
            $query .= ':';
            $query .= implode(', :', array_keys($ukBindings));
        }
        if (count($bindings) > 0) {
            if ((!$isPKAutoIncremented && count($pkBinding) > 0 || count($ukBindings) > 0))
                $query .= ', ';
            $query .= ':';
            $query .= implode(', :', array_keys($bindings));
        }
        $query .= ')';

        $response = $this->pdo->prepare($query);
        $tab = [];
        if (!$isPKAutoIncremented)
            foreach ($pkBinding as $key => $value)
                $tab[":" . $key] = $item->{"get" . ucwords($value)}();
        foreach ($ukBindings as $key => $value)
            $tab[":" . $key] = $item->{"get" . ucwords($value)}();
        foreach ($bindings as $key => $value)
            $tab[":" . $key] = $item->{"get" . ucwords($value)}();
        $response->execute($tab);
        return $this->pdo->lastInsertId();
    }

    public function insertWithID($item)
    {
        $pkBinding = $this->getPKBinding();
        $ukBindings = $this->getUKBindings();
        $bindings = $this->getBindings();

        $query = 'INSERT INTO ';
        $query .= $this->getTableName();
        $query .= '(';
        $query .= implode(',', array_keys($pkBinding));
        $query .= ',';
        $query .= implode(',', array_keys($ukBindings));
        $query .= ',';
        $query .= implode(',', array_keys($bindings));
        $query .= ') VALUES (:';
        $query .= implode(',:', array_keys($pkBinding));
        $query .= ',:';
        $query .= implode(',:', array_keys($ukBindings));
        $query .= ',:';
        $query .= implode(',:', array_keys($bindings));
        $query .= ')';
        $response = $this->pdo->prepare($query);
        $tab = [];
        foreach ($pkBinding as $key => $value)
            $tab[":" . $key] = $item->{"get" . ucwords($value)}();
        foreach ($ukBindings as $key => $value)
            $tab[":" . $key] = $item->{"get" . ucwords($value)}();
        foreach ($bindings as $key => $value)
            $tab[":" . $key] = $item->{"get" . ucwords($value)}();
        return $response->execute($tab);
    }

    public function insertWithPK($entity)
    {
        $pkBindings = $this->getPKBinding();
        $bindings = $this->getBindings();

        $query = 'INSERT INTO ';
        $query .= $this->getTableName();
        $query .= '(';
        $query .= implode(',', array_keys($pkBindings));
        $query .= ',';
        $query .= implode(',', array_keys($bindings));
        $query .= ') VALUES (:';
        $query .= implode(',:', array_keys($pkBindings));
        $query .= ',:';
        $query .= implode(',:', array_keys($bindings));
        $query .= ')';
        $response = $this->pdo->prepare($query);
        $tab = [];
        foreach ($pkBindings as $key => $value)
            $tab[":" . $key] = $entity->{"get" . ucwords($value)}();
        foreach ($bindings as $key => $value)
            $tab[":" . $key] = $entity->{"get" . ucwords($value)}();
        $response->execute($tab);
        return $this->pdo->lastInsertId();
    }

    public function update($id, $entity)
    {
        $bindings = $this->getBindings();
        $ukBindings = $this->getUKBindings();
        $pkBinding = $this->getPKBinding();
        $query = 'UPDATE ';
        $query .= $this->getTableName();
        $query .= ' SET ';
        foreach ($ukBindings as $key => $value) {
            $query .= $key;
            $query .= " = ";
            $query .= ':';
            $query .= $key;
            $query .= ', ';
        }
        foreach ($bindings as $key => $value) {
            $query .= $key;
            $query .= " = ";
            $query .= ':';
            $query .= $key;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' WHERE ';
        $query .= key($pkBinding);
        $query .= ' = :';
        $query .= key($pkBinding);
        $response = $this->pdo->prepare($query);
        $tab = [];
        foreach ($pkBinding as $key => $value)
            $tab[":" . $key] = $id;
        foreach ($ukBindings as $key => $value)
            $tab[":" . $key] = $entity->{"get" . ucwords($value)}();
        foreach ($bindings as $key => $value)
            $tab[":" . $key] = $entity->{"get" . ucwords($value)}();
        return $response->execute($tab);
    }

    public function put($id, $entity)
    {
        $tab = [];
        $bindings = $this->getBindings();
        $ukBindings = $this->getUKBindings();
        $pkBinding = $this->getPKBinding();
        $query = 'UPDATE ';
        $query .= $this->getTableName();
        $query .= ' SET ';
        foreach ($ukBindings as $phpVariableName => $dbColumnName) {
            if ($entity->{"get" . ucwords($phpVariableName)}() !== null) {
                $query .= $dbColumnName . ' = :' . $phpVariableName . ', ';
                $tab[':' . $phpVariableName] = ($entity->{"get" . ucwords($phpVariableName)}() !== null && $entity->{"get" . ucwords($phpVariableName)}() !== '') ? $entity->{"get" . ucwords($phpVariableName)}() : null;
            }
        }
        foreach ($bindings as $phpVariableName => $dbColumnName) {
            if ($entity->{"get" . ucwords($phpVariableName)}() !== null) {
                $query .= $dbColumnName . ' = :' . $phpVariableName . ', ';
                $tab[':' . $phpVariableName] = ($entity->{"get" . ucwords($phpVariableName)}() !== null && $entity->{"get" . ucwords($phpVariableName)}() !== '') ? $entity->{"get" . ucwords($phpVariableName)}() : null;
            }
        }
        $query = substr($query, 0, -2);
        $query .= ' WHERE ';
        foreach ($pkBinding as $phpVariableName => $dbColumnName) {
            $query .= $dbColumnName . ' = :' . $phpVariableName . ' AND ';
            $tab[":" . $phpVariableName] = $entity->{"get" . ucwords($dbColumnName)}();
        }
        $query = substr($query, 0, -5);
        $response = $this->pdo->prepare($query);
        foreach ($pkBinding as $key => $value)
            $tab[":" . $key] = $entity->{"get" . ucwords($value)}();
        return $response->execute($tab);
//        return -1;
    }

    public function delete($entityId)
    {
        $query = 'DELETE FROM ';
        $query .= $this->getTableName();
        $query .= ' WHERE ';
        $query .= key($this->getPKBinding());
        $query .= ' = :id';
        $response = $this->pdo->prepare($query);
        return $response->execute(array(
            ':id' => $entityId,
        ));
    }
}