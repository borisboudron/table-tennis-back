<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 27-11-18
 * Time: 14:26
 */

namespace Toolbox;


interface IRepository
{
    function getAll();
    function getByID($id);
    function getByUKs($entity);
    function insert($entity);
    function put($id, $entity);
    function delete($id);
}