<?php

/*
 * This file is part of Twig.
 *
 * (c) Fabien Potencier
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Twig_Tests_Loader_FilesystemTest extends \PHPUnit\Framework\TestCase
{
    public function testGetSourceContext()
    {
        $path = __DIR__.'/../Fixtures';
        $loader = new Twig_Loader_Filesystem(array($path));
        try {
            $this->assertEquals('errors/index.html', $loader->getSourceContext('errors/index.html')->getName());
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $this->assertEquals(realpath($path . '/errors/index.html'), realpath($loader->getSourceContext('errors/index.html')->getPath()));
        } catch (Twig_Error_Loader $e) {
        }
    }

    /**
     * @dataProvider getSecurityTests
     */
    public function testSecurity($template)
    {
        $loader = new Twig_Loader_Filesystem(array(__DIR__.'/../Fixtures'));

        try {
            $loader->getCacheKey($template);
            $this->fail();
        } catch (Twig_Error_Loader $e) {
            $this->assertNotContains('Unable to find template', $e->getMessage());
        }
    }

    public function getSecurityTests()
    {
        return array(
            array("AutoloaderTest\0.php"),
            array('..\\AutoloaderTest.php'),
            array('..\\\\\\AutoloaderTest.php'),
            array('../AutoloaderTest.php'),
            array('..////AutoloaderTest.php'),
            array('./../AutoloaderTest.php'),
            array('.\\..\\AutoloaderTest.php'),
            array('././././././../AutoloaderTest.php'),
            array('.\\./.\\./.\\./../AutoloaderTest.php'),
            array('foo/../../AutoloaderTest.php'),
            array('foo\\..\\..\\AutoloaderTest.php'),
            array('foo/../bar/../../AutoloaderTest.php'),
            array('foo/bar/../../../AutoloaderTest.php'),
            array('filters/../../AutoloaderTest.php'),
            array('filters//..//..//AutoloaderTest.php'),
            array('filters\\..\\..\\AutoloaderTest.php'),
            array('filters\\\\..\\\\..\\\\AutoloaderTest.php'),
            array('filters\\//../\\/\\..\\AutoloaderTest.php'),
            array('/../AutoloaderTest.php'),
        );
    }

    /**
     * @dataProvider getBasePaths
     */
    public function testPaths($basePath, $cacheKey, $rootPath)
    {
        $loader = new Twig_Loader_Filesystem(array($basePath.'/normal', $basePath.'/normal_bis'), $rootPath);
        $loader->setPaths(array($basePath.'/named', $basePath.'/named_bis'), 'named');
        try {
            $loader->addPath($basePath . '/named_ter', 'named');
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $loader->addPath($basePath . '/normal_ter');
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $loader->prependPath($basePath . '/normal_final');
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $loader->prependPath($basePath . '/named/../named_quater', 'named');
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $loader->prependPath($basePath . '/named_final', 'named');
        } catch (Twig_Error_Loader $e) {
        }

        $this->assertEquals(array(
            $basePath.'/normal_final',
            $basePath.'/normal',
            $basePath.'/normal_bis',
            $basePath.'/normal_ter',
        ), $loader->getPaths());
        $this->assertEquals(array(
            $basePath.'/named_final',
            $basePath.'/named/../named_quater',
            $basePath.'/named',
            $basePath.'/named_bis',
            $basePath.'/named_ter',
        ), $loader->getPaths('named'));

        // do not use realpath here as it would make the test unuseful
        try {
            $this->assertEquals($cacheKey, str_replace('\\', '/', $loader->getCacheKey('@named/named_absolute.html')));
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $this->assertEquals("path (final)\n", $loader->getSourceContext('index.html')->getCode());
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $this->assertEquals("path (final)\n", $loader->getSourceContext('@__main__/index.html')->getCode());
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $this->assertEquals("named path (final)\n", $loader->getSourceContext('@named/index.html')->getCode());
        } catch (Twig_Error_Loader $e) {
        }
    }

    public function getBasePaths()
    {
        return array(
            array(
                __DIR__.'/Fixtures',
                'test/Twig/Tests/Loader/Fixtures/named_quater/named_absolute.html',
                null,
            ),
            array(
                __DIR__.'/Fixtures/../Fixtures',
                'test/Twig/Tests/Loader/Fixtures/named_quater/named_absolute.html',
                null,
            ),
            array(
                'test/Twig/Tests/Loader/Fixtures',
                'test/Twig/Tests/Loader/Fixtures/named_quater/named_absolute.html',
                getcwd(),
            ),
            array(
                'Fixtures',
                'Fixtures/named_quater/named_absolute.html',
                getcwd().'/test/Twig/Tests/Loader',
            ),
            array(
                'Fixtures',
                'Fixtures/named_quater/named_absolute.html',
                getcwd().'/test/../test/Twig/Tests/Loader',
            ),
        );
    }

    public function testEmptyConstructor()
    {
        $loader = new Twig_Loader_Filesystem();
        $this->assertEquals(array(), $loader->getPaths());
    }

    public function testGetNamespaces()
    {
        $loader = new Twig_Loader_Filesystem(sys_get_temp_dir());
        $this->assertEquals(array(Twig_Loader_Filesystem::MAIN_NAMESPACE), $loader->getNamespaces());

        try {
            $loader->addPath(sys_get_temp_dir(), 'named');
        } catch (Twig_Error_Loader $e) {
        }
        $this->assertEquals(array(Twig_Loader_Filesystem::MAIN_NAMESPACE, 'named'), $loader->getNamespaces());
    }

    public function testFindTemplateExceptionNamespace()
    {
        $basePath = __DIR__.'/Fixtures';

        $loader = new Twig_Loader_Filesystem(array($basePath.'/normal'));
        try {
            $loader->addPath($basePath . '/named', 'named');
        } catch (Twig_Error_Loader $e) {
        }

        try {
            $loader->getSourceContext('@named/nowhere.html');
        } catch (Exception $e) {
            $this->assertInstanceof('Twig_Error_Loader', $e);
            $this->assertContains('Unable to find template "@named/nowhere.html"', $e->getMessage());
        }
    }

    public function testFindTemplateWithCache()
    {
        $basePath = __DIR__.'/Fixtures';

        $loader = new Twig_Loader_Filesystem(array($basePath.'/normal'));
        try {
            $loader->addPath($basePath . '/named', 'named');
        } catch (Twig_Error_Loader $e) {
        }

        // prime the cache for index.html in the named namespace
        try {
            $namedSource = $loader->getSourceContext('@named/index.html')->getCode();
        } catch (Twig_Error_Loader $e) {
        }
        $this->assertEquals("named path\n", $namedSource);

        // get index.html from the main namespace
        try {
            $this->assertEquals("path\n", $loader->getSourceContext('index.html')->getCode());
        } catch (Twig_Error_Loader $e) {
        }
    }

    public function testLoadTemplateAndRenderBlockWithCache()
    {
        $loader = new Twig_Loader_Filesystem(array());
        try {
            $loader->addPath(__DIR__ . '/Fixtures/themes/theme2');
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $loader->addPath(__DIR__ . '/Fixtures/themes/theme1');
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $loader->addPath(__DIR__ . '/Fixtures/themes/theme1', 'default_theme');
        } catch (Twig_Error_Loader $e) {
        }

        $twig = new Twig_Environment($loader);

        try {
            $template = $twig->loadTemplate('blocks.html.twig');
        } catch (Twig_Error_Loader $e) {
        } catch (Twig_Error_Runtime $e) {
        } catch (Twig_Error_Syntax $e) {
        }
        $this->assertSame('block from theme 1', $template->renderBlock('b1', array()));

        try {
            $template = $twig->loadTemplate('blocks.html.twig');
        } catch (Twig_Error_Loader $e) {
        } catch (Twig_Error_Runtime $e) {
        } catch (Twig_Error_Syntax $e) {
        }
        $this->assertSame('block from theme 2', $template->renderBlock('b2', array()));
    }

    public function getArrayInheritanceTests()
    {
        return array(
            'valid array inheritance' => array('array_inheritance_valid_parent.html.twig'),
            'array inheritance with null first template' => array('array_inheritance_null_parent.html.twig'),
            'array inheritance with empty first template' => array('array_inheritance_empty_parent.html.twig'),
            'array inheritance with non-existent first template' => array('array_inheritance_nonexistent_parent.html.twig'),
        );
    }

    /**
     * @dataProvider getArrayInheritanceTests
     *
     * @param $templateName string Template name with array inheritance
     */
    public function testArrayInheritance($templateName)
    {
        $loader = new Twig_Loader_Filesystem(array());
        try {
            $loader->addPath(__DIR__ . '/Fixtures/inheritance');
        } catch (Twig_Error_Loader $e) {
        }

        $twig = new Twig_Environment($loader);

        try {
            $template = $twig->loadTemplate($templateName);
        } catch (Twig_Error_Loader $e) {
        } catch (Twig_Error_Runtime $e) {
        } catch (Twig_Error_Syntax $e) {
        }
        $this->assertSame('VALID Child', $template->renderBlock('body', array()));
    }

    public function testLoadTemplateFromPhar()
    {
        $loader = new Twig_Loader_Filesystem(array());
        // phar-sample.phar was created with the following script:
        // $f = new Phar('phar-test.phar');
        // $f->addFromString('hello.twig', 'hello from phar');
        try {
            $loader->addPath('phar://' . __DIR__ . '/Fixtures/phar/phar-sample.phar');
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $this->assertSame('hello from phar', $loader->getSourceContext('hello.twig')->getCode());
        } catch (Twig_Error_Loader $e) {
        }
    }
}
