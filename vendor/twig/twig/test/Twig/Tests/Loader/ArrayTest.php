<?php

/*
 * This file is part of Twig.
 *
 * (c) Fabien Potencier
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Twig_Tests_Loader_ArrayTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @expectedException Twig_Error_Loader
     */
    public function testGetSourceContextWhenTemplateDoesNotExist()
    {
        $loader = new Twig_Loader_Array(array());

        $loader->getSourceContext('foo');
    }

    public function testGetCacheKey()
    {
        $loader = new Twig_Loader_Array(array('foo' => 'bar'));

        try {
            $this->assertEquals('foo:bar', $loader->getCacheKey('foo'));
        } catch (Twig_Error_Loader $e) {
        }
    }

    public function testGetCacheKeyWhenTemplateHasDuplicateContent()
    {
        $loader = new Twig_Loader_Array(array(
            'foo' => 'bar',
            'baz' => 'bar',
        ));

        try {
            $this->assertEquals('foo:bar', $loader->getCacheKey('foo'));
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $this->assertEquals('baz:bar', $loader->getCacheKey('baz'));
        } catch (Twig_Error_Loader $e) {
        }
    }

    public function testGetCacheKeyIsProtectedFromEdgeCollisions()
    {
        $loader = new Twig_Loader_Array(array(
            'foo__' => 'bar',
            'foo' => '__bar',
        ));

        try {
            $this->assertEquals('foo__:bar', $loader->getCacheKey('foo__'));
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $this->assertEquals('foo:__bar', $loader->getCacheKey('foo'));
        } catch (Twig_Error_Loader $e) {
        }
    }

    /**
     * @expectedException Twig_Error_Loader
     */
    public function testGetCacheKeyWhenTemplateDoesNotExist()
    {
        $loader = new Twig_Loader_Array(array());

        $loader->getCacheKey('foo');
    }

    public function testSetTemplate()
    {
        $loader = new Twig_Loader_Array(array());
        $loader->setTemplate('foo', 'bar');

        try {
            $this->assertEquals('bar', $loader->getSourceContext('foo')->getCode());
        } catch (Twig_Error_Loader $e) {
        }
    }

    public function testIsFresh()
    {
        $loader = new Twig_Loader_Array(array('foo' => 'bar'));
        try {
            $this->assertTrue($loader->isFresh('foo', time()));
        } catch (Twig_Error_Loader $e) {
        }
    }

    /**
     * @expectedException Twig_Error_Loader
     */
    public function testIsFreshWhenTemplateDoesNotExist()
    {
        $loader = new Twig_Loader_Array(array());

        $loader->isFresh('foo', time());
    }
}
