<?php

/*
 * This file is part of Twig.
 *
 * (c) Fabien Potencier
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Twig_Tests_Loader_ChainTest extends \PHPUnit\Framework\TestCase
{
    public function testGetSourceContext()
    {
        $path = __DIR__.'/../Fixtures';
        $loader = new Twig_Loader_Chain(array(
            new Twig_Loader_Array(array('foo' => 'bar')),
            new Twig_Loader_Array(array('errors/index.html' => 'baz')),
            new Twig_Loader_Filesystem(array($path)),
        ));

        try {
            $this->assertEquals('foo', $loader->getSourceContext('foo')->getName());
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $this->assertSame('', $loader->getSourceContext('foo')->getPath());
        } catch (Twig_Error_Loader $e) {
        }

        try {
            $this->assertEquals('errors/index.html', $loader->getSourceContext('errors/index.html')->getName());
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $this->assertSame('', $loader->getSourceContext('errors/index.html')->getPath());
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $this->assertEquals('baz', $loader->getSourceContext('errors/index.html')->getCode());
        } catch (Twig_Error_Loader $e) {
        }

        try {
            $this->assertEquals('errors/base.html', $loader->getSourceContext('errors/base.html')->getName());
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $this->assertEquals(realpath($path . '/errors/base.html'), realpath($loader->getSourceContext('errors/base.html')->getPath()));
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $this->assertNotEquals('baz', $loader->getSourceContext('errors/base.html')->getCode());
        } catch (Twig_Error_Loader $e) {
        }
    }

    /**
     * @expectedException Twig_Error_Loader
     */
    public function testGetSourceContextWhenTemplateDoesNotExist()
    {
        $loader = new Twig_Loader_Chain(array());

        $loader->getSourceContext('foo');
    }

    public function testGetCacheKey()
    {
        $loader = new Twig_Loader_Chain(array(
            new Twig_Loader_Array(array('foo' => 'bar')),
            new Twig_Loader_Array(array('foo' => 'foobar', 'bar' => 'foo')),
        ));

        try {
            $this->assertEquals('foo:bar', $loader->getCacheKey('foo'));
        } catch (Twig_Error_Loader $e) {
        }
        try {
            $this->assertEquals('bar:foo', $loader->getCacheKey('bar'));
        } catch (Twig_Error_Loader $e) {
        }
    }

    /**
     * @expectedException Twig_Error_Loader
     */
    public function testGetCacheKeyWhenTemplateDoesNotExist()
    {
        $loader = new Twig_Loader_Chain(array());

        $loader->getCacheKey('foo');
    }

    public function testAddLoader()
    {
        $loader = new Twig_Loader_Chain();
        $loader->addLoader(new Twig_Loader_Array(array('foo' => 'bar')));

        try {
            $this->assertEquals('bar', $loader->getSourceContext('foo')->getCode());
        } catch (Twig_Error_Loader $e) {
        }
    }

    public function testExists()
    {
        $loader1 = $this->getMockBuilder('Twig_LoaderInterface')->getMock();
        $loader1->expects($this->once())->method('exists')->will($this->returnValue(false));
        $loader1->expects($this->never())->method('getSourceContext');

        $loader2 = $this->getMockBuilder('Twig_LoaderInterface')->getMock();
        $loader2->expects($this->once())->method('exists')->will($this->returnValue(true));
        $loader2->expects($this->never())->method('getSourceContext');

        $loader = new Twig_Loader_Chain();
        $loader->addLoader($loader1);
        $loader->addLoader($loader2);

        $this->assertTrue($loader->exists('foo'));
    }
}
