<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-10-18
 * Time: 16:23
 */

namespace TTTheux\Models;


class Ranking
{
    private $id;
    private $ranking;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Ranking
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRanking()
    {
        return $this->ranking;
    }

    /**
     * @param mixed $ranking
     * @return Ranking
     */
    public function setRanking($ranking)
    {
        $this->ranking = $ranking;
        return $this;
    }
}