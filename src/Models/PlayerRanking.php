<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 17:49
 */

namespace TTTheux\Models;


use TTTheux\Repositories\RankingRepository;

class PlayerRanking
{
    private $id;
    private $playerId;
    private $rankingId;
    private $ranking;
    private $season;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return PlayerRanking
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayerId()
    {
        return $this->playerId;
    }

    /**
     * @param mixed $playerId
     * @return PlayerRanking
     */
    public function setPlayerId($playerId)
    {
        $this->playerId = $playerId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRankingId()
    {
        return $this->rankingId;
    }

    /**
     * @param mixed $rankingId
     * @return PlayerRanking
     */
    public function setRankingId($rankingId)
    {
        $this->rankingId = $rankingId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRanking()
    {
        if ($this->ranking == null) {
            $repo = new RankingRepository();
            $this->ranking = $repo->getByID($this->rankingId);
        }
        return $this->ranking;
    }

    /**
     * @param mixed $ranking
     * @return PlayerRanking
     */
    public function setRanking($ranking)
    {
        $this->ranking = $ranking;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * @param mixed $season
     * @return PlayerRanking
     */
    public function setSeason($season)
    {
        $this->season = $season;
        return $this;
    }
}