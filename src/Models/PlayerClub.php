<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 16:43
 */

namespace TTTheux\Models;


use TTTheux\Repositories\ClubRepository;

class PlayerClub
{
    private $id;
    private $playerId;
    private $clubId;
    private $club;
    private $season;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return PlayerClub
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayerId()
    {
        return $this->playerId;
    }

    /**
     * @param mixed $playerId
     * @return PlayerClub
     */
    public function setPlayerId($playerId)
    {
        $this->playerId = $playerId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClubId()
    {
        return $this->clubId;
    }

    /**
     * @param mixed $clubId
     * @return PlayerClub
     */
    public function setClubId($clubId)
    {
        $this->clubId = $clubId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClub()
    {
        if ($this->club == null) {
            $repo = new ClubRepository();
            $this->club = $repo->getByID($this->playerId);
        }
        return $this->club;
    }

    /**
     * @param mixed $club
     * @return PlayerClub
     */
    public function setClub($club)
    {
        $this->club = $club;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * @param mixed $season
     * @return PlayerClub
     */
    public function setSeason($season)
    {
        $this->season = $season;
        return $this;
    }
}