<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 20:48
 */

namespace TTTheux\Models;


use Toolbox\BaseEntity;
use TTTheux\Repositories\MatchSetRepository;

class Match extends BaseEntity
{
    private $id;
    private $interclubsId;
    private $position;
    private $sets;
    private $homeTotal;
    private $awayTotal;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Match
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInterclubsId()
    {
        return $this->interclubsId;
    }

    /**
     * @param mixed $interclubsId
     * @return Match
     */
    public function setInterclubsId($interclubsId)
    {
        $this->interclubsId = $interclubsId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     * @return Match
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHomeTotal()
    {
        if ($this->homeTotal == null && count($this->getSets()) > 0) {
            $this->homeTotal = 0;
            /** @var Set $set */
            foreach ($this->getSets() as $set)
                if ($set->getHomeScore() > $set->getAwayScore())
                    $this->homeTotal++;
        }
        return $this->homeTotal;
    }

    /**
     * @param mixed $homeTotal
     * @return Match
     */
    public function setHomeTotal($homeTotal)
    {
        $this->homeTotal = $homeTotal;
        return $this;
    }

    /**
     * @return Set[]
     */
    public function getSets()
    {
        if ($this->sets === null) {
            $repo = new MatchSetRepository();
            $matchsets = $repo->getByMatchId($this->id);
            $this->sets = [];
            /** @var MatchSet $ms */
            foreach ($matchsets as $ms) {
                $set = $ms->getSet();
                $set->setPosition($ms->getPosition());
                $this->sets[] = $set;
            }
        }
        return $this->sets;
    }

    /**
     * @param mixed $sets
     * @return Match
     */
    public function setSets($sets)
    {
        $this->sets = $sets;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAwayTotal()
    {
        if ($this->awayTotal == null && count($this->getSets()) > 0) {
            $this->awayTotal = 0;
            /** @var Set $set */
            foreach ($this->getSets() as $set)
                if ($set->getHomeScore() < $set->getAwayScore())
                    $this->awayTotal++;
        }
        return $this->awayTotal;
    }

    /**
     * @param mixed $awayTotal
     * @return Match
     */
    public function setAwayTotal($awayTotal)
    {
        $this->awayTotal = $awayTotal;
        return $this;
    }

    public function getChildVars()
    {
        return get_object_vars($this);
    }

    public function getJSONIgnore()
    {
        return [];
    }

    public function getJSONEncode()
    {
        return [
            'sets',
        ];
    }

    public function getJSONTransfer()
    {
        return [];
    }

    public function getTabTBindings()
    {
        return [
            'position' => 'Position',
            'homeTotal' => 'HomeSetCount',
            'awayTotal' => 'AwaySetCount',
        ];
    }
}