<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-10-18
 * Time: 16:36
 */

namespace TTTheux\Models;


class PlayerSeries
{
    private $playerId;
    private $player;
    private $serieId;
    private $serie;

    /**
     * @return mixed
     */
    public function getPlayerId()
    {
        return $this->playerId;
    }

    /**
     * @param mixed $playerId
     * @return PlayerSeries
     */
    public function setPlayerId($playerId)
    {
        $this->playerId = $playerId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * @param mixed $player
     * @return PlayerSeries
     */
    public function setPlayer($player)
    {
        $this->player = $player;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSerieId()
    {
        return $this->serieId;
    }

    /**
     * @param mixed $serieId
     * @return PlayerSeries
     */
    public function setSerieId($serieId)
    {
        $this->serieId = $serieId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * @param mixed $serie
     * @return PlayerSeries
     */
    public function setSerie($serie)
    {
        $this->serie = $serie;
        return $this;
    }
}