<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 15:53
 */

namespace TTTheux\Models;


use TTTheux\Repositories\InterclubsRepository;
use TTTheux\Repositories\PlayerRepository;

class InterclubsPlayer
{
    private $id;
    private $interclubsId;
    private $interclubs;
    private $playerId;
    private $player;
    private $position;
    private $victories;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return InterclubsPlayer
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInterclubsId()
    {
        return $this->interclubsId;
    }

    /**
     * @param mixed $interclubsId
     * @return InterclubsPlayer
     */
    public function setInterclubsId($interclubsId)
    {
        $this->interclubsId = $interclubsId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInterclubs()
    {
        if ($this->interclubs == null) {
            $repo = new InterclubsRepository();
            $this->interclubs = $repo->getByID($this->interclubsId);
        }
        return $this->interclubs;
    }

    /**
     * @param mixed $interclubs
     * @return InterclubsPlayer
     */
    public function setInterclubs($interclubs)
    {
        $this->interclubs = $interclubs;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayerId()
    {
        return $this->playerId;
    }

    /**
     * @param mixed $playerId
     * @return InterclubsPlayer
     */
    public function setPlayerId($playerId)
    {
        $this->playerId = $playerId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayer()
    {
        if ($this->player == null) {
            $repo = new PlayerRepository();
            $this->player = $repo->getByID($this->playerId);
        }
        return $this->player;
    }

    /**
     * @param mixed $player
     * @return InterclubsPlayer
     */
    public function setPlayer($player)
    {
        $this->player = $player;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     * @return InterclubsPlayer
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVictories()
    {
        return $this->victories;
    }

    /**
     * @param mixed $victories
     * @return InterclubsPlayer
     */
    public function setVictories($victories)
    {
        $this->victories = $victories;
        return $this;
    }
}