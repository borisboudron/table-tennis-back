<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 22-11-18
 * Time: 08:49
 */

namespace TTTheux\Models;


class MatchDetails_DBPing
{
    private $DetailsCreated;
    private $HomePlayers;
    private $AwayPlayers;
    private $IndividualMatchResults;
    private $MatchSystem;
    private $HomeScore;
    private $AwayScore;
    private $CommentCount;
    private $CommentEntries;

    /**
     * @return mixed
     */
    public function getDetailsCreated()
    {
        return $this->DetailsCreated;
    }

    /**
     * @param mixed $DetailsCreated
     * @return MatchDetails_DBPing
     */
    public function setDetailsCreated($DetailsCreated)
    {
        $this->DetailsCreated = $DetailsCreated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHomePlayers()
    {
        return $this->HomePlayers;
    }

    /**
     * @param mixed $HomePlayers
     * @return MatchDetails_DBPing
     */
    public function setHomePlayers($HomePlayers)
    {
        $this->HomePlayers = $HomePlayers;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAwayPlayers()
    {
        return $this->AwayPlayers;
    }

    /**
     * @param mixed $AwayPlayers
     * @return MatchDetails_DBPing
     */
    public function setAwayPlayers($AwayPlayers)
    {
        $this->AwayPlayers = $AwayPlayers;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIndividualMatchResults()
    {
        return $this->IndividualMatchResults;
    }

    /**
     * @param mixed $IndividualMatchResults
     * @return MatchDetails_DBPing
     */
    public function setIndividualMatchResults($IndividualMatchResults)
    {
        $this->IndividualMatchResults = $IndividualMatchResults;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMatchSystem()
    {
        return $this->MatchSystem;
    }

    /**
     * @param mixed $MatchSystem
     * @return MatchDetails_DBPing
     */
    public function setMatchSystem($MatchSystem)
    {
        $this->MatchSystem = $MatchSystem;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHomeScore()
    {
        return $this->HomeScore;
    }

    /**
     * @param mixed $HomeScore
     * @return MatchDetails_DBPing
     */
    public function setHomeScore($HomeScore)
    {
        $this->HomeScore = $HomeScore;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAwayScore()
    {
        return $this->AwayScore;
    }

    /**
     * @param mixed $AwayScore
     * @return MatchDetails_DBPing
     */
    public function setAwayScore($AwayScore)
    {
        $this->AwayScore = $AwayScore;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommentCount()
    {
        return $this->CommentCount;
    }

    /**
     * @param mixed $CommentCount
     * @return MatchDetails_DBPing
     */
    public function setCommentCount($CommentCount)
    {
        $this->CommentCount = $CommentCount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommentEntries()
    {
        return $this->CommentEntries;
    }

    /**
     * @param mixed $CommentEntries
     * @return MatchDetails_DBPing
     */
    public function setCommentEntries($CommentEntries)
    {
        $this->CommentEntries = $CommentEntries;
        return $this;
    }
}