<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-10-18
 * Time: 15:15
 */

namespace TTTheux\Models;


use Toolbox\BaseEntity;
use TTTheux\Repositories\PlayerClubRepository;
use TTTheux\Repositories\PlayerRankingRepository;
use TTTheux\Repositories\PlayerSeasonRepository;

class Player extends BaseEntity
{
    private $id;
    private $order;
    private $index;
    private $lastName;
    private $firstName;
    private $clubId;
    private $clubs;
    private $rankingId;
    private $ranking;
    private $rankings;
    private $series;
    private $playerSeries;
    private $playerSeasons;
    private $training;
    private $teams;
    private $fridayEvening;
    private $saturdayAfternoon;
    private $saturdayEvening;
    private $driver;
    private $backup;
    private $remarks;
    private $position;
    private $victories;

//    public function __construct($id, $lastName, $firstName, $club, $ranking, $series)
//    {
//        $this->id = $id;
//        $this->lastName = $lastName;
//        $this->firstName = $firstName;
//        $this->club = $club;
//        $this->ranking = $ranking;
//        $this->series = $series;
//    }
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Player
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     * @return Player
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return int
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param int $index
     * @return Player
     */
    public function setIndex($index)
    {
        $this->index = $index;
        return $this;
    }

    /**
     * @param Player[] $players
     */
    public static function setOrdersAndIndexes($players)
    {
        $index = 0;
        for ($p = 0; $p < count($players) - 1; $p++) {
            $players[$p]->setOrder($p + 1);
            if ($players[$p]->getRanking() !== $players[$p + 1]->getRanking() && $players[$p + 1]->getRanking() !== 'NC') {
                for ($i = $index; $i <= $p; $i++) {
                    $players[$i]->setIndex($p + 1);
                }
                $index = $p + 1;
            }
        }
        for ($i = $index; $i < count($players); $i++) {
            $players[$i]->setOrder($i + 1);
            $players[$i]->setIndex(count($players));
        }
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return Player
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Player
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClubId()
    {
        return $this->clubId;
    }

    /**
     * @param mixed $clubId
     * @return Player
     */
    public function setClubId($clubId)
    {
        $this->clubId = $clubId;
        return $this;
    }

    /**
     * @return array[Club]
     */
    public function getClubs()
    {
        if ($this->clubs == null) {
            $repo = new PlayerClubRepository();
            $playerclubs = $repo->getByPlayerId($this->id);
            $this->clubs = [];
            /** @var $playerclubs array[PlayerClub] */
            foreach ($playerclubs as $pc)
                $this->clubs[$pc->getSeason()] = $pc->getClub();
        }
        return $this->clubs;
    }

    /**
     * @param array[Club] $clubs
     * @return Player
     */
    public function setClubs($clubs)
    {
        $this->clubs = $clubs;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRankingId()
    {
        return $this->rankingId;
    }

    /**
     * @param mixed $rankingId
     * @return Player
     */
    public function setRankingId($rankingId)
    {
        $this->rankingId = $rankingId;
        return $this;
    }

    /**
     * @return string
     */
    public function getRanking()
    {
        return $this->ranking;
    }

    /**
     * @param string $ranking
     * @return Player
     */
    public function setRanking($ranking)
    {
        $this->ranking = $ranking;
        return $this;
    }

    /**
     * @return array[Ranking]
     */
    public function getRankings()
    {
        if ($this->rankings == null) {
            $repo = new PlayerRankingRepository();
            $playerrankings = $repo->getByPlayerId($this->id);
            $this->rankings = [];
            foreach ($playerrankings as $pr)
                $this->rankings[$pr->getSeason()] = $pr->getRanking();
        }
        return $this->rankings;
    }

    /**
     * @param array[Ranking] $rankings
     * @return Player
     */
    public function setRankings($rankings)
    {
        $this->rankings = $rankings;
        return $this;
    }

    /**
     * @return array[Serie]
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * @param array[Serie] $series
     * @return Player
     */
    public function setSeries($series)
    {
        $this->series = $series;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayerSeries()
    {
        return $this->playerSeries;
    }

    /**
     * @param mixed $playerSeries
     * @return Player
     */
    public function setPlayerSeries($playerSeries)
    {
        $this->playerSeries = $playerSeries;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayerSeasons()
    {
        if ($this->playerSeasons == null) {
            $repo = new PlayerSeasonRepository();
            $playerseasons = $repo->getByPlayerId($this->id);
            $this->playerSeasons = [];
            foreach ($playerseasons as $ps)
                $this->playerSeasons[$ps->getSeason()] = $ps;
        }
        return $this->playerSeasons;
    }

    /**
     * @param mixed $playerSeasons
     * @return Player
     */
    public function setPlayerSeasons($playerSeasons)
    {
        $this->playerSeasons = $playerSeasons;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTraining()
    {
        return $this->training;
    }

    /**
     * @param mixed $training
     * @return Player
     */
    public function setTraining($training)
    {
        $this->training = $training;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTeams()
    {
        return $this->teams;
    }

    /**
     * @param mixed $teams
     * @return Player
     */
    public function setTeams($teams)
    {
        $this->teams = $teams;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFridayEvening()
    {
        return $this->fridayEvening;
    }

    /**
     * @param mixed $fridayEvening
     * @return Player
     */
    public function setFridayEvening($fridayEvening)
    {
        $this->fridayEvening = $fridayEvening;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSaturdayAfternoon()
    {
        return $this->saturdayAfternoon;
    }

    /**
     * @param mixed $saturdayAfternoon
     * @return Player
     */
    public function setSaturdayAfternoon($saturdayAfternoon)
    {
        $this->saturdayAfternoon = $saturdayAfternoon;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSaturdayEvening()
    {
        return $this->saturdayEvening;
    }

    /**
     * @param mixed $saturdayEvening
     * @return Player
     */
    public function setSaturdayEvening($saturdayEvening)
    {
        $this->saturdayEvening = $saturdayEvening;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @param mixed $driver
     * @return Player
     */
    public function setDriver($driver)
    {
        $this->driver = $driver;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBackup()
    {
        return $this->backup;
    }

    /**
     * @param boolean $backup
     * @return Player
     */
    public function setBackup($backup)
    {
        $this->backup = $backup;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * @param mixed $remarks
     * @return Player
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     * @return Player
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVictories()
    {
        return $this->victories;
    }

    /**
     * @param mixed $victories
     * @return Player
     */
    public function setVictories($victories)
    {
        $this->victories = $victories;
        return $this;
    }

    public function getChildVars()
    {
        return get_object_vars($this);
    }

    public function getJSONEncode()
    {
        return [];
    }

    public function getJSONIgnore()
    {
        return [];
    }

    public function getJSONTransfer()
    {
        return [];
    }

    public function getTabTBindings()
    {
        return [
            'id' => 'UniqueIndex',
            'lastName' => 'LastName',
            'firstName' => 'FirstName',
            'ranking' => 'Ranking',
            'position' => 'Position',
            'victories' => 'VictoryCount',
        ];
    }
}