<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 14:25
 */

namespace TTTheux\Models;


use Toolbox\BaseEntity;
use TTTheux\Repositories\CategoryRepository;

class League extends BaseEntity
{
    private $id;
    private $division;
    private $serie;
    private $categoryId;
    private $category;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return League
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * @param mixed $division
     * @return League
     */
    public function setDivision($division)
    {
        $this->division = $division;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * @param mixed $serie
     * @return League
     */
    public function setSerie($serie)
    {
        $this->serie = $serie;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param mixed $categoryId
     * @return League
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        if ($this->category === null) {
            $repo = new CategoryRepository();
            $this->category = $repo->getByID($this->categoryId);
        }
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return League
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    public function getChildVars()
    {
        return get_object_vars($this);
    }

    public function getJSONIgnore()
    {
        return [
            'categoryId',
        ];
    }

    public function getJSONEncode()
    {
        return [
            'category',
        ];
    }

    public function getJSONTransfer()
    {
        return [];
    }

    public function getTabTBindings()
    {
        return [
            'division' => 'Division',
            'serie' => 'Serie',
            'categoryId' => 'CategoryId',
        ];
    }
}