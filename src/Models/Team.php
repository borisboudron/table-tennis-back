<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 15:37
 */

namespace TTTheux\Models;


use Toolbox\BaseEntity;
use TTTheux\Repositories\ClubRepository;

class Team extends BaseEntity
{
    private $id;
    private $clubId;
    private $club;
    private $letter;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Team
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClubId()
    {
        return $this->clubId;
    }

    /**
     * @param mixed $clubId
     * @return Team
     */
    public function setClubId($clubId)
    {
        $this->clubId = $clubId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClub()
    {
        if ($this->club == null && $this->clubId !== null) {
            $repo = new ClubRepository();
            $this->club = $repo->getByID($this->clubId);
        }
        return $this->club;
    }

    /**
     * @param mixed $club
     * @return Team
     */
    public function setClub($club)
    {
        if (is_array($club)) {
            $realClub = new Club();
            $realClub = $realClub->getTabTMapping((object)$club);
            $this->club = $realClub;
        } else
            $this->club = $club;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLetter()
    {
        return $this->letter;
    }

    /**
     * @param mixed $letter
     * @return Team
     */
    public function setLetter($letter)
    {
        $this->letter = $letter;
        return $this;
    }

    public function getChildVars()
    {
        return get_object_vars($this);
    }

    public function getJSONIgnore()
    {
        return [
            'clubId',
        ];
    }

    public function getJSONEncode()
    {
        return [
            'club',
        ];
    }

    public function getJSONTransfer()
    {
        return [];
    }

    public function getTabTBindings()
    {
        return [
            'club' => ['ClubIndex', 'TeamName'],
            'letter' => 'TeamLetter',
        ];
    }
}