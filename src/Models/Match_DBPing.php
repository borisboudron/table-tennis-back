<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 21-11-18
 * Time: 18:33
 */

namespace TTTheux\Models;


class Match_DBPing
{
    private $DivisionName;
    private $MatchId;
    private $WeekName;
    private $Date;
    private $Time;
    private $Venue;
    private $HomeClub;
    private $HomeTeam;
    private $AwayClub;
    private $AwayTeam;
    private $Score;
    private $MatchUniqueId;
    private $NextWeekName;
    private $IsHomeForfeited;
    private $IsAwayForfeited;
    private $MatchDetails;
    private $DivisionId;
    private $DivisionCategory;
    private $IsHomeWithdrawn;
    private $IsAwayWithdrawn;

    /**
     * @return mixed
     */
    public function getDivisionName()
    {
        return $this->DivisionName;
    }

    /**
     * @param mixed $DivisionName
     * @return Match_DBPing
     */
    public function setDivisionName($DivisionName)
    {
        $this->DivisionName = $DivisionName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMatchId()
    {
        return $this->MatchId;
    }

    /**
     * @param mixed $MatchId
     * @return Match_DBPing
     */
    public function setMatchId($MatchId)
    {
        $this->MatchId = $MatchId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWeekName()
    {
        return $this->WeekName;
    }

    /**
     * @param mixed $WeekName
     * @return Match_DBPing
     */
    public function setWeekName($WeekName)
    {
        $this->WeekName = $WeekName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->Date;
    }

    /**
     * @param mixed $Date
     * @return Match_DBPing
     */
    public function setDate($Date)
    {
        $this->Date = $Date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->Time;
    }

    /**
     * @param mixed $Time
     * @return Match_DBPing
     */
    public function setTime($Time)
    {
        $this->Time = $Time;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVenue()
    {
        return $this->Venue;
    }

    /**
     * @param mixed $Venue
     * @return Match_DBPing
     */
    public function setVenue($Venue)
    {
        $this->Venue = $Venue;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHomeClub()
    {
        return $this->HomeClub;
    }

    /**
     * @param mixed $HomeClub
     * @return Match_DBPing
     */
    public function setHomeClub($HomeClub)
    {
        $this->HomeClub = $HomeClub;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHomeTeam()
    {
        return $this->HomeTeam;
    }

    /**
     * @param mixed $HomeTeam
     * @return Match_DBPing
     */
    public function setHomeTeam($HomeTeam)
    {
        $this->HomeTeam = $HomeTeam;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAwayClub()
    {
        return $this->AwayClub;
    }

    /**
     * @param mixed $AwayClub
     * @return Match_DBPing
     */
    public function setAwayClub($AwayClub)
    {
        $this->AwayClub = $AwayClub;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAwayTeam()
    {
        return $this->AwayTeam;
    }

    /**
     * @param mixed $AwayTeam
     * @return Match_DBPing
     */
    public function setAwayTeam($AwayTeam)
    {
        $this->AwayTeam = $AwayTeam;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getScore()
    {
        return $this->Score;
    }

    /**
     * @param mixed $Score
     * @return Match_DBPing
     */
    public function setScore($Score)
    {
        $this->Score = $Score;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMatchUniqueId()
    {
        return $this->MatchUniqueId;
    }

    /**
     * @param mixed $MatchUniqueId
     * @return Match_DBPing
     */
    public function setMatchUniqueId($MatchUniqueId)
    {
        $this->MatchUniqueId = $MatchUniqueId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNextWeekName()
    {
        return $this->NextWeekName;
    }

    /**
     * @param mixed $NextWeekName
     * @return Match_DBPing
     */
    public function setNextWeekName($NextWeekName)
    {
        $this->NextWeekName = $NextWeekName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisHomeForfeited()
    {
        return $this->IsHomeForfeited;
    }

    /**
     * @param mixed $IsHomeForfeited
     * @return Match_DBPing
     */
    public function setIsHomeForfeited($IsHomeForfeited)
    {
        $this->IsHomeForfeited = $IsHomeForfeited;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisAwayForfeited()
    {
        return $this->IsAwayForfeited;
    }

    /**
     * @param mixed $IsAwayForfeited
     * @return Match_DBPing
     */
    public function setIsAwayForfeited($IsAwayForfeited)
    {
        $this->IsAwayForfeited = $IsAwayForfeited;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMatchDetails()
    {
        return $this->MatchDetails;
    }

    /**
     * @param mixed $MatchDetails
     * @return Match_DBPing
     */
    public function setMatchDetails($MatchDetails)
    {
        $this->MatchDetails = $MatchDetails;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDivisionId()
    {
        return $this->DivisionId;
    }

    /**
     * @param mixed $DivisionId
     * @return Match_DBPing
     */
    public function setDivisionId($DivisionId)
    {
        $this->DivisionId = $DivisionId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDivisionCategory()
    {
        return $this->DivisionCategory;
    }

    /**
     * @param mixed $DivisionCategory
     * @return Match_DBPing
     */
    public function setDivisionCategory($DivisionCategory)
    {
        $this->DivisionCategory = $DivisionCategory;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisHomeWithdrawn()
    {
        return $this->IsHomeWithdrawn;
    }

    /**
     * @param mixed $IsHomeWithdrawn
     * @return Match_DBPing
     */
    public function setIsHomeWithdrawn($IsHomeWithdrawn)
    {
        $this->IsHomeWithdrawn = $IsHomeWithdrawn;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisAwayWithdrawn()
    {
        return $this->IsAwayWithdrawn;
    }

    /**
     * @param mixed $IsAwayWithdrawn
     * @return Match_DBPing
     */
    public function setIsAwayWithdrawn($IsAwayWithdrawn)
    {
        $this->IsAwayWithdrawn = $IsAwayWithdrawn;
        return $this;
    }

    public function set($data)
    {
        foreach ($data AS $key => $value) $this->{$key} = $value;
    }
}