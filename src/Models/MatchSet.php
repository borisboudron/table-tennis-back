<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 21:52
 */

namespace TTTheux\Models;


use TTTheux\Repositories\SetRepository;

class MatchSet
{
    private $id;
    private $matchId;
    private $setId;
    private $set;
    private $position;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return MatchSet
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMatchId()
    {
        return $this->matchId;
    }

    /**
     * @param mixed $matchId
     * @return MatchSet
     */
    public function setMatchId($matchId)
    {
        $this->matchId = $matchId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSetId()
    {
        return $this->setId;
    }

    /**
     * @param mixed $setId
     * @return MatchSet
     */
    public function setSetId($setId)
    {
        $this->setId = $setId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSet()
    {
        if ($this->set == null && $this->setId !== null) {
            $repo = new SetRepository();
            $this->set = $repo->getByID($this->setId);
        }
        return $this->set;
    }

    /**
     * @param mixed $set
     * @return MatchSet
     */
    public function setSet($set)
    {
        $this->set = $set;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     * @return MatchSet
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }
}