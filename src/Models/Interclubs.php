<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 13:31
 */

namespace TTTheux\Models;


use DateTime;
use Toolbox\BaseEntity;
use TTTheux\Repositories\InterclubsPlayerRepository;
use TTTheux\Repositories\LeagueRepository;
use TTTheux\Repositories\MatchRepository;
use TTTheux\Repositories\PlayerRepository;
use TTTheux\Repositories\VenueRepository;

class Interclubs extends BaseEntity
{
    private $id;
    private $venueId;
    private $venue;
    private $date;
    private $dateString;
    private $leagueId;
    private $league;
    private $interclubsNb;
    private $startTime;
    private $startTimeString;
    private $endTime;
    private $endTimeString;
    private $selection1Id;
    private $selection2Id;
    private $selections;
    private $players;
    private $season;
    private $matches;
    private $playersVictories;
    private $homeCaptainId;
    private $homeCaptain;
    private $awayCaptainId;
    private $awayCaptain;
    private $refereeId;
    private $referee;
    private $score;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Interclubs
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getVenueId()
    {
        return $this->venueId;
    }

    /**
     * @param $venueId int
     * @return Interclubs
     */
    public function setVenueId($venueId)
    {
        if (is_array($venueId) && $this->venueId === null) {
            $venueTabTId = $venueId['Venue'];
            $clubId = $venueId['HomeClub'];
            $repo = new VenueRepository();
            $venue = $repo->getByTabTData($venueTabTId, $clubId);
            $this->venue = $repo->getByUKs($venue);
            if ($this->venue !== false)
                $this->venueId = $this->venue->getId();
            else {
                $this->venueId = $repo->insert($venue);
                $venue->setId($this->venueId);
                $this->venue = $venue;
            }
        } else
            $this->venueId = $venueId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVenue()
    {
//        if ($this->venue === null && $this->venueId !== null) {
//            $repo = new VenueRepository();
//            $this->venue = $repo->getByID($this->venueId);
//        }
        return $this->venue;
    }

    /**
     * @param mixed $venue
     * @return Interclubs
     */
    public function setVenue($venue)
    {
        // Eventually delete the local DB storage of the venue (is its id useful ?)
        if (is_array($venue) && $this->venueId === null) {
            $venueTabTId = $venue['Venue'];
            $clubId = $venue['HomeClub'];
            $repo = new VenueRepository();
            $realVenue = $repo->getByTabTData($venueTabTId, $clubId);
            $this->venue = $repo->getByUKs($realVenue);
            if ($this->venue !== false)
                $this->venueId = $this->venue->getId();
            else {
                $this->venueId = $repo->insert($realVenue);
                $realVenue->setId($this->venueId);
                $this->venue = $realVenue;
            }
        } else
            $this->venue = $venue;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     * @return Interclubs
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateString()
    {
        if ($this->dateString === null && $this->date !== null)
            $this->dateString = (new DateTime($this->date))->format('Y-m-d');
        return $this->dateString;
    }

    /**
     * @param mixed $dateString
     */
    public function setDateString($dateString)
    {
        $this->dateString = $dateString;
    }

    /**
     * @return int
     */
    public function getLeagueId()
    {
        return $this->leagueId;
    }

    /**
     * @param int $leagueId
     * @return Interclubs
     */
    public function setLeagueId($leagueId)
    {
        $this->leagueId = $leagueId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLeague()
    {
//        if ($this->league == null && $this->leagueId !== null) {
//            $repo = new LeagueRepository();
//            $this->league = $repo->getByID($this->leagueId);
//        }
        return $this->league;
    }

    /**
     * @param mixed $league
     * @return Interclubs
     */
    public function setLeague($league)
    {
        // Eventually delete the local DB storage of the league (is its id useful ?)
        if (is_array($league) && $this->league === null) {
            $realLeague = new League();
            $tabTLeague = (object)[
                'Division' => substr($league['DivisionName'], 9, 1),
                'Serie' => substr($league['DivisionName'], 10, 1),
                'CategoryId' => $league['DivisionCategory'],
            ];
            $realLeague = $realLeague->getTabTMapping($tabTLeague);
            $repo = new LeagueRepository();
            $this->league = $repo->getByUKs($realLeague);
            if ($this->league !== false)
                $this->leagueId = $this->league->getId();
            else {
                $this->leagueId = $repo->insert($realLeague);
                $realLeague->setId($this->leagueId);
                $this->league = $realLeague;
            }
        } else
            $this->league = $league;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInterclubsNb()
    {
        return $this->interclubsNb;
    }

    /**
     * @param mixed $interclubsNb
     * @return Interclubs
     */
    public function setInterclubsNb($interclubsNb)
    {
        $this->interclubsNb = $interclubsNb;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param mixed $startTime
     * @return Interclubs
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartTimeString()
    {
        if ($this->startTimeString === null && $this->startTime !== null)
            $this->startTimeString = (new DateTime($this->startTime))->format('H:i');
        return $this->startTimeString;
    }

    /**
     * @param mixed $startTimeString
     */
    public function setStartTimeString($startTimeString)
    {
        $this->startTimeString = $startTimeString;
    }

    /**
     * @return mixed
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @param mixed $endTime
     * @return Interclubs
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
        $this->endTimeString = $endTime->format('H:i');
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndTimeString()
    {
        if ($this->endTimeString === null && $this->endTime !== null)
            $this->endTimeString = (new DateTime($this->endTime))->format('H:i');
        return $this->endTimeString;
    }

    /**
     * @param mixed $endTimeString
     */
    public function setEndTimeString($endTimeString)
    {
        $this->endTimeString = $endTimeString;
    }

    /**
     * @return mixed
     */
    public function getSelection1Id()
    {
        return $this->selection1Id;
    }

    /**
     * @param mixed $selection1Id
     * @return Interclubs
     */
    public function setSelection1Id($selection1Id)
    {
        $this->selection1Id = $selection1Id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSelection2Id()
    {
        return $this->selection2Id;
    }

    /**
     * @param mixed $selection2Id
     * @return Interclubs
     */
    public function setSelection2Id($selection2Id)
    {
        $this->selection2Id = $selection2Id;
        return $this;
    }

    /**
     * @return array[Team]
     */
    public function getSelections()
    {
//        if ($this->selections === null && $this->selection1Id !== null && $this->selection2Id !== null) {
//            $repo = new SelectionRepository();
//            for ($i = 1; $i <= 2; $i++) {
//                $home = ($i === 1) ? 'Home' : 'Away';
//                $selection = $repo->getById($this->{'selection' . $i . 'Id'});
//                $selection->SetHomeOrAway($home);
//                $this->selections[] = $selection;
//            }
//        }
        return $this->selections;
    }

    /**
     * @param array[Team] $teams
     * @return Interclubs
     */
    public function setSelections($selections)
    {
        if (array_key_exists('WeekName', $selections) && $this->selections === null) {
            $home = 'Home';
            while ($home !== '') {
                $selection = new Selection();
                $tabTSelection = (object)[
                    'WeekName' => $selections['WeekName'],
                    'ClubIndex' => $selections[$home . 'Club'],
                    'TeamName' => substr($selections[$home . 'Team'], 0, strlen($selections[$home . 'Team']) - 2),
                    'TeamLetter' => substr($selections[$home . 'Team'], strlen($selections[$home . 'Team']) - 1),
                    'Players' => $selections['MatchDetails']->{$home . 'Players'}->Players,
                ];
                $selection = $selection->getTabTMapping($tabTSelection);
                $selection->setHomeOrAway($home);
                $this->selections[] = $selection;
                $home = ($home === 'Home') ? 'Away' : '';
            }
        } else
            $this->selections = $selections;
        return $this;
    }

    /**
     * @return array[Player]
     */
    public function getPlayers()
    {
        if ($this->players == null) {
            $repo = new InterclubsPlayerRepository();
            $interclubsplayers = $repo->getByInterclubsId($this->id);
            $this->players = [];
            foreach ($interclubsplayers as $ip)
                $this->players[$ip->getPosition()] = $ip->getPlayer();
        }
        return $this->players;
    }

    /**
     * @param array[Player] $players
     * @return Interclubs
     */
    public function setPlayers($players)
    {
        $this->players = $players;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSeason()
    {
        if ($this->season === null) {
            $this->season = substr(date_parse($this->getDate())['year'], 2, 2);
            if (date_parse($this->getDate())['month'] > 6)
                $this->season++;
        }
        return $this->season;
    }

    /**
     * @param mixed $season
     * @return Interclubs
     */
    public function setSeason($season)
    {
        $this->season = $season;
        return $this;
    }

    /**
     * @return array[Match]
     */
    public function getMatches()
    {
        return $this->matches;
    }

    /**
     * @param array[Match] $matches
     * @return Interclubs
     */
    public function setMatches($matches)
    {
        if (is_array($matches) && array_key_exists('MatchUniqueId', $matches)) {
            $repo = new MatchRepository();
            $realMatches = $repo->getByInterclubsId($matches['MatchUniqueId']);
            if (!$realMatches)
                $realMatches = [];
            foreach ($matches['MatchDetails']->IndividualMatchResults as $tabTMatch) {
                $found = false;
                $m = 0;
                while (!$found && $m < count($realMatches)) {
                    // PDO only sends back strings
                    if ($realMatches[$m]->getPosition() == $tabTMatch->Position) {
                        $realMatches[$m] = $realMatches[$m]->getTabTMapping($tabTMatch);
                        $found = true;
                    } else
                        $m++;
                }
                if (!$found)
                {
                    $match = (new Match())->setInterclubsId($matches['MatchUniqueId'])->setPosition($tabTMatch->Position);
                    $match->setId($repo->insert($match));
                    $realMatches[] = $match->getTabTMapping($tabTMatch);
                }
            }
            $this->matches = $realMatches;
        } else
            $this->matches = $matches;
        return $this;
    }

    public function getMatchesByPosition()
    {
        $repo = new MatchRepository();
        $this->matches = $repo->getByInterclubsId($this->id);
        return $this->matches;
    }

    /**
     * @return array[int]
     */
    public function getPlayersVictories()
    {
        if ($this->playersVictories == null) {
            $this->playersVictories = [];
            $this->players = $this->getPlayers();
            foreach ($this->getMatches() as $match) {
                $homePlayer = 0;
                $awayPlayer = 0;
                // Récupérer qui joue selon la catégorie
                switch ($this->getLeague()->getCategoryId()) {
                    case 1:
                        $homePlayer = 5 - ($match->getPosition() % 4);
                        switch ($match->getPosition()) {
                            case 1:
                            case 6:
                            case 12:
                            case 15:
                                $awayPlayer = 6;
                                break;
                            case 2:
                            case 5:
                            case 11:
                            case 16:
                                $awayPlayer = 5;
                                break;
                            case 3:
                            case 8:
                            case 10:
                            case 13:
                                $awayPlayer = 8;
                                break;
                            case 4:
                            case 7:
                            case 9:
                            case 14:
                                $awayPlayer = 7;
                                break;
                            default:
                                $homePlayer = 0;
                                $awayPlayer = 0;
                                break;
                        }
                        break;
                    case 2:
                    case 3:
                    case 4:
                        switch ($match->getPosition()) {
                            case 1:
                                $homePlayer = 3;
                                $awayPlayer = 6;
                                break;
                            case 6:
                                $homePlayer = 1;
                                $awayPlayer = 6;
                                break;
                            case 9:
                                $homePlayer = 2;
                                $awayPlayer = 6;
                                break;
                            case 2:
                                $homePlayer = 2;
                                $awayPlayer = 5;
                                break;
                            case 4:
                                $homePlayer = 3;
                                $awayPlayer = 5;
                                break;
                            case 10:
                                $homePlayer = 1;
                                $awayPlayer = 5;
                                break;
                            case 3:
                                $homePlayer = 1;
                                $awayPlayer = 7;
                                break;
                            case 5:
                                $homePlayer = 2;
                                $awayPlayer = 7;
                                break;
                            case 8:
                                $homePlayer = 3;
                                $awayPlayer = 7;
                                break;
                            default:
                                $homePlayer = 0;
                                $awayPlayer = 0;
                                break;
                        }
                        break;
                    default:
                        break;
                }

                if (($homePlayer != 0) && ($awayPlayer != 0)) {
                    if (!array_key_exists($homePlayer, $this->playersVictories))
                        $this->playersVictories[$homePlayer] = 0;
                    if (!array_key_exists($awayPlayer, $this->playersVictories))
                        $this->playersVictories[$awayPlayer] = 0;
                    $match->setSets($match->getSets());
                    if ($match->getHomeTotal() > $match->getAwayTotal())
                        $this->playersVictories[$homePlayer]++;
                    elseif ($match->getHomeTotal() < $match->getAwayTotal())
                        $this->playersVictories[$awayPlayer]++;
                }
            }
        }
        return $this->playersVictories;
    }

    /**
     * @param array[int] $playersVictories
     * @return Interclubs
     */
    public function setPlayersVictories($playersVictories)
    {
        $this->playersVictories = $playersVictories;
        return $this;
    }

    /**
     * @return int
     */
    public function getHomeCaptainId()
    {
        return $this->homeCaptainId;
    }

    /**
     * @param int $homeCaptainId
     * @return Interclubs
     */
    public function setHomeCaptainId($homeCaptainId)
    {
        $this->homeCaptainId = $homeCaptainId;
        return $this;
    }

    /**
     * @return Player
     */
    public function getHomeCaptain()
    {
        if ($this->homeCaptain == null && $this->homeCaptainId !== null) {
            $repo = new PlayerRepository();
            $this->homeCaptain = $repo->getByID($this->homeCaptainId);
        }
        return $this->homeCaptain;
    }

    /**
     * @param Player $homeCaptain
     * @return Interclubs
     */
    public function setHomeCaptain($homeCaptain)
    {
        $this->homeCaptain = $homeCaptain;
        return $this;
    }

    /**
     * @return int
     */
    public function getAwayCaptainId()
    {
        return $this->awayCaptainId;
    }

    /**
     * @param int $awayCaptainId
     * @return Interclubs
     */
    public function setAwayCaptainId($awayCaptainId)
    {
        $this->awayCaptainId = $awayCaptainId;
        return $this;
    }

    /**
     * @return Player
     */
    public function getAwayCaptain()
    {
        if ($this->awayCaptain == null && $this->awayCaptainId !== null) {
            $repo = new PlayerRepository();
            $this->awayCaptain = $repo->getByID($this->awayCaptainId);
        }
        return $this->awayCaptain;
    }

    /**
     * @param Player $awayCaptain
     * @return Interclubs
     */
    public function setAwayCaptain($awayCaptain)
    {
        $this->awayCaptain = $awayCaptain;
        return $this;
    }

    /**
     * @return int
     */
    public function getRefereeId()
    {
        return $this->refereeId;
    }

    /**
     * @param int $refereeId
     * @return Interclubs
     */
    public function setRefereeId($refereeId)
    {
        $this->refereeId = $refereeId;
        return $this;
    }

    /**
     * @return Player
     */
    public function getReferee()
    {
        if ($this->referee == null && $this->refereeId !== null) {
            $repo = new PlayerRepository();
            $this->referee = $repo->getByID($this->refereeId);
        }
        return $this->referee;
    }

    /**
     * @param Player $referee
     * @return Interclubs
     */
    public function setReferee($referee)
    {
        $this->referee = $referee;
        return $this;
    }

    /**
     * @return string
     */
    public function getScore()
    {
        if ($this->score == null) {
            $homeScore = 0;
            $awayScore = 0;
            $matches = $this->getMatches();
            foreach ($matches as $match) {
                if ($match->getHomeTotal() > $match->getAwayTotal())
                    $homeScore++;
                elseif ($match->getHomeTotal() < $match->getAwayTotal())
                    $awayScore++;
            }
            if (($homeScore != 0) || ($awayScore != 0))
                $this->score = $homeScore . '-' . $awayScore;
        }
        return $this->score;
    }

    /**
     * @param string $score
     * @return Interclubs
     */
    public function setScore($score)
    {
        $this->score = $score;
        return $this;
    }

    public function getChildVars()
    {
        return get_object_vars($this);
    }

    public function getJSONIgnore()
    {
        return [
            'venueId',
            'leagueId',
            'selection1Id',
            'selection2Id',
            'homeCaptainId',
            'awayCaptainId',
            'refereeId',
        ];
    }

    public function getJSONEncode()
    {
        return [
            'venue',
            'league',
            'selections',
            'homeCaptain',
            'awayCaptain',
            'referee',
        ];
    }

    public function getJSONTransfer()
    {
        return [
            'dateString' => 'date',
            'startTimeString' => 'startTime',
            'endTimeString' => 'endTime',
        ];
    }

    public function getTabTBindings()
    {
        return [
            'id' => 'MatchUniqueId',
            'interclubsNb' => 'MatchId',
            'date' => 'Date',
            'startTime' => 'Time',
            'venue' => ['Venue', 'HomeClub'],
            'league' => ['DivisionName', 'DivisionCategory'],
            'selections' => ['WeekName', 'HomeTeam', 'AwayTeam', 'HomeClub', 'AwayClub', 'MatchDetails'],
            'matches' => ['MatchUniqueId', 'MatchDetails'],
        ];
    }
}