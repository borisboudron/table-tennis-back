<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 14:19
 */

namespace TTTheux\Models;


use Toolbox\BaseEntity;
use TTTheux\Repositories\CityRepository;

class Venue extends BaseEntity
{
    private $id;
    private $building;
    private $address;
    private $cityId;
    private $city;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Venue
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * @param mixed $building
     * @return Venue
     */
    public function setBuilding($building)
    {
        $this->building = $building;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return Venue
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * @param $cityString
     * @return Venue
     */
    public function setCityId($cityString)
    {
        if ($this->cityId === null && $cityString !== null) {
            $pos = strpos($cityString, ' ');
            $city = new City();
            $city->setPostalCode(substr($cityString, 0, $pos));
            $city->setName(substr($cityString, $pos + 1));
            $repo = new CityRepository();
            $this->city = $repo->getByUKs($city);
            if ($this->city !== false)
                $this->cityId = $this->city->getId();
            else {
                $this->cityId = $repo->insert($city);
                $city->setId($this->cityId);
                $this->city = $city;
            }
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        if ($this->city == null) {
            $repo = new CityRepository();
            $this->city = $repo->getByID($this->getCityId());
        }
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Venue
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    public function getChildVars()
    {
        return get_object_vars($this);
    }

    public function getJSONIgnore()
    {
        return [
            'cityId',
            'cityString',
        ];
    }

    public function getJSONEncode()
    {
        return [
            'city',
        ];
    }

    public function getJSONTransfer()
    {
        return [];
    }

    public function getTabTBindings()
    {
        return [
            'building' => 'Name',
            'address' => 'Street',
            'cityId' => 'Town',
        ];
    }
}