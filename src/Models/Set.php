<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 10-10-18
 * Time: 18:26
 */

namespace TTTheux\Models;


use Toolbox\BaseEntity;

class Set extends BaseEntity
{
    private $id;
    private $position;
    private $homeScore;
    private $awayScore;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Set
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getHomeScore()
    {
        return $this->homeScore;
    }

    /**
     * @param mixed $homeScore
     * @return Set
     */
    public function setHomeScore($homeScore)
    {
        $this->homeScore = $homeScore;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAwayScore()
    {
        return $this->awayScore;
    }

    /**
     * @param mixed $awayScore
     * @return Set
     */
    public function setAwayScore($awayScore)
    {
        $this->awayScore = $awayScore;
        return $this;
    }

    public function getChildVars()
    {
        return get_object_vars($this);
    }

    public function getJSONIgnore()
    {
        return [];
    }

    public function getJSONEncode()
    {
        return [];
    }

    public function getJSONTransfer()
    {
        return [];
    }

    public function getTabTBindings()
    {
        return [];
    }
}