<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-10-18
 * Time: 16:26
 */

namespace TTTheux\Models;


use Toolbox\BaseEntity;

class Club extends BaseEntity
{
    private $id;
    private $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Club
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Club
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getChildVars()
    {
        return get_object_vars($this);
    }

    public function getJSONIgnore()
    {
        return [];
    }

    public function getJSONEncode()
    {
        return [];
    }

    public function getJSONTransfer()
    {
        return [];
    }

    public function getTabTBindings()
    {
        return [
            'id' => 'ClubIndex',
            'name' => 'TeamName',
        ];
    }
}