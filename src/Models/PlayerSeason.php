<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 30-10-18
 * Time: 21:47
 */

namespace TTTheux\Models;


use TTTheux\Repositories\ClubRepository;
use TTTheux\Repositories\RankingRepository;

class PlayerSeason
{
    private $id;
    private $playerId;
    private $season;
    private $clubId;
    private $club;
    private $rankingId;
    private $ranking;
    private $orderNb;
    private $indexNb;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return PlayerSeason
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getPlayerId()
    {
        return $this->playerId;
    }

    /**
     * @param int $playerId
     * @return PlayerSeason
     */
    public function setPlayerId($playerId)
    {
        $this->playerId = $playerId;
        return $this;
    }

    /**
     * @return int
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * @param int $season
     * @return PlayerSeason
     */
    public function setSeason($season)
    {
        $this->season = $season;
        return $this;
    }

    /**
     * @return int
     */
    public function getClubId()
    {
        return $this->clubId;
    }

    /**
     * @param int $clubId
     * @return PlayerSeason
     */
    public function setClubId($clubId)
    {
        $this->clubId = $clubId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClub()
    {
        if ($this->club == null) {
            $repo = new ClubRepository();
            $this->club = $repo->getByID($this->playerId);
        }
        return $this->club;
    }

    /**
     * @param mixed $club
     * @return PlayerSeason
     */
    public function setClub($club)
    {
        $this->club = $club;
        return $this;
    }

    /**
     * @return int
     */
    public function getRankingId()
    {
        return $this->rankingId;
    }

    /**
     * @param int $rankingId
     * @return PlayerSeason
     */
    public function setRankingId($rankingId)
    {
        $this->rankingId = $rankingId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRanking()
    {
        if ($this->ranking == null) {
            $repo = new RankingRepository();
            $this->ranking = $repo->getByID($this->rankingId);
        }
        return $this->ranking;
    }

    /**
     * @param mixed $ranking
     * @return PlayerSeason
     */
    public function setRanking($ranking)
    {
        $this->ranking = $ranking;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrderNb()
    {
        return $this->orderNb;
    }

    /**
     * @param int $orderNb
     * @return PlayerSeason
     */
    public function setOrderNb($orderNb)
    {
        $this->orderNb = $orderNb;
        return $this;
    }

    /**
     * @return int
     */
    public function getIndexNb()
    {
        return $this->indexNb;
    }

    /**
     * @param int $indexNb
     * @return PlayerSeason
     */
    public function setIndexNb($indexNb)
    {
        $this->indexNb = $indexNb;
        return $this;
    }
}