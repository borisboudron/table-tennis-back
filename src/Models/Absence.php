<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 22-12-18
 * Time: 18:16
 */

namespace TTTheux\Models;


use DateTime;
use Toolbox\BaseEntity;
use TTTheux\Repositories\PlayerRepository;

class Absence extends BaseEntity
{
    private $playerId;
    private $player;
    private $weekDay;
    /** @var DateTime $informedDate */
    private $informedDate;
    private $informedDateString;
    private $informedMethod;
    private $reason;

    /**
     * @return mixed
     */
    public function getPlayerId()
    {
        return $this->playerId;
    }

    /**
     * @param mixed $playerId
     * @return Absence
     */
    public function setPlayerId($playerId)
    {
        $this->playerId = $playerId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayer()
    {
        if ($this->player === null) {
            $repo = new PlayerRepository();
            $this->player = $repo->getById2($this->playerId);
        }
        return $this->player;
    }

    /**
     * @param mixed $player
     * @return Absence
     */
    public function setPlayer($player)
    {
        $this->player = $player;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWeekDay()
    {
        return $this->weekDay;
    }

    /**
     * @param mixed $weekDay
     * @return Absence
     */
    public function setWeekDay($weekDay)
    {
        $this->weekDay = $weekDay;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getInformedDate()
    {
        return $this->informedDate;
    }

    /**
     * @param DateTime $informedDate
     * @return Absence
     */
    public function setInformedDate($informedDate)
    {
        $this->informedDate = $informedDate;
        return $this;
    }

    /**
     * @return string|DateTime
     */
    public function getInformedDateString()
    {
        if ($this->informedDateString === null)
            $this->informedDateString = $this->informedDate->format('Y-m-d H:i:s');
        return $this->informedDateString;
    }

    /**
     * @param mixed $informedDateString
     * @return Absence
     */
    public function setInformedDateString($informedDateString)
    {
        $this->informedDateString = $informedDateString;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInformedMethod()
    {
        return $this->informedMethod;
    }

    /**
     * @param mixed $informedMethod
     * @return Absence
     */
    public function setInformedMethod($informedMethod)
    {
        $this->informedMethod = $informedMethod;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param mixed $reason
     * @return Absence
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
        return $this;
    }

    public function getChildVars()
    {
        return get_object_vars($this);
    }

    public function getJSONIgnore()
    {
        return [
            'playerId',
        ];
    }

    public function getJSONEncode()
    {
        return [
            'player',
        ];
    }

    public function getJSONTransfer()
    {
        return [
            'informedDateString' => 'informedDate',
        ];
    }

    public function getTabTBindings()
    {
        return [];
    }
}