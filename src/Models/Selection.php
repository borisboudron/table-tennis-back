<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 25-12-18
 * Time: 01:00
 */

namespace TTTheux\Models;


use Toolbox\BaseEntity;
use TTTheux\Repositories\PlayerRepository;
use TTTheux\Repositories\TeamRepository;

class Selection extends BaseEntity
{
    private $weekDay;
    private $teamId;
    private $player1Id;
    private $player2Id;
    private $player3Id;
    private $player4Id;
    private $team;
    private $players;
    private $homeOrAway;

    /**
     * @return mixed
     */
    public function getWeekDay()
    {
        return $this->weekDay;
    }

    /**
     * @param mixed $weekDay
     * @return Selection
     */
    public function setWeekDay($weekDay)
    {
        if (is_array($weekDay)) {
            var_dump($weekDay);
        } else
            $this->weekDay = $weekDay;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * @param mixed $teamId
     * @return Selection
     */
    public function setTeamId($teamId)
    {
        $this->teamId = $teamId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayer1Id()
    {
        return $this->player1Id;
    }

    /**
     * @param mixed $player1Id
     * @return Selection
     */
    public function setPlayer1Id($player1Id)
    {
        $this->player1Id = $player1Id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayer2Id()
    {
        return $this->player2Id;
    }

    /**
     * @param mixed $player2Id
     * @return Selection
     */
    public function setPlayer2Id($player2Id)
    {
        $this->player2Id = $player2Id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayer3Id()
    {
        return $this->player3Id;
    }

    /**
     * @param mixed $player3Id
     * @return Selection
     */
    public function setPlayer3Id($player3Id)
    {
        $this->player3Id = $player3Id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayer4Id()
    {
        return $this->player4Id;
    }

    /**
     * @param mixed $player4Id
     * @return Selection
     */
    public function setPlayer4Id($player4Id)
    {
        $this->player4Id = $player4Id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHomeOrAway()
    {
        return $this->homeOrAway;
    }

    /**
     * @param mixed $homeOrAway
     * @return Selection
     */
    public function setHomeOrAway($homeOrAway)
    {
        $this->homeOrAway = $homeOrAway;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTeam()
    {
        if ($this->team === null && $this->teamId !== null) {
            $repo = new TeamRepository();
            $this->team = $repo->getById($this->teamId);
        }
        return $this->team;
    }

    /**
     * @param mixed $team
     * @return Selection
     */
    public function setTeam($team)
    {
        if (is_array($team)) {
            $realTeam = new Team();
            $realTeam = $realTeam->getTabTMapping((object)$team);
            $this->setTeam($realTeam);
        } else
            $this->team = $team;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayers()
    {
        if ($this->players === null && $this->player1Id && $this->player2Id && $this->player3Id && $this->player4Id) {
            $repo = new PlayerRepository();
            for ($i = 1; $i <= 4; $i++) {
                $player = $repo->getById($this->{'player' . $i . 'Id'});
                $player->setPosition($i);
                $this->players[] = $player;
            }
        }
        return $this->players;
    }

    public function addPlayer($player)
    {
        $this->players[] = $player;
        return $this;
    }

    /**
     * @param mixed $players
     * @return Selection
     */
    public function setPlayers($players)
    {
        if (is_array($players) && array_key_exists('UniqueIndex', $players[0])) {
            foreach ($players as $tabTPlayer) {
                $player = new Player();
                $player = $player->getTabTMapping($tabTPlayer);
                $this->addPlayer($player);
            }
        } else
            $this->players = $players;
        return $this;
    }

    public function getChildVars()
    {
        return get_object_vars($this);
    }

    public function getJSONIgnore()
    {
        return [
            'teamId',
            'player1Id',
            'player2Id',
            'player3Id',
            'player4Id',
        ];
    }

    public function getJSONEncode()
    {
        return [
            'team',
            'players',
        ];
    }

    public function getJSONTransfer()
    {
        return [];
    }

    public function getTabTBindings()
    {
        return [
            'weekDay' => 'WeekName',
            'team' => ['ClubIndex', 'TeamName', 'TeamLetter'],
            'players' => 'Players',
        ];
    }
}