<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 14:46
 */

namespace TTTheux\Models;


use TTTheux\Repositories\TeamRepository;

class InterclubsTeam
{
    private $id;
    private $interclubsId;
    private $teamId;
    private $team;
    private $position;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return InterclubsTeam
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInterclubsId()
    {
        return $this->interclubsId;
    }

    /**
     * @param mixed $interclubsId
     * @return InterclubsTeam
     */
    public function setInterclubsId($interclubsId)
    {
        $this->interclubsId = $interclubsId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * @param mixed $teamId
     * @return InterclubsTeam
     */
    public function setTeamId($teamId)
    {
        $this->teamId = $teamId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTeam()
    {
        if ($this->team == null) {
            $repo = new TeamRepository();
            $this->team = $repo->getByID($this->teamId);
        }
        return $this->team;
    }

    /**
     * @param mixed $team
     * @return InterclubsTeam
     */
    public function setTeam($team)
    {
        $this->team = $team;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     * @return InterclubsTeam
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }
}