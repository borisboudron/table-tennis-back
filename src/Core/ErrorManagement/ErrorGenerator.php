<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 30-11-18
 * Time: 11:40
 */

namespace TTTheux\Core\ErrorManagement;


class ErrorGenerator
{
    public static function getAllErrors()
    {
        return [
            new Error(404, 'Page non trouvée'),
            new Error(1000, 'La construction de l\'objet {class} a échoué : erreur pour le paramètre \'{param}\'.'),
        ];
    }

    public static function getError($code, $params = null)
    {
        $errors = self::getAllErrors();
        foreach ($errors as $error)
        {
            /** @var Error $error */
            if ($error->getCode() === $code)
            {
                if ($params !== null)
                    foreach ($params as $key => $param)
                        $error->setMessage(str_replace('{' . $key . '}', $param, $error->getMessage()));
                return $error;
            }
        }
        return new Error($code, 'Erreur non gérée');
    }
}