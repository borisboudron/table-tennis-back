<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 17-12-18
 * Time: 18:44
 */

namespace TTTheux\Core\DB;


use SoapClient;

class TabTConnector
{
    private static $tabTInstance = null;
    private static $Credentials = null;

    /**
     * @return null|SoapClient
     */
    public static function getInstance()
    {
        if (!self::$tabTInstance) {
            self::$tabTInstance = new SoapClient("https://api.vttl.be/?wsdl");
        }
        return self::$tabTInstance;
    }

    /**
     * @return array|null
     */
    public static function getCredentials()
    {
        if (!self::$Credentials) {
            $filePath = "./src/Core/DB/Credentials.json";
            self::$Credentials = json_decode(file_get_contents($filePath));
        }
        return self::$Credentials;
    }
}