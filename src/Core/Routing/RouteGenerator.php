<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 17-12-18
 * Time: 19:04
 */

namespace TTTheux\Core\Routing;


use TTTheux\Controllers\AbsenceController;
use TTTheux\Controllers\ClubController;
use TTTheux\Controllers\InterclubsController;
use TTTheux\Controllers\MatchController;
use TTTheux\Controllers\MatchSetController;
use TTTheux\Controllers\PlayerController;
use TTTheux\Controllers\SelectionController;
use TTTheux\Controllers\TeamController;

class RouteGenerator
{
    public static function getAllRoutes()
    {
        return [
            new Route('/clubs', 'GET', 'getAll', ClubController::class),
            new Route('/clubs/:id', 'GET', 'getByID', ClubController::class),
            new Route('/clubs/:id/players', 'GET', 'getAllByClub', PlayerController::class),
            new Route('/clubs/:clubId/teams', 'GET', 'getAllByClub', TeamController::class),
            new Route('/clubs/:clubId/teams2', 'GET', 'getTabTAllByClub', TeamController::class),
            new Route('/clubs/:clubId/:teamLetter/interclubs', 'GET', 'getAllByTeam', InterclubsController::class),
            new Route('/players/:id', 'GET', 'getByID', PlayerController::class),
            new Route('/players/:id', 'PATCH', 'patch', PlayerController::class),
            new Route('/absences', 'GET', 'getAll', AbsenceController::class),
            new Route('/absences', 'POST', 'add', AbsenceController::class),
            new Route('/absences/:id', 'GET', 'get', AbsenceController::class),
            new Route('/absences/:id', 'PATCH', 'patch', AbsenceController::class),
            new Route('/absences/:id', 'DELETE', 'delete', AbsenceController::class),
            new Route('/selections/:weekday', 'GET', 'getAllByWeekDay', SelectionController::class),
            new Route('/selections/:weekday', 'POST', 'addAllByWeekDay', SelectionController::class),
            new Route('/interclubs/:id', 'GET', 'getByID', InterclubsController::class),
            new Route('/interclubs/:id', 'PATCH', 'patch', InterclubsController::class),
            new Route('/matches', 'POST', 'post', MatchController::class),
            new Route('/matches/:matchId/sets', 'POST', 'post', MatchSetController::class),
            new Route('/matches/:matchId/sets/:position', 'PUT', 'put', MatchSetController::class),
//            new Route('/legume', 'POST', 'post', VegetableController::class),
//            new Route('/legume/:id', 'GET', 'get', VegetableController::class),
//            new Route('/legume/:id', 'PUT', 'put', VegetableController::class),
//            new Route('/legume/:id', 'PATCH', 'patch', VegetableController::class),
//            new Route('/legume/:id', 'DELETE', 'delete', VegetableController::class),
//            new Route('/rayon', 'GET', 'getAll', RayonController::class),
//            new Route('/rayon', 'POST', 'post', RayonController::class),
//            new Route('/rayon/:id', 'GET', 'get', RayonController::class),
//            new Route('/rayon/:id', 'PUT', 'put', RayonController::class),
//            new Route('/rayon/:id', 'PATCH', 'patch', RayonController::class),
//            new Route('/rayon/:id', 'DELETE', 'delete', RayonController::class),
        ];
    }
}