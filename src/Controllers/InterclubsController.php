<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 31-10-18
 * Time: 09:15
 */

namespace TTTheux\Controllers;


use DateTime;
use TTTheux\Models\Interclubs;
use TTTheux\Repositories\ClubRepository;
use TTTheux\Repositories\InterclubsRepository;
use TTTheux\Repositories\VenueRepository;
use Toolbox\DBPingRepository;
use Twig_Environment;

class InterclubsController
{
    /** @var Twig_Environment */
    private $twig;
    private $repo;

    public function __construct($twig = null)
    {
        $this->twig = $twig;
        $this->repo = new InterclubsRepository();
    }

    public function index()
    {
        $repo = new InterclubsRepository();
        $list = $repo->getAll();
        try {
            return $this->twig->render("interclubsList.html.twig", [
                "interclubs" => $list,
            ]);
        } catch (\Twig_Error_Loader $e) {
            return $e->getMessage();
        } catch (\Twig_Error_Runtime $e) {
            return $e->getMessage();
        } catch (\Twig_Error_Syntax $e) {
            return $e->getMessage();
        }
    }

    public function indexFromDBPing()
    {
        $repo = new DBPingRepository();
        // Following 2 variables should be passed as parameters of detailsFromDBPing
        $Club = 'L184';

        $list = $repo->getMatches($Club);
        try {
            return $this->twig->render("interclubsList.html.twig", [
                "interclubs" => $list,
            ]);
        } catch (\Twig_Error_Loader $e) {
            return $e->getMessage();
        } catch (\Twig_Error_Runtime $e) {
            return $e->getMessage();
        } catch (\Twig_Error_Syntax $e) {
            return $e->getMessage();
        }
    }

    public function details($interclubsId)
    {
        $repo = new InterclubsRepository();
        $interclubs = $repo->getByID($interclubsId);
        try {
            return $this->twig->render("feuilleMatch.html.twig", [
                "interclubs" => $interclubs,
            ]);
        } catch (\Twig_Error_Loader $e) {
            return $e->getMessage();
        } catch (\Twig_Error_Runtime $e) {
            return $e->getMessage();
        } catch (\Twig_Error_Syntax $e) {
            return $e->getMessage();
        }
    }


    // A retravailler : récupérer les 2 interclubs, checker leur état respectif => 4 états possibles => construire l'objet final (Classe perso) selon les 4 cas

    public function detailsFromDBPing($interclubsId, $season)
    {
        // Récupérer l'interclubs local
        $localInterclubs = new Interclubs();
        $localInterclubs->setInterclubsNb($interclubsId);
        $localInterclubs->setSeason($season);
        $interclubsrepo = new InterclubsRepository();
        /** @var Interclubs $localInterclubs */
        $localInterclubs = $interclubsrepo->getByUKs($localInterclubs);
        $repo = new DBPingRepository();
        $venue = null;
        $players = null;
        $DBPingInterclubs = null;
        if ($localInterclubs !== false && $localInterclubs->getId() !== null) {
            // Récupérer l'interclubs distant
            $DBPingInterclubs = $repo->getMatch($interclubsId, $season);

            // Récupérer le lieu et les joueurs de l'interclubs, et les membres de chaque club (pour indexes)
            $venue = $repo->getVenue($DBPingInterclubs);
            $players = $repo->getPlayersFromInterclubs($DBPingInterclubs);
            $homeMembers = $repo->getPlayersFromClub($DBPingInterclubs->HomeClub);
            $awayMembers = $repo->getPlayersFromClub($DBPingInterclubs->AwayClub);
            // Pour chaque joueur (équipes de 4 ou 3), récupérer ses indexes
            for ($i = 1; $i <= 4; $i++) {
                if (array_key_exists($i, $players)) {
                    $indexes = ($i < 5) ? $repo->getPlayerIndexes($players[$i], $homeMembers) : $repo->getPlayerIndexes($players[$i], $awayMembers);
                    $players[$i]->ClubPosition = $indexes['ClubPosition'];
                    $players[$i]->ClubIndex = $indexes['ClubIndex'];
                    if (array_key_exists('IsForfeited', $players[$i]))
                        $players[$i]->VictoryCount = 'WO';
                }
            }

            // Pour chaque match, vérifier les WO et ajuster les sets
            foreach ($DBPingInterclubs->MatchDetails->IndividualMatchResults as $result) {
                if (array_key_exists('IsHomeForfeited', $result)) {
                    $players[$result->AwayPlayerMatchIndex + 4]->VictoryCount++;
                    $result->HomeSetCount = 0;
                    $result->AwaySetCount = 3;
                }
                if (array_key_exists('IsAwayForfeited', $result)) {
                    $players[$result->HomePlayerMatchIndex]->VictoryCount++;
                    $result->HomeSetCount = 3;
                    $result->AwaySetCount = 0;
                }
            }
        }

        $localInterclubs->setHomeCaptain($repo->getPlayer($localInterclubs->getHomeCaptainId()));
        $localInterclubs->setAwayCaptain($repo->getPlayer($localInterclubs->getAwayCaptainId()));
        $localInterclubs->setReferee($repo->getPlayer($localInterclubs->getRefereeId()));
        $localInterclubs->setPlayers($players);
        $localInterclubs->setVenue($venue);
        $localInterclubs->setLeague(json_decode(json_encode([
            'division' => substr($DBPingInterclubs->DivisionName, 9, 1),
            'serie' => substr($DBPingInterclubs->DivisionName, 10, 1),
            'categoryId' => $DBPingInterclubs->DivisionCategory,
        ], JSON_NUMERIC_CHECK)));
        $localInterclubs->setStartTime($DBPingInterclubs->Time);
        $localInterclubs->setSelections([
            ['club' => $DBPingInterclubs->HomeTeam, 'id' => $DBPingInterclubs->HomeClub],
            ['club' => $DBPingInterclubs->AwayTeam, 'id' => $DBPingInterclubs->AwayClub],
        ]);


        try {
            return $this->twig->render("feuilleMatch.html.twig", [
                "interclubs" => $localInterclubs,
                "players" => $players,
            ]);
        } catch (\Twig_Error_Loader $e) {
            return $e->getMessage();
        } catch (\Twig_Error_Runtime $e) {
            return $e->getMessage();
        } catch (\Twig_Error_Syntax $e) {
            return $e->getMessage();
        }
    }

    public function add()
    {
        $clubrepo = new ClubRepository();
        $clubs = $clubrepo->getAll();
        $venuerepo = new VenueRepository();
        $venues = $venuerepo->getAll();
        try {
            return $this->twig->render("interclubsAdd.html.twig", [
                'clubs' => $clubs,
                'venues' => $venues,
            ]);
        } catch (\Twig_Error_Loader $e) {
            return $e->getMessage();
        } catch (\Twig_Error_Runtime $e) {
            return $e->getMessage();
        } catch (\Twig_Error_Syntax $e) {
            return $e->getMessage();
        }
    }

    public function update($interclubsId)
    {

    }

    public function getByID($params = null)
    {
        if (array_key_exists('id', $params))
            return $this->repo->getByID2($params['id']);
        else
            return $this->repo->getByID2(0);
    }

    public function patch($params = null)
    {
        parse_str(file_get_contents('php://input'), $_PATCH);
        var_dump($params);
        var_dump($_PATCH);
        if (array_key_exists('id', $params) && array_key_exists('hours', $_PATCH) && array_key_exists('minutes', $_PATCH)) {
            $time = DateTime::createFromFormat('h:i', $_PATCH['hours'] . ':' . $_PATCH['minutes']);
            $interclubs = (new Interclubs())->setEndTime($time);
            return $this->repo->put($params['id'], $interclubs);
        }
        return 0;
    }

    public function getAllByTeam($params = null)
    {
        if (array_key_exists('clubId', $params)&&array_key_exists('teamLetter', $params))
            return $this->repo->getTabTAllByTeam($params['clubId'], $params['teamLetter']);
        else
            return -1;
    }
}