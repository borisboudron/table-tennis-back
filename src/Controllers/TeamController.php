<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 24-12-18
 * Time: 20:22
 */

namespace TTTheux\Controllers;


use Toolbox\BaseController;
use TTTheux\Repositories\TeamRepository;

class TeamController extends BaseController
{
    public function getRepositoryName()
    {
        return TeamRepository::class;
    }

    public function getAllByClub($params = null)
    {
        if (array_key_exists('clubId', $params))
            return $this->repo->getAllByClub2($params['clubId']);
        else
            return $this->repo->getAllByClub2(0);
    }

    public function getTabTAllByClub($params = null)
    {
        if (array_key_exists('clubId', $params))
            return $this->repo->getTabTAllByClub($params['clubId']);
        else
            return -1;
    }
}