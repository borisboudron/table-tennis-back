<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 25-12-18
 * Time: 01:18
 */

namespace TTTheux\Controllers;


use Toolbox\BaseController;
use TTTheux\Models\Selection;
use TTTheux\Repositories\SelectionRepository;

class SelectionController extends BaseController
{
    public function getRepositoryName()
    {
        return SelectionRepository::class;
    }

    public function getAllByWeekDay($params = null)
    {
        if (array_key_exists('weekday', $params)) {
            if (array_key_exists('id', $params))
                return $this->repo->getAllByWeekDay($params['weekday'], $params['id']);
            else
                return $this->repo->getAllByWeekDay($params['weekday']);
        } else
            return $this->repo->getAllByWeekDay(0);
    }

    public function addAllByWeekDay($params = null)
    {
        $lastInsertId = -1;
        foreach (json_decode($_POST['selections']) as $selectionArray) {
            $selection = (new Selection())->setWeekDay($params['weekday'])->getMapping($selectionArray);
            $lastInsertId = $this->repo->insert($selection);
        }
        return $lastInsertId;
    }
}