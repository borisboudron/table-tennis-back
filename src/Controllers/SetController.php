<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 10-10-18
 * Time: 18:36
 */

namespace TTTheux\Controllers;


use TTTheux\Models\Set;
use TTTheux\Repositories\SetRepository;
use TTTheux\Utils\TwigExtension;
use Twig_Environment;

class SetController
{
    /** @var Twig_Environment $twig */
    private $twig;

    public function __construct($twig = null)
    {
        $this->twig = $twig;
    }

    public function index()
    {
        $repo = new SetRepository();
        $list = $repo->getAll();
        try {
            return $this->twig->render("matchSetList.html.twig", [
                "matchsets" => $list
            ]);
        } catch (\Twig_Error_Loader $e) {
            return $e;
        } catch (\Twig_Error_Runtime $e) {
            return $e;
        } catch (\Twig_Error_Syntax $e) {
            return $e;
        }
    }

    public function add($post = null)
    {
        $titre = "Nouveau classement";
        $disabled = "";
        $errors = [];
        if (isset($post["submit"])) {
            $matchset = new Set();
            //hydratation
            $matchset->setHomeScore($post["home"]);
            $matchset->setAwayScore($post["away"]);
            //si le formulaire est valide
            $isValid = true;
            $errors = [];
            // Vérification des scores, 2 points d'écart ou l'un des 2 vaut 11.
            if (($post["home"] != 11) && ($post["away"] != 11)) {
                if (($post["home"] > 10) && (abs($post["home"] - $post["away"]) > 2)) {
                    $isValid = false;
                    $errors["home"] = "Score trop élevé";
                } elseif (($post["away"] > 10) && (abs($post["home"] - $post["away"]) > 2)) {
                    $isValid = false;
                    $errors["away"] = "Score trop élevé";
                } elseif ((($post["home"] > 10) || ($post["away"] > 10)) && (abs($post["home"] - $post["away"]) < 2)) {
                    $isValid = false;
                    $errors["homeaway"] = "Ce set n'est pas fini";
                } elseif (($post["home"] < 11) && ($post["away"] < 11)) {
                    $isValid = false;
                    $errors["homeaway"] = "Ce set n'est pas fini";
                }
            }
            if ($isValid) {
                $matchsetrepo = new SetRepository();
                if ($matchsetrepo->insert($matchset))
                    header("Location:" . TwigExtension::path('matchset', 'index'));
                return -1;
            } else {
                try {
                    return $this->twig->render("newMatchSet.html.twig", [
                        "titre" => $titre,
                        "disabled" => $disabled,
                        "matchset" => $matchset,
                        "errors" => $errors,
                    ]);
                } catch (\Twig_Error_Loader $e) {
                    return $e;
                } catch (\Twig_Error_Runtime $e) {
                    return $e;
                } catch (\Twig_Error_Syntax $e) {
                    return $e;
                }
            }
        } else
            try {
                return $this->twig->render("newMatchSet.html.twig", [
                    "titre" => $titre,
                    "disabled" => $disabled,
                    "errors" => $errors,
                ]);
            } catch (\Twig_Error_Loader $e) {
                return $e;
            } catch (\Twig_Error_Runtime $e) {
                return $e;
            } catch (\Twig_Error_Syntax $e) {
                return $e;
            }
    }

    public function delete($id)
    {
        $repo = new SetRepository();
        if ($repo->delete($id))
            header("Location:" . TwigExtension::path('matchset', 'index'));
    }
}