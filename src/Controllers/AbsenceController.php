<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 22-12-18
 * Time: 18:13
 */

namespace TTTheux\Controllers;


use Toolbox\BaseController;
use TTTheux\Models\Absence;
use TTTheux\Repositories\AbsenceRepository;

class AbsenceController extends BaseController
{

    public function getRepositoryName()
    {
        return AbsenceRepository::class;
    }

    public function patch($params = null)
    {
        parse_str(file_get_contents('php://input'), $_PATCH);
        $absence = (new Absence())->getMapping(json_decode($_PATCH['absence']));
        if (array_key_exists('id', $params))
            return $this->repo->put($params['id'], $absence);
        else
            return $this->repo->put(0, $absence);
    }

    public function delete($params = null)
    {
        if (array_key_exists('id', $params))
            return $this->repo->delete($params['id']);
        else
            return $this->repo->delete(0);
    }
}