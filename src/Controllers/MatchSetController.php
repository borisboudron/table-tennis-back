<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 14-01-19
 * Time: 17:44
 */

namespace TTTheux\Controllers;


use Toolbox\BaseController;
use TTTheux\Models\MatchSet;
use TTTheux\Models\Set;
use TTTheux\Repositories\MatchSetRepository;
use TTTheux\Repositories\SetRepository;

class MatchSetController extends BaseController
{

    public function getRepositoryName()
    {
        return MatchSetRepository::class;
    }

    public function post($params = null)
    {
        if (array_key_exists('homeScore', $_POST) && array_key_exists('awayScore', $_POST)) {
            $set = (new Set())->setHomeScore($_POST['homeScore'])->setAwayScore($_POST['awayScore']);
            $setrepo = new SetRepository();
            if (($dbset = $setrepo->getByUKs($set)) === false)
                $setId = $setrepo->insert($set);
            else
                $setId = $dbset->getId();

            if (array_key_exists('matchId', $params) && array_key_exists('position', $_POST)) {
                $msrepo = new MatchSetRepository();
                $matchSet = new MatchSet();
                $matchSet->setMatchId($params['matchId'])->setSetId($setId)->setPosition($_POST['position']);
                return $msrepo->insert($matchSet);
            }
        }
        return -1;
    }

    public function put($params = null)
    {
        parse_str(file_get_contents('php://input'), $_PUT);
        if (array_key_exists('homeScore', $_PUT) && array_key_exists('awayScore', $_PUT)) {
            $set = (new Set())->setHomeScore($_PUT['homeScore'])->setAwayScore($_PUT['awayScore']);
            $setrepo = new SetRepository();
            if (($dbset = $setrepo->getByUKs($set)) === false)
                $setId = $setrepo->insert($set);
            else
                $setId = $dbset->getId();
            if (array_key_exists('matchId', $params) && array_key_exists('position', $params)) {
                $msrepo = new MatchSetRepository();
                $matchSet = new MatchSet();
                $matchSet->setMatchId($params['matchId'])->setSetId($setId)->setPosition($params['position']);
                return $msrepo->updateByUKs($matchSet);
            }
        }
        return -1;
    }
}