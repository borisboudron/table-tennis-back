<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 05-10-18
 * Time: 17:47
 */

namespace TTTheux\Controllers;


use Toolbox\DBPingRepository;
use TTTheux\Models\Club;
use TTTheux\Models\Player;
use TTTheux\Models\Ranking;
use TTTheux\Repositories\ClubRepository;
use TTTheux\Repositories\PlayerRepository;
use TTTheux\Repositories\RankingRepository;
use TTTheux\Utils\TwigExtension;
use Twig_Environment;

class PlayerController
{
    /** @var Twig_Environment $twig */
    private $twig;
    private $repo;

    public function __construct($twig = null)
    {
        $this->twig = $twig;
        $this->repo = new PlayerRepository();
    }

    public function index()
    {
        $list = $this->repo->getAll();
        try {
            return $this->twig->render("playerList.html.twig", [
                "players" => $list
            ]);
        } catch (\Twig_Error_Loader $e) {
            return $e;
        } catch (\Twig_Error_Runtime $e) {
            return $e;
        } catch (\Twig_Error_Syntax $e) {
            return $e;
        }
    }

    public function indexFromDBPing()
    {
        $repo = new DBPingRepository();
        $list = $repo->getPlayersFromClub('L184');
        try {
            return $this->twig->render("playerList.html.twig", [
                "players" => $list
            ]);
        } catch (\Twig_Error_Loader $e) {
            return $e;
        } catch (\Twig_Error_Runtime $e) {
            return $e;
        } catch (\Twig_Error_Syntax $e) {
            return $e;
        }
    }

    public function details($id)
    {
        $repo = new PlayerRepository();
        $prod = $repo->getByID($id);
        try {
            return $this->twig->render("playerDetails.html.twig", [
                "player" => $prod
            ]);
        } catch (\Twig_Error_Loader $e) {
            return $e;
        } catch (\Twig_Error_Runtime $e) {
            return $e;
        } catch (\Twig_Error_Syntax $e) {
            return $e;
        }
    }

    public function detailsFromDBPing($id)
    {
        $repo = new DBPingRepository();
        $prod = $repo->getPlayer($id)->MemberEntries;
        try {
            return $this->twig->render("playerDetails.html.twig", [
                "player" => $prod
            ]);
        } catch (\Twig_Error_Loader $e) {
            return $e;
        } catch (\Twig_Error_Runtime $e) {
            return $e;
        } catch (\Twig_Error_Syntax $e) {
            return $e;
        }
    }

    public function add($post = null)
    {
        $titre = "Nouveau joueur";
        $disabled = "";
        $clubrepo = new ClubRepository();
        $clubs = $clubrepo->getAll();
        asort($clubs);
        $rankingrepo = new RankingRepository();
        $rankings = $rankingrepo->getAll();
        if (isset($post["submit"])) {
            //hydratation
            $player = new Player();
            $player->setId($post["id"]);
            $player->setLastName($post["lastname"]);
            $player->setFirstName($post["firstname"]);
            $player->setClubId($post["club"]);
            $player->setRankingId($post["ranking"]);
            //si le formulaire est valide
            $isValid = true;
            $errors = [];
            // Vérification de l'id, ne peut pas être vide
            if (strlen(trim($post["id"])) == 0) {
                $isValid = false;
                $errors["playerId"] = "Le champ est requis";
            }
            // Vérification du nom, ne peut pas être vide
            if (strlen(trim($post["lastname"])) == 0) {
                $isValid = false;
                $errors["playerLastName"] = "Le champ est requis";
            }
            // Vérification du prénom, ne peut pas être vide
            if (strlen(trim($post["firstname"])) == 0) {
                $isValid = false;
                $errors["playerFirstName"] = "Le champ est requis";
            }
            // Vérification du club, doit appartenir à la liste de choix
            if (!in_array($post["club"], array_map(function ($c) {
                /** @var Club $c */
                return $c->getId();
            }, $clubs))) {
                $isValid = false;
                $errors["playerClub"] = "Le club d'indice " . $post["club"] . " ne figure pas dans la base de données";
            }
            // Vérification du classement, doit appartenir à la liste de choix
            if (!in_array($post["ranking"], array_map(function ($r) {
                /** @var Ranking $r */
                return $r->getId();
            }, $rankings))) {
                $isValid = false;
                $errors["playerRanking"] = "Le classement d'indice " . $post["ranking"] . " ne figure pas dans la base de données";
            }
            if ($isValid) {
                $playerrepo = new PlayerRepository();
                if ($playerrepo->insertWithID($player))
                    header("Location:" . TwigExtension::path('player', 'index'));
                return -1;
            } else {
                try {
                    return $this->twig->render("newPlayer.html.twig", [
                        "titre" => $titre,
                        "disabled" => $disabled,
                        "clubs" => $clubs,
                        "rankings" => $rankings,
                        "errors" => $errors
                    ]);
                } catch (\Twig_Error_Loader $e) {
                    return $e;
                } catch (\Twig_Error_Runtime $e) {
                    return $e;
                } catch (\Twig_Error_Syntax $e) {
                    return $e;
                }
            }
        } else
            try {
                return $this->twig->render("newPlayer.html.twig", [
                    "titre" => $titre,
                    "disabled" => $disabled,
                    "clubs" => $clubs,
                    "rankings" => $rankings,
                ]);
            } catch (\Twig_Error_Loader $e) {
                return $e;
            } catch (\Twig_Error_Runtime $e) {
                return $e;
            } catch (\Twig_Error_Syntax $e) {
                return $e;
            }
    }

    public function update($id, $post = null)
    {
        $titre = "Modification du joueur : " . $id;
        $disabled = " disabled";
        $playerrepo = new PlayerRepository();
        $player = $playerrepo->getByID($id);
        $clubrepo = new ClubRepository();
        $clubs = $clubrepo->getAll();
        asort($clubs);
        $rankingrepo = new RankingRepository();
        $rankings = $rankingrepo->getAll();
        $errors = [];
        $errors['test'] = "Test";
        if (isset($post["submit"])) {
            //hydratation
            $player->setLastName($post["lastname"]);
            $player->setFirstName($post["firstname"]);
            $player->setClubId($post["club"]);
            $player->setRankingId($post["ranking"]);
            //si le formulaire est valide
            $isValid = true;
            $errors = [];
            // Vérification de l'id, ne peut pas être modifié
            if (isset($post['id']))
                if ($id != $post['id']) {
                    $isValid = false;
                    $errors["playerId"] = "Vous ne pouvez pas modifier le numéro de licence";
                }
            // Vérification du nom, ne peut pas être vide
            if (strlen(trim($post["lastname"])) == 0) {
                $isValid = false;
                $errors["playerLastName"] = "Le champ est requis";
            }
            // Vérification du prénom, ne peut pas être vide
            if (strlen(trim($post["firstname"])) == 0) {
                $isValid = false;
                $errors["playerFirstName"] = "Le champ est requis";
            }
            // Vérification du club, doit appartenir à la liste de choix
            if (!in_array($post["club"], array_map(function ($c) {
                /** @var Club $c */
                return $c->getId();
            }, $clubs))) {
                $isValid = false;
                $errors["playerClub"] = "Le club d'indice " . $post["club"] . " ne figure pas dans la base de données";
            }
            // Vérification du classement, doit appartenir à la liste de choix
            if (!in_array($post["ranking"], array_map(function ($r) {
                /** @var Ranking $r */
                return $r->getId();
            }, $rankings))) {
                $isValid = false;
                $errors["playerRanking"] = "Le classement d'indice " . $post["ranking"] . " ne figure pas dans la base de données";
            }
            if ($isValid) {
                if ($playerrepo->update($id, $player))
                    header("Location:" . TwigExtension::path('player', 'index'));
                return -1;
            } else {
                try {
                    return $this->twig->render("newPlayer.html.twig", [
                        "titre" => $titre,
                        "disabled" => $disabled,
                        "player" => $player,
                        "clubs" => $clubs,
                        "rankings" => $rankings,
                        "errors" => $errors,
                    ]);
                } catch (\Twig_Error_Loader $e) {
                    return $e;
                } catch (\Twig_Error_Runtime $e) {
                    return $e;
                } catch (\Twig_Error_Syntax $e) {
                    return $e;
                }
            }
        } else
            try {
                return $this->twig->render("newPlayer.html.twig", [
                    "titre" => $titre,
                    "disabled" => $disabled,
                    "player" => $player,
                    "clubs" => $clubs,
                    "rankings" => $rankings,
                    "errors" => $errors,
                ]);
            } catch (\Twig_Error_Loader $e) {
                return $e;
            } catch (\Twig_Error_Runtime $e) {
                return $e;
            } catch (\Twig_Error_Syntax $e) {
                return $e;
            }
    }

    public function delete($id)
    {
        $repo = new PlayerRepository();
        if ($repo->delete($id))
            header("Location:" . TwigExtension::path('player', 'index'));
    }

    public function getAllByClub($params = null)
    {
        if (array_key_exists('id', $params))
            $players = $this->repo->getAllByClub2($params['id']);
        else
            $players = $this->repo->getAllByClub2(0);
        Player::setOrdersAndIndexes($players);
        return $players;
    }

    public function getByID($params = null)
    {
        if (array_key_exists('id', $params))
            return $this->repo->getByID2($params['id']);
        else
            return $this->repo->getByID2(0);
    }

    public function patch($params = null)
    {
        parse_str(file_get_contents('php://input'), $_PATCH);
        $player = (new Player())->getMapping(json_decode($_PATCH['player']));
        if (array_key_exists('id', $params))
            return $this->repo->put($params['id'], $player);
        else
            return $this->repo->put(0, $player);
    }
}