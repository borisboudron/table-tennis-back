<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 10-10-18
 * Time: 16:14
 */

namespace TTTheux\Controllers;


use Toolbox\DBPingRepository;
use TTTheux\Models\Club;
use TTTheux\Repositories\ClubRepository;
use TTTheux\Utils\TwigExtension;
use Twig_Environment;

class ClubController
{
    /** @var Twig_Environment $twig */
    private $twig;
    private $repo;

    public function __construct($twig = null)
    {
        $this->twig = $twig;
        $this->repo = new ClubRepository();
    }

    public function index()
    {
        $list = $this->repo->getAll();
        try {
            return $this->twig->render("clubList.html.twig", [
                "clubs" => $list
            ]);
        } catch (\Twig_Error_Loader $e) {
            return $e;
        } catch (\Twig_Error_Runtime $e) {
            return $e;
        } catch (\Twig_Error_Syntax $e) {
            return $e;
        }
    }

    public function details($id)
    {
        $prod = $this->repo->getByID($id);
        try {
            return $this->twig->render("clubDetails.html.twig", [
                "club" => $prod
            ]);
        } catch (\Twig_Error_Loader $e) {
            return $e;
        } catch (\Twig_Error_Runtime $e) {
            return $e;
        } catch (\Twig_Error_Syntax $e) {
            return $e;
        }
    }

    public function detailsFromDBPing($id)
    {
        $repo = new DBPingRepository();
        $prod = $repo->getClubByUniqueID($id);
        try {
            return $this->twig->render("clubDetails.html.twig", [
                "club" => $prod
            ]);
        } catch (\Twig_Error_Loader $e) {
            return $e;
        } catch (\Twig_Error_Runtime $e) {
            return $e;
        } catch (\Twig_Error_Syntax $e) {
            return $e;
        }
    }

    public function add($post = null)
    {
        $titre = "Nouveau club";
        $disabled = "";
        $errors = [];
        if (isset($post["submit"])) {
            $club = new Club();
            //hydratation
            $club->setId($post["id"]);
            $club->setName($post["name"]);
            //si le formulaire est valide
            $isValid = true;
            $errors = [];
            // Vérification de l'id, ne peut pas être vide
            if (strlen(trim($post["id"])) == 0) {
                $isValid = false;
                $errors["clubId"] = "Le champ est requis";
            }
            // Vérification du nom, ne peut pas être vide
            if (strlen(trim($post["name"])) == 0) {
                $isValid = false;
                $errors["clubName"] = "Le champ est requis";
            }
            if ($isValid) {
                $clubrepo = new ClubRepository();
                if ($clubrepo->insertWithID($club))
                    header("Location:" . TwigExtension::path('club', 'index'));
                return -1;
            } else {
                try {
                    return $this->twig->render("newClub.html.twig", [
                        "titre" => $titre,
                        "disabled" => $disabled,
                        "club" => $club,
                        "errors" => $errors,
                    ]);
                } catch (\Twig_Error_Loader $e) {
                    return $e;
                } catch (\Twig_Error_Runtime $e) {
                    return $e;
                } catch (\Twig_Error_Syntax $e) {
                    return $e;
                }
            }
        } else
            try {
                return $this->twig->render("newClub.html.twig", [
                    "titre" => $titre,
                    "disabled" => $disabled,
                    "errors" => $errors,
                ]);
            } catch (\Twig_Error_Loader $e) {
                return $e;
            } catch (\Twig_Error_Runtime $e) {
                return $e;
            } catch (\Twig_Error_Syntax $e) {
                return $e;
            }
    }

    public function update($id, $post = null)
    {
        $titre = "Modification du club : " . $id;
        $disabled = " disabled";
        $clubrepo = new ClubRepository();
        $club = $clubrepo->getByID($id);
        $errors = [];
        if (isset($post["submit"])) {
            //hydratation
            $club->setClub($post["name"]);
            //si le formulaire est valide
            $isValid = true;
            $errors = [];
            // Vérification de l'id, ne peut pas être modifié
            if (isset($post['id']))
                if ($id != $post['id']) {
                    $isValid = false;
                    $errors["clubId"] = "Vous ne pouvez pas modifier l'indice du club";
                }
            // Vérification du nom, ne peut pas être vide
            if (strlen(trim($post["name"])) == 0) {
                $isValid = false;
                $errors["clubName"] = "Le champ est requis";
            }
            if ($isValid) {
                if ($clubrepo->update($id, $club))
                    header("Location:" . TwigExtension::path('club', 'index'));
                return -1;
            } else {
                try {
                    return $this->twig->render("newClub.html.twig", [
                        "titre" => $titre,
                        "disabled" => $disabled,
                        "club" => $club,
                        "errors" => $errors,
                    ]);
                } catch (\Twig_Error_Loader $e) {
                    return $e;
                } catch (\Twig_Error_Runtime $e) {
                    return $e;
                } catch (\Twig_Error_Syntax $e) {
                    return $e;
                }
            }
        } else
            try {
                return $this->twig->render("newClub.html.twig", [
                    "titre" => $titre,
                    "disabled" => $disabled,
                    "club" => $club,
                    "errors" => $errors,
                ]);
            } catch (\Twig_Error_Loader $e) {
                return $e;
            } catch (\Twig_Error_Runtime $e) {
                return $e;
            } catch (\Twig_Error_Syntax $e) {
                return $e;
            }
    }

    public function delete($id)
    {
        $repo = new ClubRepository();
        if ($repo->delete($id))
            header("Location:" . TwigExtension::path('club', 'index'));
    }

    public function getAll($params = null)
    {
        return $this->repo->getAll2($params);
    }

    public function getByID($params)
    {
        if (array_key_exists('id', $params))
            return $this->repo->getByID2($params['id']);
        else
            return $this->repo->getByID2(0);
    }
}