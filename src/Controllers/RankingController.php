<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 10-10-18
 * Time: 18:04
 */

namespace TTTheux\Controllers;


use TTTheux\Models\Ranking;
use TTTheux\Repositories\RankingRepository;
use TTTheux\Utils\TwigExtension;
use Twig_Environment;

class RankingController
{
    /** @var Twig_Environment $twig */
    private $twig;

    public function __construct($twig)
    {
        $this->twig = $twig;
    }

    public function index()
    {
        $repo = new RankingRepository();
        $list = $repo->getAll();
        try {
            return $this->twig->render("rankingList.html.twig", [
                "rankings" => $list
            ]);
        } catch (\Twig_Error_Loader $e) {
            return $e;
        } catch (\Twig_Error_Runtime $e) {
            return $e;
        } catch (\Twig_Error_Syntax $e) {
            return $e;
        }
    }

    public function add($post = null)
    {
        $titre = "Nouveau classement";
        $disabled = "";
        $errors = [];
        if (isset($post["submit"])) {
            $ranking = new Ranking();
            //hydratation
            $ranking->setRanking($post["name"]);
            //si le formulaire est valide
            $isValid = true;
            $errors = [];
            // Vérification du classement, ne peut pas être vide
            if (strlen(trim($post["ranking"])) == 0) {
                $isValid = false;
                $errors["ranking"] = "Le champ est requis";
            }
            if ($isValid) {
                $rankingrepo = new RankingRepository();
                if ($rankingrepo->insert($ranking))
                    header("Location:" . TwigExtension::path('ranking', 'index'));
                return -1;
            } else {
                try {
                    return $this->twig->render("newRanking.html.twig", [
                        "titre" => $titre,
                        "disabled" => $disabled,
                        "ranking" => $ranking,
                        "errors" => $errors,
                    ]);
                } catch (\Twig_Error_Loader $e) {
                    return $e;
                } catch (\Twig_Error_Runtime $e) {
                    return $e;
                } catch (\Twig_Error_Syntax $e) {
                    return $e;
                }
            }
        } else
            try {
                return $this->twig->render("newRanking.html.twig", [
                    "titre" => $titre,
                    "disabled" => $disabled,
                    "errors" => $errors,
                ]);
            } catch (\Twig_Error_Loader $e) {
                return $e;
            } catch (\Twig_Error_Runtime $e) {
                return $e;
            } catch (\Twig_Error_Syntax $e) {
                return $e;
            }
    }

    public function delete($id)
    {
        $repo = new RankingRepository();
        if ($repo->delete($id))
            header("Location:" . TwigExtension::path('club', 'index'));
    }

}