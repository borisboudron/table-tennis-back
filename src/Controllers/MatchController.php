<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 15-01-19
 * Time: 16:29
 */

namespace TTTheux\Controllers;


use Toolbox\BaseController;
use TTTheux\Models\Match;
use TTTheux\Repositories\MatchRepository;

class MatchController extends BaseController
{

    public function getRepositoryName()
    {
        return MatchRepository::class;
    }

    public function post($params = null)
    {
        if (array_key_exists('interclubsId', $_POST) && array_key_exists('position', $_POST)) {
            $match = (new Match())->setInterclubsId($_POST['interclubsId'])->setPosition($_POST['position']);
            return $this->repo->insert($match);
        }
        return -1;
    }
}