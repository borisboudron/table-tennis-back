<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 21:54
 */

namespace TTTheux\Repositories;


use TTTheux\Models\MatchSet;
use Toolbox\BaseRepository;

class MatchSetRepository extends BaseRepository
{
    protected function getTableName()
    {
        return 'matchsets';
    }

    protected function getPKBinding()
    {
        return [
            'id' => 'id',
        ];
    }

    protected function getUKBindings()
    {
        return [
            'matchId' => 'matchId',
            'position' => 'position',
        ];
    }

    protected function getEntityName()
    {
        return MatchSet::class;
    }

    protected function getBindings()
    {
        return [
            'setId' => 'setId',
        ];
    }

    public function getByMatchId($matchId)
    {
        $query = 'SELECT ';
        foreach ($this->getPKBinding() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($this->getUKBindings() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($this->getBindings() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM ';
        $query .= $this->getTableName();
        $query .= ' WHERE matchId = :matchId';
        $response = $this->pdo->prepare($query);
        $response->execute(array(
            ':matchId' => $matchId,
        ));
        $items = [];
        while ($item = $response->fetchObject($this->getEntityName()))
            $items[] = $item;
        return $items;
    }

    public function updateByUKs($item)
    {
        $bindings = $this->getBindings();
        $ukBindings = $this->getUKBindings();
        $query = 'UPDATE ';
        $query .= $this->getTableName();
        $query .= ' SET ';
        foreach ($bindings as $key => $value) {
            $query .= $key;
            $query .= " = ";
            $query .= ':';
            $query .= $key;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' WHERE ';
        foreach ($ukBindings as $key => $value) {
            $query .= $key;
            $query .= " = ";
            $query .= ':';
            $query .= $key;
            $query .= ' AND ';
        }
        $query = substr($query, 0, -5);
        $response = $this->pdo->prepare($query);
        $tab = [];
        foreach ($ukBindings as $key => $value)
            $tab[":" . $key] = $item->{"get" . ucwords($value)}();
        foreach ($bindings as $key => $value)
            $tab[":" . $key] = $item->{"get" . ucwords($value)}();
        return $response->execute($tab);
    }
}