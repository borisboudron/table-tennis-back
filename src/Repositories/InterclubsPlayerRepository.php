<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 15:57
 */

namespace TTTheux\Repositories;


use TTTheux\Models\InterclubsPlayer;
use Toolbox\BaseRepository;

class InterclubsPlayerRepository extends BaseRepository
{

    protected function getTableName()
    {
        return 'interclubsplayers';
    }

    protected function getPKBinding()
    {
        return [
            'id' => 'id',
        ];
    }

    protected function getUKBindings()
    {
        return [];
    }

    protected function getEntityName()
    {
        return InterclubsPlayer::class;
    }

    protected function getBindings()
    {
        return [
            'interclubsId' => 'interclubsId',
            'playerId' => 'playerId',
            'position' => 'position',
        ];
    }

    /**
     * @param $interclubsId
     * @return InterclubsPlayer[]
     */
    public function getByInterclubsId($interclubsId)
    {
        $query = 'SELECT ';
        foreach ($this->getPKBinding() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($this->getBindings() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM ';
        $query .= $this->getTableName();
        $query .= ' WHERE interclubsId = :interclubsId';
        $response = $this->pdo->prepare($query);
        $response->execute(array(
            ':interclubsId' => $interclubsId,
        ));
        $items = [];
        while ($item = $response->fetchObject($this->getEntityName()))
            $items[] = $item;
        return $items;
    }
}