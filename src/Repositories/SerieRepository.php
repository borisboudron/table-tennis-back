<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-10-18
 * Time: 16:20
 */

namespace TTTheux\Repositories;

use TTTheux\models\Serie;
use Toolbox\SimpleObjectRepository;

class SerieRepository extends SimpleObjectRepository
{
    protected function getTableName()
    {
        return 'serie';
    }

    protected function getPKBinding()
    {
        return ['id' => 'id'];
    }

    protected function getUKBindings()
    {
        return [];
    }

    protected function getEntityName()
    {
        return Serie::class;
    }

    protected function getBindings()
    {
        return [
            "serie" => "serie",
        ];
    }
}