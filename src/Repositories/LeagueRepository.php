<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 14:26
 */

namespace TTTheux\Repositories;


use TTTheux\Models\League;
use Toolbox\BaseRepository;

class LeagueRepository extends BaseRepository
{

    protected function getTableName()
    {
        return 'league';
    }

    protected function getPKBinding()
    {
        return [
            'id' => 'id',
        ];
    }

    protected function getUKBindings()
    {
        return [
            'division' => 'division',
            'serie' => 'serie',
            'categoryId' => 'categoryId',
        ];
    }

    protected function getEntityName()
    {
        return League::class;
    }

    protected function getBindings()
    {
        return [];
    }
}