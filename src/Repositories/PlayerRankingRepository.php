<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 17:58
 */

namespace TTTheux\Repositories;


use TTTheux\Models\PlayerRanking;
use Toolbox\BaseRepository;

class PlayerRankingRepository extends BaseRepository
{

    protected function getTableName()
    {
        return 'playerrankings';
    }

    protected function getPKBinding()
    {
        return [
            'id' => 'id',
        ];
    }

    protected function getUKBindings()
    {
        return [];
    }

    protected function getEntityName()
    {
        return PlayerRanking::class;
    }

    protected function getBindings()
    {
        return [
            'playerId' => 'playerId',
            'rankingId' => 'rankingId',
            'season' => 'season',
        ];
    }

    /**
     * @param $playerId
     * @return PlayerRanking[]
     */
    public function getByPlayerId($playerId)
    {
        $query = 'SELECT ';
        foreach ($this->getPKBinding() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($this->getBindings() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM ';
        $query .= $this->getTableName();
        $query .= ' WHERE playerId = :playerId';
        $response = $this->pdo->prepare($query);
        $response->execute(array(
            ':playerId' => $playerId,
        ));
        $items = [];
        while ($item = $response->fetchObject($this->getEntityName()))
            $items[] = $item;
        return $items;
    }
}