<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 20:50
 */

namespace TTTheux\Repositories;


use PDO;
use TTTheux\Models\Match;
use Toolbox\BaseRepository;

class MatchRepository extends BaseRepository
{

    protected function getTableName()
    {
        return 'pingmatch';
    }

    protected function getPKBinding()
    {
        return [
            'id' => 'id',
        ];
    }

    protected function getUKBindings()
    {
        return [];
    }

    protected function getEntityName()
    {
        return Match::class;
    }

    protected function getBindings()
    {
        return [
            'interclubsId' => 'interclubsId',
            'position' => 'position',
        ];
    }

    /**
     * @param $interclubsId
     * @return Match[]
     */
    public function getByInterclubsId($interclubsId)
    {
        $query = 'SELECT ';
        foreach ($this->getPKBinding() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($this->getBindings() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM ';
        $query .= $this->getTableName();
        $query .= ' WHERE interclubsId = :interclubsId';
        $response = $this->pdo->prepare($query);
        $response->execute(array(
            ':interclubsId' => $interclubsId,
        ));
        $items = $response->fetchAll(PDO::FETCH_CLASS, Match::class);
        return $items;
    }
}