<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 16:46
 */

namespace TTTheux\Repositories;


use TTTheux\Models\PlayerClub;
use Toolbox\BaseRepository;

class PlayerClubRepository extends BaseRepository
{

    protected function getTableName()
    {
        return 'playerclubs';
    }

    protected function getPKBinding()
    {
        return [
            'id' => 'id',
        ];
    }

    protected function getUKBindings()
    {
        return [];
    }

    protected function getEntityName()
    {
        return PlayerClub::class;
    }

    protected function getBindings()
    {
        return [
            'playerId' => 'playerId',
            'clubId' => 'clubId',
            'season' => 'season',
        ];
    }

    /**
     * @param int $playerId
     * @return array[PlayerClub]
     */
    public function getByPlayerId($playerId)
    {
        $query = 'SELECT ';
        foreach ($this->getPKBinding() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($this->getBindings() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM ';
        $query .= $this->getTableName();
        $query .= ' WHERE playerId = :playerId';
        $response = $this->pdo->prepare($query);
        $response->execute(array(
            ':playerId' => $playerId,
        ));
        $items = [];
        while ($item = $response->fetchObject($this->getEntityName()))
            $items[] = $item;
        return $items;
    }
}