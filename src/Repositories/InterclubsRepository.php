<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 13:40
 */

namespace TTTheux\Repositories;


use PDOException;
use TTTheux\Models\Interclubs;
use Toolbox\BaseRepository;
use TTTheux\Models\Player;
use TTTheux\Models\Selection;

class InterclubsRepository extends BaseRepository
{

    protected function getTableName()
    {
        return 'interclubs';
    }

    protected function getPKBinding()
    {
        return [
            'id' => 'id',
        ];
    }

    protected function getUKBindings()
    {
        return [
            'interclubsNb' => 'interclubsNb',
            'season' => 'season',
        ];
    }

    protected function getEntityName()
    {
        return Interclubs::class;
    }

    protected function getBindings()
    {
        return [
            'date' => 'date',
            'venueId' => 'venueId',
            'leagueId' => 'leagueId',
            'startTime' => 'startTime',
            'endTime' => 'endTime',
            'selection1Id' => 'selection1Id',
            'selection2Id' => 'selection2Id',
            'homeCaptainId' => 'homeCaptainId',
            'awayCaptainId' => 'awayCaptainId',
            'refereeId' => 'refereeId',
        ];
    }

    public function getByID2($interclubsId)
    {
        // Local-side
        $interclubs = $this->getByID($interclubsId);
        // TabT-side
        $GetMatchesRequest = array('Credentials' => $this->Credentials,
            'ShowDivisionName' => 'yes',
            'WithDetails' => true,
            'MatchUniqueId' => $interclubsId,
        );
        $ResponseMatch = $this->tabt->GetMatches($GetMatchesRequest);
        // Data merging
        if (is_null($interclubs) || $interclubs === false)
            $interclubs = new Interclubs();
        $interclubs = $interclubs->getTabTMapping($ResponseMatch->TeamMatchesEntries);
        try {
            $this->insert($interclubs, false);
        } catch (PDOException $e) {
        }
        // Set player indexes
        /** @var Selection $selection */
        foreach ($interclubs->getSelections() as $selection) {
            $repo = new PlayerRepository();
            /** @var Player[] $players */
            $players = $repo->getAllByClub2($selection->getTeam()->getClub()->getId());
            Player::setOrdersAndIndexes($players);
            /** @var Player $player */
            foreach ($selection->getPlayers() as $player) {
                $found = false;
                $p = 0;
                while (!$found && $p < count($players)) {
                    // PDO sends back string only
                    if ($player->getId() == $players[$p]->getId()) {
                        $found = true;
                        $player->setIndex($players[$p]->getIndex());
                        $player->setOrder($players[$p]->getOrder());
                    } else
                        $p++;
                }
            }
        }
        return $interclubs;
    }

    public function getTabTAllByTeam($clubId, $teamLetter)
    {
        $GetMatchesRequest = array(
            'Credentials' => $this->Credentials,
            'Club' => $clubId,
            'Team' => $teamLetter,
        );
        return $this->tabt->GetMatches($GetMatchesRequest);
    }
}