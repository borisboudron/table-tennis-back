<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-10-18
 * Time: 16:37
 */

namespace TTTheux\Repositories;


use TTTheux\models\PlayerSeries;
use PDO;
use Toolbox\BaseRepository;

class PlayerSeriesRepository extends BaseRepository
{

    protected function getTableName()
    {
        return 'playerseries';
    }

    protected function getPKBinding()
    {
        return ['id' => 'id'];
    }

    protected function getUKBindings()
    {
        return [];
    }

    protected function getEntityName()
    {
        return PlayerSeries::class;
    }

    protected function getBindings()
    {
        return [
            "playerId" => "playerId",
            "serieId" => "serieId",
        ];
    }

    public function getByPlayerId($id)
    {
        $query = "SELECT * FROM playerseries WHERE playerId = :id";
        $response = $this->pdo->prepare($query);
        $tab[":id"] = $id;
        $response->execute($tab);
        return $response->fetchAll(PDO::FETCH_CLASS, PlayerSeries::class);
    }

    public function getBySerieId($id)
    {
        $query = "SELECT * FROM playerseries WHERE serieId = :id";
        $response = $this->pdo->prepare($query);
        $tab[":id"] = $id;
        $response->execute($tab);
        return $response->fetchAll(PDO::FETCH_CLASS, PlayerSeries::class);
    }
}