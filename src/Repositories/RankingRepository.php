<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-10-18
 * Time: 16:24
 */

namespace TTTheux\Repositories;


use TTTheux\models\Ranking;
use Toolbox\SimpleObjectRepository;

class RankingRepository extends SimpleObjectRepository
{
    protected function getTableName()
    {
        return 'ranking';
    }

    protected function getPKBinding()
    {
        return ['id' => 'id'];
    }

    protected function getUKBindings()
    {
        return [];
    }

    protected function getEntityName()
    {
        return Ranking::class;
    }

    protected function getBindings()
    {
        return [
            "ranking" => "ranking",
        ];
    }
}