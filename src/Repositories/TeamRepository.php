<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 15:38
 */

namespace TTTheux\Repositories;


use TTTheux\Models\Team;
use Toolbox\BaseRepository;

class TeamRepository extends BaseRepository
{

    protected function getTableName()
    {
        return 'team';
    }

    protected function getPKBinding()
    {
        return [
            'id' => 'id',
        ];
    }

    protected function getUKBindings()
    {
        return [];
    }

    protected function getEntityName()
    {
        return Team::class;
    }

    protected function getBindings()
    {
        return [
            'clubId' => 'clubId',
            'letter' => 'letter'
        ];
    }

    public function getAllByClub2($clubId)
    {
        $bindings = $this->getBindings();
        $teams = [];
        $query = 'SELECT ';
        foreach ($this->getPKBinding() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($this->getUKBindings() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($bindings as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM ';
        $query .= $this->getTableName();
        $query .= ' WHERE clubId = :clubId';
        $response = $this->pdo->prepare($query);
        $tab = [
            'clubId' => $clubId,
        ];
        $response->execute($tab);
        while ($team = $response->fetchObject($this->getEntityName()))
            $teams[] = $team;
        return $teams;
    }

    public function getTabTAllByClub($id)
    {
        $GetClubTeamsRequest = array(
            'Credentials' => $this->Credentials,
            'Club' => $id,
        );
        $GetClubTeamsResponse = $this->tabt->GetClubTeams($GetClubTeamsRequest);
        foreach ($GetClubTeamsResponse->TeamEntries as $team) {
            if (strcmp('Afdeling', substr($team->DivisionName, 0, 8)) === 0) {
                $team->DivisionName = str_replace('Afdeling', 'Division', $team->DivisionName);
                $team->DivisionName = str_replace('Heren', 'Messieurs', $team->DivisionName);
                $team->DivisionName = str_replace('Veteranen', 'Vétérans', $team->DivisionName);
            }
        }
        return $GetClubTeamsResponse;
    }
}