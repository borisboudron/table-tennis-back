<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 11-01-19
 * Time: 08:58
 */

namespace TTTheux\Repositories;


use Toolbox\BaseRepository;
use TTTheux\Models\Category;

class CategoryRepository extends BaseRepository
{

    protected function getTableName()
    {
        return 'category';
    }

    protected function getPKBinding()
    {
        return [
            'id' => 'id',
        ];
    }

    protected function getUKBindings()
    {
        return [
            'name' => 'name',
        ];
    }

    protected function getEntityName()
    {
        return Category::class;
    }

    protected function getBindings()
    {
        return [];
    }
}