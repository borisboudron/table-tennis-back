<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 10-10-18
 * Time: 18:34
 */

namespace TTTheux\Repositories;


use TTTheux\Models\Set;
use Toolbox\BaseRepository;

class SetRepository extends BaseRepository
{
    protected function getTableName()
    {
        return 'pingset';
    }

    protected function getPKBinding()
    {
        return ['id' => 'id'];
    }

    protected function getUKBindings()
    {
        return [
            "homeScore" => "homeScore",
            "awayScore" => "awayScore",
        ];
    }

    protected function getEntityName()
    {
        return Set::class;
    }

    protected function getBindings()
    {
        return [];
    }
}