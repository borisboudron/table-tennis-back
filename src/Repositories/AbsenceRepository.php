<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 22-12-18
 * Time: 21:26
 */

namespace TTTheux\Repositories;


use Toolbox\BaseRepository;
use TTTheux\Models\Absence;

class AbsenceRepository extends BaseRepository
{

    protected function getTableName()
    {
        return 'absence';
    }

    protected function getPKBinding()
    {
        return [
            'id' => 'id',
        ];
    }

    protected function getUKBindings()
    {
        return [];
    }

    protected function getEntityName()
    {
        return Absence::class;
    }

    protected function getBindings()
    {
        return [
            'weekDay' => 'weekDay',
            'playerId' => 'playerId',
            'informedDate' => 'informedDate',
            'informedMethod' => 'informedMethod',
            'reason' => 'reason',
        ];
    }
}