<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 30-10-18
 * Time: 22:09
 */

namespace TTTheux\Repositories;


use TTTheux\Models\PlayerSeason;
use Toolbox\BaseRepository;

class PlayerSeasonRepository extends BaseRepository
{

    protected function getTableName()
    {
        return 'playerseasons';
    }

    protected function getPKBinding()
    {
        return [
            'id' => 'id',
        ];
    }

    protected function getUKBindings()
    {
        return [];
    }

    protected function getEntityName()
    {
        return PlayerSeason::class;
    }

    protected function getBindings()
    {
        return [
            'playerId' => 'playerId',
            'season' => 'season',
            'clubId' => 'clubId',
            'rankingId' => 'rankingId',
            'orderNb' => 'orderNb',
            'indexNb' => 'indexNb',
        ];
    }

    /**
     * @param int $playerId
     * @return PlayerSeason[]
     */
    public function getByPlayerId($playerId)
    {
        $query = 'SELECT ';
        foreach ($this->getPKBinding() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($this->getBindings() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM ';
        $query .= $this->getTableName();
        $query .= ' WHERE playerId = :playerId';
        $response = $this->pdo->prepare($query);
        $response->execute(array(
            ':playerId' => $playerId,
        ));
        $items = [];
        while ($item = $response->fetchObject($this->getEntityName()))
            $items[] = $item;
        return $items;
    }
}