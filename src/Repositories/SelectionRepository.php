<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 25-12-18
 * Time: 00:57
 */

namespace TTTheux\Repositories;


use Toolbox\BaseRepository;
use TTTheux\Models\Selection;

class SelectionRepository extends BaseRepository
{

    protected function getTableName()
    {
        return 'selection';
    }

    protected function getPKBinding()
    {
        return ['id' => 'id'];
    }

    protected function getUKBindings()
    {
        return [];
    }

    protected function getEntityName()
    {
        return Selection::class;
    }

    protected function getBindings()
    {
        return [
            'weekDay' => 'weekDay',
            'teamId' => 'teamId',
            'player1Id' => 'player1Id',
            'player2Id' => 'player2Id',
            'player3Id' => 'player3Id',
            'player4Id' => 'player4Id',
        ];
    }

    public function getAllByWeekDay($weekDay, $clubId = null)
    {
        $bindings = $this->getBindings();
        $selections = [];
        $query = 'SELECT ';
        foreach ($this->getPKBinding() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($this->getUKBindings() as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        foreach ($bindings as $key => $value) {
            $query .= $key;
            $query .= " AS ";
            $query .= $value;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM ';
        $query .= $this->getTableName();
        $query .= ' WHERE weekDay = :weekDay';
        $tab = [
            'weekDay' => $weekDay,
        ];
        if ($clubId !== null) {
            $query .= ' AND clubId = :clubId';
            $tab[':clubId'] = $clubId;
        }
        $response = $this->pdo->prepare($query);
        $response->execute($tab);
        while ($selection = $response->fetchObject($this->getEntityName()))
            $selections[] = $selection;
        return $selections;
    }
}