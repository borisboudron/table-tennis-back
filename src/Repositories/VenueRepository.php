<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 29-10-18
 * Time: 14:20
 */

namespace TTTheux\Repositories;


use TTTheux\Models\Venue;
use Toolbox\BaseRepository;

class VenueRepository extends BaseRepository
{
    protected function getTableName()
    {
        return "venue";
    }

    protected function getPKBinding()
    {
        return [
            'id' => 'id',
        ];
    }

    protected function getUKBindings()
    {
        return [
            'building' => 'building',
            'address' => 'address',
            'cityId' => 'cityId',
        ];
    }

    protected function getEntityName()
    {
        return Venue::class;
    }

    protected function getBindings()
    {
        return [];
    }

    public function getByTabTData($tabTVenueId, $clubId)
    {
        $venue = null;
        if ($clubId !== null) {
            // TabT-side
            $GetClubsRequest = array('Credentials' => $this->Credentials,
                'Club' => $clubId,
            );
            $ResponseClub = $this->tabt->GetClubs($GetClubsRequest);
            if (array_key_exists('ClubEntries', $ResponseClub) && is_object($ResponseClub->ClubEntries)) {
                $club = $ResponseClub->ClubEntries;
                if (array_key_exists('VenueEntries', $club)) {
                    $venue = new Venue();
                    if (is_object($club->VenueEntries) && $tabTVenueId === 1) {
                        $venue->getTabTMapping($club->VenueEntries);
                    } else {
                        $venue->getTabTMapping($club->VenueEntries[$tabTVenueId - 1]);
                    }
                }
            }
        }
        return $venue;
    }
}