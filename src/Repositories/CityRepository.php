<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 11-11-18
 * Time: 13:53
 */

namespace TTTheux\Repositories;


use TTTheux\Models\City;
use Toolbox\BaseRepository;

class CityRepository extends BaseRepository
{

    protected function getTableName()
    {
        return 'city';
    }

    protected function getPKBinding()
    {
        return [
            'id' => 'id',
        ];
    }

    protected function getUKBindings()
    {
        return [
            'postalCode' => 'postalCode',
            'name' => 'name',
        ];
    }

    protected function getEntityName()
    {
        return City::class;
    }

    protected function getBindings()
    {
        return [];
    }
}