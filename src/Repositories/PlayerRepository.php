<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-10-18
 * Time: 15:27
 */

namespace TTTheux\Repositories;

use PDOException;
use TTTheux\models\Player;
use PDO;
use Toolbox\BaseRepository;

class PlayerRepository extends BaseRepository
{
    protected function getTableName()
    {
        return 'playerapi';
    }

    protected function getPKBinding()
    {
        return ['id' => 'id'];
    }

    protected function getUKBindings()
    {
        return [];
    }

    protected function getEntityName()
    {
        return Player::class;
    }

    protected function getBindings()
    {
        return [
            "lastName" => "lastName",
            "firstName" => "firstName",
            "training" => "training",
            "teams" => "teams",
            "fridayEvening" => "fridayEvening",
            "saturdayAfternoon" => "saturdayAfternoon",
            "saturdayEvening" => "saturdayEvening",
            "driver" => "driver",
            "backup" => "backup",
            "remarks" => "remarks",
        ];
    }

    public function getAllByClub($season, $clubId)
    {
        $query = 'SELECT * FROM playersbyclub WHERE clubId = :clubId AND season = :season';
        $response = $this->pdo->prepare($query);
        $response->execute(array(
            ':clubId' => $clubId,
            ':season' => $season,
        ));
        $items = $response->fetchAll(PDO::FETCH_ASSOC);
        return $items;
    }

    public function getAllByClub2($clubId)
    {
        // Local-side
        /** @var Player[] $players */
        $players = $this->getAll();
        // TabT-side
        $GetMembersRequest = array('Credentials' => $this->Credentials
        , 'Club' => $clubId
        , 'ExtendedInformation' => true
        );
        $ResponseMembers = $this->tabt->GetMembers($GetMembersRequest);
        // Data merging
        $members = [];
        $nonPlayers = 0;
        if (is_object($ResponseMembers->MemberEntries)) {
            $member = $ResponseMembers->MemberEntries;
            if ($member->Status === 'A') {
                $player = new Player();
                $members[] = $player->getTabTMapping($member);
            }
        } else
            foreach ($ResponseMembers->MemberEntries as $member) {
                // Check non-players from TabT
                if ($member->Status === 'A') {
                    $member->Position = $member->Position - $nonPlayers;
                    // Check local player correspondance
                    $player = null;
                    $found = false;
                    $p = 0;
                    while (!$found && $p < count($players)) {
                        if ($players[$p]->getId() == $member->UniqueIndex) {
                            $player = $players[$p];
                            $found = true;
                        } else
                            $p++;
                    }
                    // Data merging
                    if (is_null($player))
                        $player = new Player();
                    $members[] = $player->getTabTMapping($member);
                    try {
                        $this->insert($player, false);
                    } catch (PDOException $e) {
                    }
                } else
                    $nonPlayers++;
            }
        return $members;
    }

    public function getByID2($playerId)
    {
        // Local-side
        $player = $this->getByID($playerId);
        // TabT-side
        $GetMembersRequest = array('Credentials' => $this->Credentials,
            'UniqueIndex' => $playerId,
            'ExtendedInformation' => true,
        );
        $ResponseMembers = $this->tabt->GetMembers($GetMembersRequest);
        // Data merging
        if (is_null($player) || $player === false)
            $player = new Player();
        if (is_object($ResponseMembers->MemberEntries)) {
            $player = $player->getTabTMapping($ResponseMembers->MemberEntries);
            try {
                $this->insert($player, false);
            } catch (PDOException $e) {
            }
        }
        return $player;
    }
}