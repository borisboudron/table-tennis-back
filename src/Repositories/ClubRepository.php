<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-10-18
 * Time: 16:27
 */

namespace TTTheux\Repositories;


use TTTheux\models\Club;
use Toolbox\SimpleObjectRepository;

class ClubRepository extends SimpleObjectRepository
{
    protected function getTableName()
    {
        return 'club';
    }

    protected function getPKBinding()
    {
        return ['id' => 'id'];
    }

    protected function getUKBindings()
    {
        return [];
    }

    protected function getEntityName()
    {
        return Club::class;
    }

    protected function getBindings()
    {
        return [
            "name" => "name",
        ];
    }

    // Retrieve club list
    public function getAll2($params = null)
    {
        $GetClubRequest = array('Credentials' => $this->Credentials);
        return $this->tabt->GetClubs($GetClubRequest)->ClubEntries;
    }

    // Retrieve club infos
    public function getByID2($index)
    {
        $GetClubRequest = array(
            'Credentials' => $this->Credentials,
            'Club' => $index,
        );
        return $this->tabt->GetClubs($GetClubRequest);
    }
}