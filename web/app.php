<?php


use TTTheux\models\Club;
use TTTheux\models\Player;
use TTTheux\models\Serie;
use TTTheux\repositories\ClubRepository;
use TTTheux\repositories\PlayerRepository;
use TTTheux\repositories\SerieRepository;
use Toolbox\Router;

require_once "../vendor/autoload.php";
$loader = new Twig_Loader_Filesystem('../src/Views');
$twig = new Twig_Environment($loader);

$twigf = new Twig_Function("path", '\TTTheux\Utils\TwigExtension::path');
$twig->addFunction($twigf);
// Encodage d'une liste de joueurs d'une série via un CSV
//$row = 1;
//if (($handle = fopen("../test.csv", "r")) !== FALSE) {
//    $repoClub = new ClubRepository();
//    $repoSerie = new SerieRepository();
//    $repoPlayer = new PlayerRepository();
//    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
//        $num = count($data);
////        echo "<p> $num fields in line $row: <br /></p>\n";
//        $row++;
////        $player = new Player($data[1], $data[2], $data[3], $data[4], $data[5], $data[0]);
////        var_dump($player);
////        $repo = new PlayerRepository();
////        $repo->insert($player);
//
//        // Vérification d'un nouveau club
//        try
//        {
//            if (!$repoClub->getByContent($data[4])) {
//                throw new UnknownClubException($data[4]);
////            $club = new Club();
////            $club->setId($data[0]);
////            $club->setClub($data[1]);
////            $repo->insertWithPK($club);
//            }
//        }
//        catch (UnknownClubException $e)
//        {
//            var_dump($e->errorMessage($e->getClub()));
//        }
//
//        // Vérification d'une nouvelle série
//        if (!$repoSerie->getByContent($data[0]))
//        {
//            $serie = new Serie();
//            $serie->setSerie($data[0]);
//            $repoSerie->insert($serie);
//        }
//
//        // Vérification d'un nouveau joueur
//        if (!$repoPlayer->getByID($data[1]))
//        {
//            $player = new Player();
//            $player->setId($data[1]);
//            $player->setLastName($data[2]);
//            $player->setFirstName($data[3]);
//            $player->setClubs($data[4]);
//            $player->setRankings($data[5]);
//            $repoPlayer->insertWithPK($player);
//        }
//
////        for ($c=0; $c < $num; $c++) {
////            echo $data[$c] . "<br />\n";
////        }
//    }
////    $all = $repo->getAll();
////    var_dump($all);
//    fclose($handle);
//}

$router = new Router();
echo $router->handleRoute($twig, $_GET, $_POST);

//header("Location:../GetMatches-Frenoy.php");
//header("Location:../test.php");