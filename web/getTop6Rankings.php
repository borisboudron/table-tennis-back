<?php

use TTTheux\Core\DB\TabTConnector;

function handleBYE(&$results, $byeMatch, $division)
{
    if ($byeMatch->HomeClub === '-')
    {
        $team = $byeMatch->MatchDetails->AwayPlayers;
        $teamName = substr($byeMatch->AwayTeam, 0,strlen($byeMatch->AwayTeam)-2);
    }
    elseif ($byeMatch->AwayClub === '-')
    {
        $team = $byeMatch->MatchDetails->HomePlayers;
        $teamName = substr($byeMatch->HomeTeam, 0,strlen($byeMatch->HomeTeam)-2);
    }
    var_dump($team->Players);
    foreach ($team->Players as $player)
        setPlayerPoints($results, $player, $division, $byeMatch->WeekName, $teamName, 5);
}
function handleFF(&$results, $ffMatch, $division)
{
    if ($ffMatch->IsHomeForfeited && (!$ffMatch->IsAwayForfeited || $ffMatch->IsHomeWithdrawn !== 'N'))
    {
        $team = $ffMatch->MatchDetails->AwayPlayers;
        $teamName = substr($ffMatch->AwayTeam, 0,strlen($ffMatch->AwayTeam)-2);
    }
    elseif ($ffMatch->IsAwayForfeited && (!$ffMatch->IsHomeForfeited || $ffMatch->IsAwayWithdrawn !== 'N'))
    {
        $team = $ffMatch->MatchDetails->HomePlayers;
        $teamName = substr($ffMatch->HomeTeam, 0,strlen($ffMatch->HomeTeam)-2);
    }
    foreach ($team->Players as $player)
        setPlayerPoints($results, $player, $division, $ffMatch->WeekName, $teamName, 5);
}
function setPlayerPoints (&$playersResults, $player, $division, $week, $team, $points)
{
    $name = $player->LastName . ' ' . $player->FirstName;
    if (!array_key_exists($name, $playersResults))
        $playersResults[$name] = array();
    if (!array_key_exists('Club', $playersResults[$name]))
        $playersResults[$name]['Club'] = $team;
    if (!array_key_exists('Ranking', $playersResults[$name]))
        $playersResults[$name]['Ranking'] = $player->Ranking;
    if (!array_key_exists('TOP6', $playersResults[$name]))
        $playersResults[$name]['TOP6'] = array();
    if (!array_key_exists($division, $playersResults[$name]))
        $playersResults[$name][$division] = array();
    if (!array_key_exists('Count', $playersResults[$name][$division]))
        $playersResults[$name][$division]['Count'] = 0;
    if (!array_key_exists('Total', $playersResults[$name][$division]))
        $playersResults[$name][$division]['Total'] = 0;
    $playersResults[$name][$division][$week] = $points;
    $playersResults[$name][$week] = array($division => $points);
    $playersResults[$name][$division]['Count']++;
    $playersResults[$name][$division]['Total'] += $points;
}
session_start();
require_once "../vendor/autoload.php";
$loader = new Twig_Loader_Filesystem('../src/Views');
$twig = new Twig_Environment($loader);
$twig->addExtension(new Twig_Extension_Debug());
$twig->addFilter(new Twig_SimpleFilter('var_dump', 'var_dump'));
//---------------------------------
// GetMatches-Frenoy.php
//---------------------------------
// Variables globales
$Season = 19;

//$Levels = array(1,16,12);   // National, IWB, Namur
//$Levels = array(1,16,12,11,4,13,14);  // AFTT
//$Levels = array(1,16,12,11,4,13,14,2,3,5,6,7,8,9,10,15); // FRBTT : National, IWB, Namur, BBW, Hainaut, Liège, Lux, ???, ???, VlBBr, Super, Oost-Vl., Antw., West-Vl., Limburg, RegionVTTL
$Levels = array(1,16,13); // National, IWB, Liège
$clubs = array('L002', 'L003', 'L066', 'L095', 'L125', 'L179', 'L184', 'L252', 'L264', 'L272', 'L274', 'L284', 'L296', 'L313', 'L318', 'L320', 'L323', 'L326', 'L328', 'L329', 'L337', 'L344',
    'L348', 'L349', 'L357', 'L360', 'L368', 'L378', 'L382', 'L389', 'L399');
//3626 => 11
//3627 => 11
//3628 => 11
//3678 => 11
//3679 => 11
//$divisions = array('3626', '3627', '3628', '3678', '3679', '3962', '3963', '3965', '3966', '3967', '3968', '3970', '3971', '3972', '3973', '3974', '3975', '3978', '3979', '3980', '3981',
//    '3982', '3987', '3988', '3989', '3990', '3994', '3996', '3997', '3998', '3999', '4000');
$divisions = array('3962');

$Credentials = TabTConnector::getCredentials();

// Variables pour appeler les fonctions de TabT
$TestRequest = array('Credentials' => $Credentials
);

$GetDivisionsRequest = array('Credentials' => $Credentials
,'Season' => $Season
,'Level' => ''
,'ShowDivisionName' => 'yes'
);

$GetMatchesRequest = array('Credentials' => $Credentials
,'DivisionId'  => -1
,'Club'  => ''
,'Team'  => ''
,'DivisionCategory'  => -1
,'Season'  => $Season
,'WeekName'  => ''
,'Level'  => ''
,'ShowDivisionName' => 'yes'
,'YearDateFrom'  => '2000-01-01'
,'YearDateTo'  => '9999-12-31'
,'WithDetails' => true
,'MatchId'  => ''
,'MatchUniqueId'  => ''
);

// Ouverture de la connection Soap
$tabt = TabTConnector::getInstance();


// en output, on aura un tableau qui donnera le statut pour chaque appel
echo '<table>';

// Test
$ResponseTest = $tabt->Test($TestRequest);
echo '<tr>';
echo '<td></td>';
echo '<td>TimeStamp</td>';
echo '<td>CurrentQuota</td>';
echo '<td>AllowedQuota</td>';
echo '</tr>';
echo '<tr>';
echo '<td>Début de test</td>';
echo '<td>', $ResponseTest->Timestamp, '</td>';
echo '<td>', $ResponseTest->CurrentQuota, '</td>';
echo '<td>', $ResponseTest->AllowedQuota, '</td>';
echo '</tr>';

$top6Results = array(
    'National' => array(),
    'P1' => array(),
    'P2' => array(),
    'P3' => array(),
    'P4' => array(),
    'P5' => array(),
    'P6' => array(),
);
$playersResults = array();
$weekLimit = 11;
$club = 'Theux';
$matchCount = 0;
$casNormal = 0;
$matches = array();
foreach ($Levels as $Level)
{
    $GetDivisionsRequest['Level'] = $Level;
    $GetMatchesRequest['Level'] = $Level;

    // Get division list from a given Level
    $ResponseDivisions = $tabt->GetDivisions($GetDivisionsRequest);
    if (array_key_exists('DivisionEntries', $ResponseDivisions))
    {
        $notYetEncoded = 0;
        // Loop through the divisions
        foreach ($ResponseDivisions->DivisionEntries as $DivisionEntry)
        {
            if ((array_key_exists('DivisionCategory', $DivisionEntry)))
            {
                // Get matchs from a given division
                $divisionNumber = substr($DivisionEntry->DivisionName, 9, 1);
                if (($DivisionEntry->DivisionCategory == 1)&&($divisionNumber != '7')&&(in_array($DivisionEntry->DivisionId, $divisions)))
                {
                    $division = ($Level == 13 && $divisionNumber > 0 && $divisionNumber < 7) ? 'P' . $divisionNumber : 'National';

                    $GetMatchesRequest['DivisionId'] = $DivisionEntry->DivisionId;
                    $GetMatchesRequest['DivisionCategory'] = $DivisionEntry->DivisionCategory;

                    $ResponseMatches = $tabt->GetMatches($GetMatchesRequest);

                    // Loop through received entries of the ranking and prepare
                    foreach ($ResponseMatches->TeamMatchesEntries as $TeamMatchesEntry)
                    {
                        $week = $TeamMatchesEntry->WeekName;
                        $isHomeTeamOK = in_array($TeamMatchesEntry->HomeClub, $clubs);
                        $isAwayTeamOK = in_array($TeamMatchesEntry->AwayClub, $clubs);
                        if (($week <= $weekLimit)&&($isHomeTeamOK || $isAwayTeamOK))
                        {

                            $matches[] = $TeamMatchesEntry;
                            $homeTeam = substr($TeamMatchesEntry->HomeTeam, 0,strlen($TeamMatchesEntry->HomeTeam)-2);
                            $awayTeam = substr($TeamMatchesEntry->AwayTeam, 0,strlen($TeamMatchesEntry->AwayTeam)-2);
                            $playerMatchResults = array();
                            var_dump($TeamMatchesEntry);
                            $matchCount++;
                            if (array_key_exists('IndividualMatchResults', $TeamMatchesEntry->MatchDetails)
                                && is_array($TeamMatchesEntry->MatchDetails->HomePlayers->Players)
                                && is_array($TeamMatchesEntry->MatchDetails->AwayPlayers->Players))
                            {
                                // Cas normal avec gestion des WO en cours de match
                                $casNormal++;
                                foreach ($TeamMatchesEntry->MatchDetails->IndividualMatchResults as $result)
                                {
//                                    while ($match < $result->Position)
//                                    {
//                                        $voidMatches[] = $match;
//                                        $match++;
//                                    }
                                    $hPlayer = $TeamMatchesEntry->MatchDetails->HomePlayers->Players[$result->HomePlayerMatchIndex-1];
                                    $aPlayer = $TeamMatchesEntry->MatchDetails->AwayPlayers->Players[$result->AwayPlayerMatchIndex-1];
                                    $hName = $hPlayer->LastName . ' ' . $hPlayer->FirstName;
                                    $aName = $aPlayer->LastName . ' ' . $aPlayer->FirstName;
                                    // Création des joueurs à leur 1er match
                                    if (!array_key_exists($hName, $playerMatchResults)&&$isHomeTeamOK)
                                        $playerMatchResults[$hName] = [0, $homeTeam, $hPlayer->Ranking];
                                    if (!array_key_exists($aName, $playerMatchResults)&&$isAwayTeamOK)
                                        $playerMatchResults[$aName] = [0, $awayTeam, $aPlayer->Ranking];
                                    // Gestion des WO en cours de match
                                    if ($isAwayTeamOK&&(array_key_exists('IsHomeForfeited', $result)||(!array_key_exists('IsAwayForfeited', $result)&&$result->AwaySetCount > $result->HomeSetCount)))
                                    {
                                        $playerMatchResults[$aName][0]++;
                                        if ($playerMatchResults[$aName][0] == 4)
                                            $playerMatchResults[$aName][0]++;
                                    }
                                    elseif ($isHomeTeamOK&&(array_key_exists('IsAwayForfeited', $result)||(!array_key_exists('IsHomeForfeited', $result)&&$result->HomeSetCount > $result->AwaySetCount)))
                                    {
                                        $playerMatchResults[$hName][0]++;
                                        if ($playerMatchResults[$hName][0] == 4)
                                            $playerMatchResults[$hName][0]++;
                                    }
                                }
                            }
//                            elseif (!$TeamMatchesEntry->MatchDetails->DetailsCreated || (!is_array($TeamMatchesEntry->MatchDetails->HomePlayers->Players)&&!is_array($TeamMatchesEntry->MatchDetails->AwayPlayers->Players)))
//                            {
//                                // BYE pas encore encodé
//                                // Match pas encore joué
//                                var_dump('P' . substr($TeamMatchesEntry->DivisionName, 9, 2) . ' (S' . $TeamMatchesEntry->WeekName . ') : ' . $TeamMatchesEntry->HomeTeam . ' - ' . $TeamMatchesEntry->AwayTeam);
//                            }
//                            elseif ($TeamMatchesEntry->HomeClub === '-' || $TeamMatchesEntry->AwayClub === '-')
//                            {
//                                // BYE encodé
//                                handleBYE($playersResults, $TeamMatchesEntry, $division);
//                            }
//                            elseif ($TeamMatchesEntry->IsHomeForfeited || $TeamMatchesEntry->IsAwayForfeited)
//                            {
//                                // Forfait encodé
//                                handleFF($playersResults, $TeamMatchesEntry, $division);
//                                var_dump($TeamMatchesEntry);
//                            }
                            foreach ($playerMatchResults as $name => $points)
                            {
                                if (!array_key_exists($name, $playersResults))
                                    $playersResults[$name] = array();
                                if (!array_key_exists('Club', $playersResults[$name]))
                                    $playersResults[$name]['Club'] = $points[1];
                                if (!array_key_exists('Ranking', $playersResults[$name]))
                                    $playersResults[$name]['Ranking'] = $points[2];
                                if (!array_key_exists('TOP6', $playersResults[$name]))
                                    $playersResults[$name]['TOP6'] = array();
                                if (!array_key_exists($division, $playersResults[$name]))
                                    $playersResults[$name][$division] = array();
                                if (!array_key_exists('Count', $playersResults[$name][$division]))
                                    $playersResults[$name][$division]['Count'] = 0;
                                if (!array_key_exists('Total', $playersResults[$name][$division]))
                                    $playersResults[$name][$division]['Total'] = 0;
                                $playersResults[$name][$division][$week] = $points[0];
                                $playersResults[$name][$week] = array($division => $points[0]);
                                $playersResults[$name][$division]['Count']++;
                                $playersResults[$name][$division]['Total'] += $points[0];
                            }

                        }
                    }
                }
            }
        }
    }
}

foreach ($playersResults as $playerName => $playerResults)
{
    $mostPlayedDivision = '';
    $mostPlayedNumber = 0;
    $mostPlayedResults = array();
    foreach ($playerResults as $playerDivision => $resultsByDivision)
    {
        if (in_array($playerDivision, ['National', 'P1', 'P2', 'P3', 'P4', 'P5', 'P6'])&&($mostPlayedNumber < $resultsByDivision['Count']))
        {
            $mostPlayedNumber = $resultsByDivision['Count'];
            $mostPlayedDivision = $playerDivision;
            $mostPlayedResults = $resultsByDivision;
        }
    }
    $playersResults[$playerName]['TOP6'] = $mostPlayedDivision;
    $playersResults[$playerName]['Total'] = $playersResults[$playerName][$mostPlayedDivision]['Total'];
    $playersResults[$playerName][$mostPlayedDivision] = $mostPlayedResults;
    for ($i = 1; $i <= 22; $i++)
    {
        $index = ($i > 9) ? '' . $i : '0' . $i;
        if (!array_key_exists($index, $playersResults[$playerName]))
            $playersResults[$playerName][$index] = array('' => '');
    }
    $top6Results[$mostPlayedDivision][$playerName] = $playersResults[$playerName];
    foreach ($top6Results[$mostPlayedDivision][$playerName] as $week => $result)
        if (($week <= $weekLimit)&&(substr($week,'0','1') != 'P')&&(in_array(substr($week,'1','1'), ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'])))
        {
            if (key($result) == $mostPlayedDivision)
                $top6Results[$mostPlayedDivision][$playerName][$week] = array ('points' => $result[key($result)], 'class' => '');
            elseif (key($result) == 'National')
            {
                $top6Results[$mostPlayedDivision][$playerName]['Total'] += (is_numeric($result[key($result)])) ? $result[key($result)] : 0;
                $top6Results[$mostPlayedDivision][$playerName][$week] = array ('points' => $result[key($result)], 'class' => 'text-primary');
            }
            elseif ((key($result) != '')&&($mostPlayedDivision == 'National'))
                $top6Results[$mostPlayedDivision][$playerName][$week] = array ('points' => 0, 'class' => 'text-danger');
            elseif (substr(key($result), 1, 1) > substr($top6Results[$mostPlayedDivision][$playerName]['TOP6'], 1, 1))
                $top6Results[$mostPlayedDivision][$playerName][$week] = array ('points' => 0, 'class' => 'text-danger');
            else
            {
                $top6Results[$mostPlayedDivision][$playerName]['Total'] += (is_numeric($result[key($result)])) ? $result[key($result)] : 0;
                $top6Results[$mostPlayedDivision][$playerName][$week] = array ('points' => $result[key($result)], 'class' => 'text-primary');
            }
        }
    ksort($top6Results[$mostPlayedDivision][$playerName]);
}
foreach ($top6Results as $division => $divisionResults)
{
    uasort($top6Results[$division], function($player1, $player2)
    {
        if ($player1['Total'] == $player2['Total'])
            return 0;
        return ($player1['Total'] > $player2['Total']) ? -1 : 1;
    });

}
// Test
$ResponseTest = $tabt->Test($TestRequest);
echo '<tr>';
echo '<td>Fin de test</td>';
echo '<td>', $ResponseTest->Timestamp, '</td>';
echo '<td>', $ResponseTest->CurrentQuota, '</td>';
echo '<td>', $ResponseTest->AllowedQuota, '</td>';
echo '</tr>';

// en output, fin du tableau
echo '</table>';
echo '<br> Matches traités : ' . $matchCount . '<br>';
echo '<br> Cas normaux : ' . $casNormal . '<br>';
echo '<br> Pas encore encodés : ' . $notYetEncoded . '<br>';
try {
    echo $twig->render("Top6Verviers.html.twig", [
        'top6Results' => $top6Results,
        'club' => $club,
    ]);
} catch (Twig_Error_Loader $e) { echo $e->getMessage();
} catch (Twig_Error_Runtime $e) { echo $e->getMessage();
} catch (Twig_Error_Syntax $e) { echo $e->getMessage();
}
//echo $twig->render("detailsMatches.html.twig", [
//    'matches' => $matches,
//]);
?>