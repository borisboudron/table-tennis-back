<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 11-11-18
 * Time: 12:02
 */

use TTTheux\Models\Venue;
use TTTheux\Repositories\VenueRepository;

require_once "../vendor/autoload.php";
CONST EXCEPTIONS = array('de', 'du', 'des', 'le', 'la', 'les', 'à', 'a');
function addressToUpperFirst($address)
{
    $words = explode(' ', $address);
    $newWords = [];
    foreach ($words as $word)
    {
        $word = strtolower($word);
        if (!in_array($word, EXCEPTIONS))
            $newWords[] = ucfirst($word);
        else
            $newWords[] = $word;
    }
    return implode($newWords, ' ');
}

var_dump($_POST);
$venue = new Venue();
$venue->setBuilding(addressToUpperFirst($_POST['building']));
$venue->setAddress(addressToUpperFirst($_POST['address']));
$venue->setCityId(addressToUpperFirst($_POST['cityId']));
$venuerepo = new VenueRepository();
try
{
    $venueId = $venuerepo->insert($venue);
}
catch (PDOException $e)
{
    $venue = $venuerepo->getByUKs($league);
    $venueId = $venue->getID();
}
var_dump($venueId);