<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 16-11-18
 * Time: 13:09
 */


use TTTheux\Models\League;
use TTTheux\Repositories\LeagueRepository;

require_once "../vendor/autoload.php";

// Ajouter une ligue ou récupérer son ID
$league = new League;
$league->setCategoryId($_POST['categoryId']);
$league->setDivision($_POST['division']);
$league->setSerie($_POST['serie']);
$leaguerepo = new LeagueRepository();
try
{
    $leagueId = $leaguerepo->insert($league);
}
catch (PDOException $e)
{
    $league = $leaguerepo->getByUKs($league);
    $leagueId = $league->getID();
}

echo $leagueId;