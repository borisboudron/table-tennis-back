<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 06-11-18
 * Time: 22:08
 */

use TTTheux\Models\Interclubs;
use TTTheux\Repositories\InterclubsRepository;

require_once "../vendor/autoload.php";

$interclubs = new Interclubs();
$interclubs->setVenueId($_POST['venueId']);
$interclubs->setDate($_POST['date']);
$interclubs->setLeagueId($_POST['leagueId']);
$interclubs->setInterclubsNb($_POST['interclubsNb']);
$interclubs->setStartTime($_POST['startHour']);
$interclubs->setEndTime($_POST['endHour']);
$interclubs->setHomeCaptainId($_POST['homeCaptain']);
$interclubs->setAwayCaptainId($_POST['awayCaptain']);
$interclubs->setRefereeId($_POST['referee']);
$interclubsrepo = new InterclubsRepository();
try
{
    $interclubsId = $interclubsrepo->insert($interclubs);
}
catch (PDOException $e)
{
    $interclubs = $interclubsrepo->getByUKs($interclubs);
    $interclubsId = $interclubs->getID();
}


echo $interclubsId;