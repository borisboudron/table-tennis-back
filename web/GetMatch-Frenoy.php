﻿<?php
//---------------------------------
// GetMatches-Frenoy.php
//---------------------------------
// Variables globales
use TTTheux\Core\DB\TabTConnector;

$Club = 'L184';
$Credentials = TabTConnector::getCredentials();
$MatchUniqueId = '345922';

$GetMatchesRequest = array('Credentials' => $Credentials
                          ,'Club'  => $Club
                          ,'ShowDivisionName' => 'yes'
                          ,'WithDetails' => true
                          ,'MatchUniqueId'  => $MatchUniqueId
                          );

// Variable d'output
$match = null;
// Ouverture de la connection Soap
$tabt = TabTConnector::getInstance();

$match = $tabt->GetMatches($GetMatchesRequest)->TeamMatchesEntries;
var_dump($match);