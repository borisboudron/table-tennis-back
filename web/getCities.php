<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 11-11-18
 * Time: 14:18
 */

use TTTheux\Repositories\CityRepository;
require_once "../vendor/autoload.php";

$repo = new CityRepository();
$cities = $repo->getAll();

$toJSON = json_encode($cities);

echo $toJSON;