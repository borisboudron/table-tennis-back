<?php


require_once "../vendor/autoload.php";
use TTTheux\Repositories\PlayerRepository;

$repo = new PlayerRepository();
$players = $repo->getAllByClub($_POST['season'], $_POST['clubId']);

$toJSON = json_encode($players);

echo $toJSON;

