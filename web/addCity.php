<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 11-11-18
 * Time: 13:44
 */

use TTTheux\Models\City;
use TTTheux\Repositories\CityRepository;

require_once "../vendor/autoload.php";

$city = new City();
$city->setPostalCode($_POST['postalCode']);
$city->setName($_POST['name']);
$cityrepo = new CityRepository();
try
{
    $cityId = $cityrepo->insert($city);
}
catch (PDOException $e)
{
    $city = $cityrepo->getByUKs($league);
    $cityId = $city->getID();
}