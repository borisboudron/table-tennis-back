<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 19-11-18
 * Time: 15:42
 */


use TTTheux\Core\DB\TabTConnector;

$Season = 19;

//$Levels = array(1,16,12);   // National, IWB, Namur
//$Levels = array(1,16,12,11,4,13,14);  // AFTT
$Levels = array(1,16,12,11,4,13,14,2,3,5,6,7,8,9,10,15); // FRBTT

$Credentials = TabTConnector::getCredentials();

// Variables pour appeler les fonctions de TabT
$TestRequest = array('Credentials' => $Credentials
);

$GetMembersRequest = array('Credentials' => $Credentials
//,'Club'  => $_POST['clubId']
,'Season'  => $Season
//,'UniqueIndex' => '128231'
,'Club'  => 'L184'
//,'Season'  => 8
,'PlayerCategory'  => 1
,'ExtendedInformation'  => true
,'RankingPointsInformation'  => true
,'WithResults'  => true
,'WithOpponentRankingEvaluation'  => true
);
$RencDetPts = array('','','','','','','','','','','','','','','','');

// Ouverture de la connection Soap
$tabt = TabTConnector::getInstance();

// Get division list from a given Level
$i = '0';
if ($i == 0)
$ResponseMembers = $tabt->GetMembers($GetMembersRequest);
$members = [];
$nonPlayers = 0;
if (is_array($ResponseMembers->MemberEntries))
{
    foreach ($ResponseMembers->MemberEntries as $member)
    {
        if ($member->Status === 'A')
        {
            $member->Position = $member->Position - $nonPlayers;
            $members[] = $member;
        }
        else
            $nonPlayers++;
    }
}
else
{
    $member = $ResponseMembers->MemberEntries;
    if ($member->Status === 'A')
    {
        $member->Position = $member->Position - $nonPlayers;
        $members[] = $member;
    }
    else
        $nonPlayers++;
}

// Test
$ResponseTest = $tabt->Test($TestRequest);

$toJSON = json_encode($members);

echo $toJSON;