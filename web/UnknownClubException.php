<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 05-10-18
 * Time: 12:58
 */

class UnknownClubException extends Exception
{
    private $club;

    public function __construct($club)
    {
        $this->club = $club;
    }

    /**
     * UnknownClubException constructor.
     * @param $int
     */
    public function errorMessage($club) {
        //error message
        $errorMsg = "Le club $club n'est pas dans la base de données.";
        return $errorMsg;
    }


    /**
     * @return mixed
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * @param mixed $club
     * @return UnknownClubException
     */
    public function setClub($club)
    {
        $this->club = $club;
        return $this;
    }
}