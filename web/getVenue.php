<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 11-11-18
 * Time: 15:52
 */
use TTTheux\Repositories\VenueRepository;

require_once "../vendor/autoload.php";

$repo = new VenueRepository();
$venue = $repo->getByID($_GET['id']);
$venue->getCity();

$toJSON = json_encode($venue);

echo $toJSON;