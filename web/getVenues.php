<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 11-11-18
 * Time: 15:16
 */

use TTTheux\Repositories\VenueRepository;

require_once "../vendor/autoload.php";

$repo = new VenueRepository();
$venues = $repo->getAll();

foreach ($venues as $venue)
    $venue->getCity();

$toJSON = json_encode($venues);

echo $toJSON;