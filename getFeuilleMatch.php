<?php

use TTTheux\Repositories\InterclubsRepository;

require_once "vendor/autoload.php";
$loader = new Twig_Loader_Filesystem('src/Views');
$twig = new Twig_Environment($loader);
$twig->addExtension(new Twig_Extension_Debug());
$twig->addFilter(new Twig_SimpleFilter('var_dump', 'var_dump'));

$repo = new InterclubsRepository();
$interclubs = $repo->getByID(1);


try {
    echo $twig->render("feuilleMatch.html.twig", [
        'interclubs' => $interclubs,
    ]);
} catch (Twig_Error_Loader $e) { echo $e->getMessage();
} catch (Twig_Error_Runtime $e) { echo $e->getMessage();
} catch (Twig_Error_Syntax $e) { echo $e->getMessage();
}
?>