<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 17-12-18
 * Time: 19:12
 */

use TTTheux\Core\Routing\Router;

session_start();
ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once './vendor/autoload.php';

$router = new Router();
$router->registerAllRoutes();
echo $router->handleRequest();