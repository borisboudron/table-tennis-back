﻿<?php
//---------------------------------
// GetMatches-Frenoy.php
//---------------------------------
// Variables globales
use TTTheux\Core\DB\TabTConnector;

$Season = 19;

//$Levels = array(1,16,12);   // National, IWB, Namur
//$Levels = array(1,16,12,11,4,13,14);  // AFTT
$Levels = array(1,16,12,11,4,13,14,2,3,5,6,7,8,9,10,15); // FRBTT

# Chemin vers fichier texte
$file ="C:\\TT\\saison ".(2000+$Season-1)."-".(2000+$Season)."\DB AFTT\Frenoy\Matchs.txt";

$Credentials = TabTConnector::getCredentials();

// Variables pour appeler les fonctions de TabT
$TestRequest = array('Credentials' => $Credentials
                    );

$GetDivisionsRequest = array('Credentials' => $Credentials
                            ,'Season' => $Season
                            ,'Level' => ''
                            ,'ShowDivisionName' => 'yes'
                            );

$GetMatchesRequest = array('Credentials' => $Credentials
                          ,'DivisionId'  => -1
                          ,'Club'  => ''
                          ,'Team'  => ''
                          ,'DivisionCategory'  => -1
                          ,'Season'  => $Season
                          ,'WeekName'  => ''
                          ,'Level'  => ''
                          ,'ShowDivisionName' => 'no'
                          ,'YearDateFrom'  => '2000-01-01'
                          ,'YearDateTo'  => '9999-12-31'
                          ,'WithDetails' => true
                          ,'MatchId'  => ''
                          ,'MatchUniqueId'  => ''
                          );

// Variable d'output
$DivisionCategory = '';
$DivisionId = '';
$MatchId = '';
$Journee = '';
$Date = '';
$Heure = '';
$IndexVt = '';
$TeamVt = '';
$PlayerVt = array('','','','');
$IsPlayerVtWo = array('','','','');
$PlayerVr = array('','','','');
$IsPlayerVrWo = array('','','','');
$ScoreVt = -1;
$ScoreVr = -1;
$IsFfg = false;
$RencSetsVt = array('','','','','','','','','','','','','','','','');
$RencSetsVr = array('','','','','','','','','','','','','','','','');
$RencDetPts = array('','','','','','','','','','','','','','','','');

// Ouverture de la connection Soap
$tabt = TabTConnector::getInstance();



// en output, on aura un tableau qui donnera le statut pour chaque appel
echo '<table>';

// Test
$ResponseTest = $tabt->Test($TestRequest);
echo '<tr>';
echo '<td>Test</td>';
echo '<td>', $ResponseTest->Timestamp, '</td>';
echo '<td>', $ResponseTest->CurrentQuota, '</td>';
echo '<td>', $ResponseTest->AllowedQuota, '</td>';
echo '</tr>';


// Ouverture du fichier d'output en mode écriture
$fileopen=(fopen($file,'w'));
fwrite($fileopen,";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\r\n");

foreach ($Levels as $Level)
{
	$GetDivisionsRequest['Level'] = $Level;
	$GetMatchesRequest['Level'] = $Level;

	// Get division list from a given Level
	$ResponseDivisions = $tabt->GetDivisions($GetDivisionsRequest);
	if (!array_key_exists('DivisionEntries', $ResponseDivisions))
		break;

	// Loop through the divisions
	foreach ($ResponseDivisions->DivisionEntries as $DivisionEntry)
	{
		if (!array_key_exists('DivisionCategory', $DivisionEntry))
			break;
		
		// Get matchs from a given division
		if (  $DivisionEntry->DivisionCategory == 1
		   || $DivisionEntry->DivisionCategory == 2
		   )
		{
			$DivisionCategory = $DivisionEntry->DivisionCategory;
			$DivisionId = $DivisionEntry->DivisionId;

			$GetMatchesRequest['DivisionId'] = $DivisionEntry->DivisionId;
			$GetMatchesRequest['DivisionCategory'] = $DivisionEntry->DivisionCategory;

			$ResponseMatches = $tabt->GetMatches($GetMatchesRequest);

			// Loop through received entries of the ranking and prepare
			foreach ($ResponseMatches->TeamMatchesEntries as $TeamMatchesEntry)
			{
				$MatchId = $TeamMatchesEntry->MatchId;
				$Journee = $TeamMatchesEntry->WeekName;
				$Date = array_key_exists('Date', $TeamMatchesEntry) ? $TeamMatchesEntry->Date : "";
				$Heure = array_key_exists('Time', $TeamMatchesEntry) ? substr($TeamMatchesEntry->Time,0,5) : "";

				$IndexVt = (($TeamMatchesEntry->HomeClub == '-') ? 'BYE' : $TeamMatchesEntry->HomeClub);
				if (substr($IndexVt,0,2) == "LK")
					$IndexVt = substr_replace($IndexVt,'Lk',0,2);
				else if (substr($IndexVt,0,3) == "OVL")
					$IndexVt = substr_replace($IndexVt,'Ovl',0,3);
				else if (substr($IndexVt,0,3) == "WVL")
					$IndexVt = substr_replace($IndexVt,'Wvl',0,3);
				else if (substr($IndexVt,0,4) == "Vl-B")
					$IndexVt = substr_replace($IndexVt,'VlB',0,4);
				$TeamVt = (($IndexVt == 'BYE') ? '' : substr($TeamMatchesEntry->HomeTeam, strlen($TeamMatchesEntry->HomeTeam)-1));
				
				$IndexVr = (($TeamMatchesEntry->AwayClub == '-') ? 'BYE' : $TeamMatchesEntry->AwayClub);
				if (substr($IndexVr,0,2) == "LK")
					$IndexVr = substr_replace($IndexVr,'Lk',0,2);
				else if (substr($IndexVr,0,3) == "OVL")
					$IndexVr = substr_replace($IndexVr,'Ovl',0,3);
				else if (substr($IndexVr,0,3) == "WVL")
					$IndexVr = substr_replace($IndexVr,'Wvl',0,3);
				else if (substr($IndexVr,0,4) == "Vl-B")
					$IndexVr = substr_replace($IndexVr,'VlB',0,4);
				$TeamVr = (($IndexVr == 'BYE') ? '' : substr($TeamMatchesEntry->AwayTeam, strlen($TeamMatchesEntry->AwayTeam)-1));

				$IsFfg = 'N';
				if (!array_key_exists('Score', $TeamMatchesEntry))
				{
					$ScoreVt = '';
					$ScoreVr = '';
				}
				else
				{
					$ScoreArray = explode("-", $TeamMatchesEntry->Score);
					$ScoreFft = explode(" ", $ScoreArray[1]);
					$ScoreVt = $ScoreArray[0];
					$ScoreVr = $ScoreFft[0];
					if (array_key_exists(1,$ScoreFft) && $ScoreFft[1] == 'ff')
					{
						if ($ScoreVt == 0)
							$ScoreVt = 'ff';
						if ($ScoreVr == 0)
							$ScoreVr = 'ff';
					}
					if (  (array_key_exists(1,$ScoreFft) && $ScoreFft[1]  == '(fg)')
					   || (array_key_exists(1,$ScoreFft) && $ScoreFft[1]  == 'fg')
					   || (array_key_exists(2,$ScoreFft) && $ScoreFft[2]  == '(fg)')
					   || (array_key_exists(2,$ScoreFft) && $ScoreFft[2]  == 'fg')
					   )
						$IsFfg = 'Y';
				}
				if (  ($IndexVr == 'BYE' && array_key_exists('IsHomeForfeited', $TeamMatchesEntry) && $TeamMatchesEntry->IsHomeForfeited)
				   || ($IndexVt == 'BYE' && array_key_exists('IsAwayForfeited', $TeamMatchesEntry) && $TeamMatchesEntry->IsAwayForfeited)
			       )
					$IsFfg = 'Y';

				// joueurs visités
				for ($i=0; $i<4; $i++)
				{
					$PlayerVt[$i] = '';
					$IsPlayerVtWo[$i] = '';
				}
				if (  $IndexVt != 'BYE'
				   && array_key_exists('MatchDetails', $TeamMatchesEntry)
				   && array_key_exists('HomePlayers', $TeamMatchesEntry->MatchDetails)
				   && $TeamMatchesEntry->MatchDetails->HomePlayers->PlayerCount > 0
				   )
				{
					if (array_key_exists('Players', $TeamMatchesEntry->MatchDetails->HomePlayers))
					{
						foreach ($TeamMatchesEntry->MatchDetails->HomePlayers->Players as $Player)
						{
							if (  is_object($Player)  // parce que parfois, le programme ne rend qu'un joueur et à ce moment, la conversion remonte le contenu d'un niveau
							   && array_key_exists('Position', $Player)
							   )
							{
								$PlayerVt[($Player->Position)-1] = $Player->UniqueIndex;
								$IsPlayerVtWo[($Player->Position)-1] = ((  array_key_exists('IsForfeited', $Player)
																		&& $Player->IsForfeited
																		) ? 'Y' : 'N'
																	   );
							}
						}
					}
				}

				// joueurs visiteurs
				for ($i=0; $i<4; $i++)
				{
					$PlayerVr[$i] = '';
					$IsPlayerVrWo[$i] = '';
				}
				if (  $IndexVr != 'BYE'
				   && array_key_exists('MatchDetails', $TeamMatchesEntry)
				   && array_key_exists('AwayPlayers', $TeamMatchesEntry->MatchDetails)
				   && $TeamMatchesEntry->MatchDetails->AwayPlayers->PlayerCount > 0
				   )
				{
					if (array_key_exists('Players', $TeamMatchesEntry->MatchDetails->AwayPlayers))
					{
					foreach ($TeamMatchesEntry->MatchDetails->AwayPlayers->Players as $Player)
						{
							if (  is_object($Player)  // parce que parfois, le programme ne rend qu'un joueur et à ce moment, la conversion remonte le contenu d'un niveau
							   && array_key_exists('Position', $Player)
							   )
							{
								$PlayerVr[($Player->Position)-1] = $Player->UniqueIndex;
								$IsPlayerVrWo[($Player->Position)-1] = ((  array_key_exists('IsForfeited', $Player)
																		&& $Player->IsForfeited
																		) ? 'Y' : 'N'
																	   );
							}
						}
					}
				}

				// scores
				for ($i=0; $i<16; $i++)
				{
					$RencSetsVt[$i] = '';
					$RencSetsVr[$i] = '';
					$RencDetPts[$i] = '';
				}
				if (  $IndexVr != 'BYE'
				   && array_key_exists('MatchDetails', $TeamMatchesEntry)
				   && array_key_exists('IndividualMatchResults', $TeamMatchesEntry->MatchDetails)
				   )
				{
					foreach ($TeamMatchesEntry->MatchDetails->IndividualMatchResults as $IndividualMatchResults)
					{
						if (  is_object($IndividualMatchResults)  // parce que parfois, le programme ne rend qu'un joueur et à ce moment, la conversion remonte le contenu d'un niveau
						   && array_key_exists('Position', $IndividualMatchResults)
						   )
						{
							$i = ($IndividualMatchResults->Position)-1;
							if (  array_key_exists('IsHomeForfeited', $IndividualMatchResults)
							   && $IndividualMatchResults->IsHomeForfeited
							   )
								$RencSetsVt[$i] = 'w';
							if (  array_key_exists('IsAwayForfeited', $IndividualMatchResults)
							   && $IndividualMatchResults->IsAwayForfeited
							   )
								$RencSetsVr[$i] = 'w';
							if ($RencSetsVt[$i] != 'w' && $RencSetsVr[$i] != 'w')
							{
								if (array_key_exists('HomeSetCount', $IndividualMatchResults))
									$RencSetsVt[$i] = $IndividualMatchResults->HomeSetCount;
								if (array_key_exists('AwaySetCount', $IndividualMatchResults))
									$RencSetsVr[$i] = $IndividualMatchResults->AwaySetCount;
							}
							else if ($RencSetsVt[$i] != 'w')
								$RencSetsVt[$i] = 3;
							else if ($RencSetsVr[$i] != 'w')
								$RencSetsVr[$i] = 3;

							if (array_key_exists('Scores', $IndividualMatchResults))
								$RencDetPts[($IndividualMatchResults->Position)-1] = $IndividualMatchResults->Scores;
						}
					}
				}

				// Ecriture de la ligne
				fwrite($fileopen,    $Level.";".$DivisionCategory.";".$DivisionId.";".$MatchId.";".$Journee.";".$Date.";".$Heure);
				fwrite($fileopen,";".$IndexVt.";".$TeamVt);
				for ($i=0; $i<4; $i++)
					fwrite($fileopen,";".$PlayerVt[$i].";".$IsPlayerVtWo[$i]);
				fwrite($fileopen,";".$IndexVr.";".$TeamVr);
				for ($i=0; $i<4; $i++)
					fwrite($fileopen,";".$PlayerVr[$i].";".$IsPlayerVrWo[$i]);
				fwrite($fileopen,";".$ScoreVt.";".$ScoreVr.";".$IsFfg);
				for ($i=0; $i<16; $i++)
					fwrite($fileopen,";".$RencSetsVt[$i].$RencSetsVr[$i].";".$RencDetPts[$i]);
				fwrite($fileopen,"\r\n");
			}
		}
	}
}

# On ferme le fichier proprement
fclose($fileopen);

// Test
$ResponseTest = $tabt->Test($TestRequest);
echo '<tr>';
echo '<td>Test</td>';
echo '<td>', $ResponseTest->Timestamp, '</td>';
echo '<td>', $ResponseTest->CurrentQuota, '</td>';
echo '<td>', $ResponseTest->AllowedQuota, '</td>';
echo '</tr>';

// en output, fin du tableau
echo '</table>';

?>
